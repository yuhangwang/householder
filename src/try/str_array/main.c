#include <stdio.h>
#include <string.h>


void write(char *file_name)
{
    FILE *p_File = fopen(file_name, "w");

    for ( int i = 0; i < 5; i++ )
    {
        fprintf(p_File, "%d\n", i);
    }
    fclose(p_File);
}
int
main(void)
{
    char str[100] = {'a'};

    for (int i = 1; i < 3; i++)
    {
        sprintf(str,"case_%d_R.txt", i);
        printf("%s (length %ld)\n", str, strlen(str));
        write(str);
    }
    return 0;

}