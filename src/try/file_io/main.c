#include <stdio.h>

int
main(void)
{
    char *file_name = "test.txt";

    FILE *p_File = fopen(file_name, "r");

    if (p_File == NULL)
    {
        puts("cannot open file");
        fclose(p_File);
        return 1;
    }
    puts("file found!");
    fclose(p_File);
    return 0;
}