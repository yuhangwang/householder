#include <stdio.h>
#include <stdbool.h>

int
main(void)
{
    for (int k = 0; k < 10; k++)
    {
        printf("k = %d\n", k);

        if (true)
        {
            if (true)
            {
                break;
            }

            printf("-----------\n");
        }
    }
    return 0;
}