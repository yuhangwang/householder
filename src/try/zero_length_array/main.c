#include <stdio.h>

int
main(void)
{
    //note: zero-length array is allowed in GNU C but not the standard:
    /*
    ref: https://gcc.gnu.org/onlinedocs/gcc/Zero-Length.html
    Quote from GNU.org:
    "
    Zero-length arrays are allowed in GNU C. They are very useful as the last element of a structure that is really a header for a variable-length object:

         struct line {
           int length;
           char contents[0];
         };
         
         struct line *thisline = (struct line *)
           malloc (sizeof (struct line) + this_length);
         thisline->length = this_length;
    In ISO C90, you would have to give contents a length of 1, which means either you waste space or complicate the argument to malloc.

    In ISO C99, you would use a flexible array member, which is slightly different in syntax and semantics:

    Flexible array members are written as contents[] without the 0.
    Flexible array members have incomplete type, and so the sizeof operator may not be applied. As a quirk of the original implementation of zero-length arrays, sizeof evaluates to zero.
    Flexible array members may only appear as the last member of a struct that is otherwise non-empty.
    A structure containing a flexible array member, or a union containing such a structure (possibly recursively), may not be a member of a structure or an element of an array. (However, these uses are permitted by GCC as extensions.)
    "
    */
    int a[0];
    printf("address of a = %p\n", a);
    return 0;
}