import numpy

def householder(A):
    tolerance = 1.E-5
    m, n = numpy.shape(A)
    V = numpy.zeros((m,m))

    for k in range(min(n, m-1)):
        alpha = -numpy.sign(A[k,k])*numpy.linalg.norm(A[k:m, k])
        v = numpy.zeros(m)
        v[k:] = A[k:, k]
        v[k] -= alpha
        beta = numpy.dot(v,v)
        if abs(beta) < tolerance:
            continue
        for j in range(k, n):
            gamma = numpy.dot(v, A[:, j])
            A[:, j] -= (2.*gamma/beta)*v

        # copy v to V
        V[:,k] = v

    R = A
    return V, R

def matrix_to_string(M):
    m,n = numpy.shape(M)
    list_Output  = []
    for i in range(m):
        for j in range(n):
            list_Output.append("{:.6f}".format(M[i,j]))
            if not ( (j == (n-1)) and (i == (m-1)) ):
                list_Output.append(', ')

        list_Output.append('\n')
    return ''.join(list_Output)

def print_matrix(M):
    print(matrix_to_string(M))


def solve(A):
    # print("A(before)")
    # print_matrix(A)
    # print()

    V, R = householder(A)

    # print("A(after)")
    # print_matrix(A)
    # print()


    # print("R")
    # print_matrix(R)
    # print()

    # print("V")
    # print_matrix(V)
    # print()

    # print("normalzed V")
    # V[-1,-1] = 1.
    # normalized_V = V/numpy.diag(V)
    # print_matrix(normalized_V)
    # print()

    return V, R


def make_a_test_case(size, A, R, V):
    template = """
    static void
    case3_{0}x{0}_matrix_io(Params_Fixture fixture)
    {{
        module_Matrix
        Matrix = import_Matrix();


        //---------------------------------------------
        // define test case content
        //---------------------------------------------
        int i_RowCount = {0};
        int i_ColumnCount = {0};
        Class_MatrixIndex2D
        obj_Id = Matrix.new_MatrixIndex2D(i_RowCount, i_ColumnCount);
        
        float 
        A[{1}] = 
        {{{2}}};
       
        float 
        R[{1}] = 
        {{{3}}};

        float 
        V[{1}] = 
        {{{4}}};

        //---------------------------------------------
        // update the content of matrix A and V_expect
        //---------------------------------------------
        for (int i = 0; i < i_RowCount; i++)
        {{
            for (int j = 0; j < i_ColumnCount; j++)
            {{
                int index = obj_Id.index(obj_Id, i, j);
                fixture.A[index] = A[index];
                fixture.V_expect[index] = V[index];
                fixture.R_expect[index] = R[index];
            }}
        }}
        //---------------------------------------------
        *fixture.p_Index       = obj_Id;
        *fixture.p_RowCount    = i_RowCount;
        *fixture.p_ColumnCount = i_ColumnCount;
    }}
    """.format(size, size*size, A, R, V)

    return template

def tridiag(size):
    output = numpy.zeros((size, size))
    for i in range(size):
        for j in range(max(0,i-1),i+1):
            output[i][j] = 1
    return output

def write_solutions(matrix_row_count):
    # A = [[1., 0, 0],
    #      [1.2, 1., 0],
    #      [1.3, 1.1, 1]]

    A = tridiag(matrix_row_count)
    A_copy = tridiag(matrix_row_count)

    A = numpy.array(A)
    A = numpy.round(A, decimals=5)

    V, R = solve(A)

    c_code = make_a_test_case(
        matrix_row_count,
        matrix_to_string(A_copy),
        matrix_to_string(R),
        matrix_to_string(V),
        )

    output_file = 'case_{0}x{0}.c'.format(matrix_row_count)
    obj_OUT = open(output_file, 'w')
    obj_OUT.write(c_code)
    obj_OUT.close()

    output_file_R = "expect_R_{0}x{0}.dat".format(matrix_row_count)
    output_file_V = "expect_V_{0}x{0}.dat".format(matrix_row_count)
    numpy.savetxt(output_file_R, R)
    numpy.savetxt(output_file_V, V)

def main():
    size = 1024
    write_solutions(size)

#=========================
main()