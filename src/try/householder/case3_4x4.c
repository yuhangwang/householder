
    static void
    case3_4x4_matrix_io(Params_Fixture fixture)
    {
        module_Matrix
        Matrix = import_Matrix();


        //---------------------------------------------
        // define test case content
        //---------------------------------------------
        int i_RowCount = 4;
        int i_ColumnCount = 4;
        Class_MatrixIndex2D
        obj_Id = Matrix.new_MatrixIndex2D(i_RowCount, i_ColumnCount);
        
        float 
        A[16] = 
        {-1.414214, -0.707107, 0.000000, 0.000000, 
        0.000000, -1.224745, -0.816497, 0.000000, 
        0.000000, 0.000000, -1.154701, -0.866025, 
        0.000000, 0.000000, -0.000000, 0.500000
        };
       
        float 
        R[16] = 
        {-1.414214, -0.707107, 0.000000, 0.000000, 
        0.000000, -1.224745, -0.816497, 0.000000, 
        0.000000, 0.000000, -1.154701, -0.866025, 
        0.000000, 0.000000, -0.000000, 0.500000
        };

        float 
        V[16] = 
        {2.414214, 0.000000, 0.000000, 0.000000, 
        1.000000, 1.931852, 0.000000, 0.000000, 
        0.000000, 1.000000, 1.732051, 0.000000, 
        0.000000, 0.000000, 1.000000, 0.000000
        };

        //---------------------------------------------
        // update the content of matrix A and V_expect
        //---------------------------------------------
        for (int i = 0; i < i_RowCount; i++)
        {
            for (int j = 0; j < i_ColumnCount; j++)
            {
                int index = obj_Id.index(obj_Id, i, j);
                fixture.A[index] = A[index];
                fixture.V_expect[index] = V[index];
                fixture.R_expect[index] = R[index];
            }
        }
        //---------------------------------------------
        *fixture.p_Index       = obj_Id;
        *fixture.p_RowCount    = i_RowCount;
        *fixture.p_ColumnCount = i_ColumnCount;
    }
	