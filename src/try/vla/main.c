#include <stdio.h>
#include <stdlib.h>

int 
main(int argc, char *argv[])
{   
    int size = 1;
    if ( argc > 1 )
    {
        size = atoi(argv[1]);
    }
    else
    {
        puts("please specify a matrix size");
        exit(0);
    }
    float A[size*size];

    A[0] = 0;
    A[0] += 1.;

    printf("size of A = %ld\n", sizeof(A)/sizeof(float));
    return 0;
}