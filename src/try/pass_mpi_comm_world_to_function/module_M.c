#include "module_M.h"
#include "mpi.h"

#include <stdio.h>
#include <string.h>



static void 
hi(void)
{
	printf("Hello world!\n");
}

static void
run(MPI_Comm obj_MpiCommWorld)
{
	int i_MPI_Volume   = 0;
	int i_MPI_ThisRank = -1;
	MPI_Comm_size(obj_MpiCommWorld, &i_MPI_Volume);
	MPI_Comm_rank(obj_MpiCommWorld, &i_MPI_ThisRank);
	printf(
		"my rank = %d "
		"total process count = %d\n",
		i_MPI_ThisRank,
		i_MPI_Volume);

}

module_M 
import_M(void)
{
	module_M obj = {
		.hi  = hi,
		.run = run,
	};

	return obj;
}
