#include "mpi.h"

typedef struct
module_M
{
	void (*hi)(void);	
	void (*run)(MPI_Comm);
}
module_M;

module_M import_M(void);
