#include "module_M.h"

#include <stdio.h>
#include "mpi.h"

int
main (int i_ArgumentCount, char *c_ArgumentArray[])
{
	// int i_MPI_Volume   = 0;
	// int i_MPI_ThisRank = -1;

	MPI_Init(&i_ArgumentCount, &c_ArgumentArray);

	// MPI_Comm_size(MPI_COMM_WORLD, &i_MPI_Volume);
	// MPI_Comm_rank(MPI_COMM_WORLD, &i_MPI_ThisRank);

	

	module_M M = import_M();

	M.run(MPI_COMM_WORLD);

	MPI_Finalize();

	return 0;
}