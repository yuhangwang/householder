# Design Principle

## One function one argument type
The benefits of using a type (struct) to define the function's argument
is: 

1. it avoids the problem of argument shifts by one issue when
   the user mess up the order of arguments and may lead to incorrect 
   results, since C only checks whether the argument type matches.
2. clearer layout: the client can visually check which
   argument goes where.
3. easier to reuse common arguments. When the many of the arguments
   are common across different loop iterations, it is easier to just
    update the one that changes.
