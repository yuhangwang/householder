# Naming Convention

## Parameter Type
Parameter type (struct) follow this template 
Params_"ModuleName"_"MethodName"_"data_type", 
e.g. Params_Matrix_Print_float. 
Both the module name and method name are in CamelCase,
while the data type in snake_case.

