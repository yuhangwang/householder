# Test module_Matrix
This is the test suite for module_Matrix.


#[test_module_Matrix.dep.h](#test_module_Matrix.dep.h "save:")
    /* N/A */

#[test_module_Matrix.h](#test_module_Matrix.h "save:")
    #include "test_module_Matrix.dep.h"



#[test_module_Matrix.dep.c](#test_module_Matrix.dep.c "save:")
    #include "test_module_Matrix.h"
    #include "module_Matrix.h"
    #include "module_Vector.h"
    #include "module_Type.h"
    #include "module_Assert.h"
    #include <stdio.h>
    #include <assert.h>
    #include <math.h>



#[test_module_Matrix.c](#test_module_Matrix.c "save:")
    #include "test_module_Matrix.dep.c"

    _"test_module_Matrix all tests"

    int
    main(void)
    {
        puts("----------------------");
        test_hi(false);
        puts("----------------------");
        test_print(false);
        puts("----------------------");
    	test_print_float(false);
        puts("----------------------");
        test_new_MatrixIndex2D(false);
        puts("----------------------");
        test_o2_norm_column_float(false);
        puts("----------------------");
        test_dot_column_float(false);
        puts("----------------------");
        test_add_scaled_vector_to_column_float(false);
        puts("----------------------");
        test_reset_float(false);
        puts("----------------------");
        test_copy_vector_to_column_float(false);
        puts("----------------------");
        test_copy_vector_to_columns_float(false);
        puts("----------------------");
        test_copy_column_to_vector_float(false);
        puts("----------------------");
        test_copy_columns_to_vector_float(false);
        puts("----------------------");
        test_write_text_file_float(true);
        puts("----------------------");


    	return 0;
    }

# test_module_Matrix all tests 

Test hi
    static bool
    test_hi(bool bool_RunTest)
    {
        if ( ! bool_RunTest ) { return true; }
        module_Matrix 
        Matrix = import_Matrix();

        Matrix.hi();
        return true;
    }

Test module_Matrix.print 
    static bool
    test_print(bool bool_RunTest)
    {
        if ( ! bool_RunTest ) { return true; }

        puts("\ntest module_Matrix.print()\n");
        
        module_Matrix 
        Matrix = import_Matrix();

        module_Type
        Type = import_Type();

        MyTypeChoice my_type = Type.Float;

        int i_RowCount    = 2;
        int i_ColumnCount = 2;
        float A[4] = {1, 0, 
                      0, 1};
        Params_Matrix_Print
        params = {
            .matrix = &A[0],
            .matrix_row_count = i_RowCount,
            .matrix_column_count = i_ColumnCount,
            .type = my_type,
            .precision = 1,
        };

        Matrix.print(params);
        return true;
    }

Test module_Matrix.print 
    static bool
    test_print_float(bool bool_RunTest)
    {
        if ( ! bool_RunTest ) { return true; }

        puts("\ntest module_Matrix.print_float()\n");
        
        module_Matrix 
        Matrix = import_Matrix();


        int i_RowCount    = 2;
        int i_ColumnCount = 2;
        float A[4] = {1, 0, 
                      0, 1};
        Params_Matrix_Print_float
        params = {
            .matrix = &A[0],
            .matrix_row_count = i_RowCount,
            .matrix_column_count = i_ColumnCount,
            .precision = 1,
        };

        Matrix.print_float(params);
        return true;
    }

Test module_Matrix.new_MatrixIndex2D
    static bool
    test_new_MatrixIndex2D(bool bool_RunTest)
    {
        if ( ! bool_RunTest ) { return true; }

        puts("\ntest module_Matrix.new_MatrixIndex2D()\n");

        module_Matrix
        Matrix = import_Matrix();

        module_Assert
        Assert = import_Assert();

        int i_RowCount = 2;
        int i_ColumnCount = 3;


        Class_MatrixIndex2D
        obj_Index = Matrix.new_MatrixIndex2D(i_RowCount, i_ColumnCount);

        for (int i = 0; i < i_RowCount; i++)
        {
            for (int j = 0; j < i_ColumnCount; j++)
            {
                int expect = (i * i_ColumnCount) + j;
                int answer = obj_Index.index(obj_Index, i,j);

                Params_Assert_Equal_int
                params_assert = {
                    .input1 = answer,
                    .input2 = expect,
                };
                assert(Assert.equal_int(params_assert));
                printf("[%d][%d] ok!\n", i, j);
            }
        }
        return true;
    }

Test module_Matrix.o2_norm_column_float
    static bool
    test_o2_norm_column_float(bool bool_RunTest)
    {
        if ( ! bool_RunTest ) { return true; }

        puts("\ntest module_Matrix.o2_norm_column_float()");
        //-----------------------------
        module_Matrix
        Matrix = import_Matrix();

        module_Vector
        Vector = import_Vector();

        module_Type
        Type = import_Type();

        module_Assert
        Assert = import_Assert();
        //-----------------------------


        int i_RowCount = 4;
        int i_ColumnCount = 4;
        // int i_MatrixElementCount = i_RowCount * i_ColumnCount;
        float A[16] = {0., 0., 0., 0.,
                       0., 2., 0., 0.,
                       0., 1., 2., 0.,
                       0., 1., 1., 2.};

        //------------------------------------
        // print out the matrix
        //------------------------------------

        Params_Matrix_Print
        matrix_print_params = {
            .matrix = A,
            .matrix_row_count = i_RowCount,
            .matrix_column_count = i_ColumnCount,
            .type = Type.Float,
            .precision = 0,
        };
        Matrix.print(matrix_print_params);
        //------------------------------------

        //------------------------------------
        // define which rows and columns to work on
        //------------------------------------
        int column_ids[] = {0, 1, 2, 3};
        int row_id_eon[] = {0, 1, 2, 3};
        int row_id_end[] = {3, 3, 3, 3};
        int length = 4;
        float answer[4]  = {-9999.};
        float expect[4]  = {0.};
        float tolerance  = 1.E-5;
        expect[0] = sqrt(0.);
        expect[1] = sqrt(6.);
        expect[2] = sqrt(5.);
        expect[3] = sqrt(4.);
        
        Params_Vector_Print_float
        vector_print_params = {
            .vector = answer,
            .length = length,
            .precision = 3,
        };


        Params_Matrix_o2NormColumns_float
        params = Matrix.new_Params_o2NormColumns_float();
        params.matrix = A;
        params.row_ids_eon = row_id_eon;
        params.row_ids_end = row_id_end;
        params.column_ids = column_ids;
        params.length     = length;
        params.matrix_row_count  = i_RowCount;
        params.matrix_column_count = i_ColumnCount;
        params.results_OUT  = answer;

        Matrix.o2_norm_columns_float(params);

        printf("answer: ");
        vector_print_params.vector = answer;
        Vector.print_float(vector_print_params);

        printf("expect: ");
        vector_print_params.vector = expect;
        Vector.print_float(vector_print_params);

        //----------------------------------------
        // Check results
        //----------------------------------------
        Params_Assert_EqualArray_float
        assert_params = {
            .input1 = answer,
            .input2 = expect,
            .tolerance = tolerance,
            .length = length,
        };

        Assert.equal_array_float(assert_params);
        return true;
    }

Test Matrix.dot_column_float 
    static bool
    test_dot_column_float(bool bool_RunTest)
    {
        if ( ! bool_RunTest ) { return true; }

        puts("\ntest module_Matrix.o2_norm_column_float()");
        //-----------------------------
        module_Matrix
        Matrix = import_Matrix();

        module_Type
        Type = import_Type();

        module_Assert
        Assert = import_Assert();
        //-----------------------------


        int i_RowCount = 4;
        int i_ColumnCount = 4;
        // int i_MatrixElementCount = i_RowCount * i_ColumnCount;
        float A[16] = {0., 1., 1., 1.,
                       1., 2., 0., 0.,
                       0., 1., 2., 0.,
                       2., 1., 1., 2.};

        float V[16] = {0., 1., 0., 2.,
                       1., 2., 1., 1.,
                       1., 0., 2., 1.,
                       1., 0., 0., 2.};
        int vector_length = 4;
        float f_ExpectedArray[4]    = {5., 6., 5., 4.};
        int column_ids[4]  = {0, 1, 2, 3};
        int row_ids_eon[4] = {0, 1, 2, 3};
        int row_ids_end[4] = {3, 3, 3, 3};
        //------------------------------------
        // print out the matrix
        //------------------------------------

        Params_Matrix_Print
        matrix_print_params = {
            .matrix = A,
            .matrix_row_count = i_RowCount,
            .matrix_column_count = i_ColumnCount,
            .type = Type.Float,
            .precision = 0,
        };
        Matrix.print(matrix_print_params);
        //------------------------------------

        Class_MatrixIndex2D
        obj_Id = Matrix.new_MatrixIndex2D(i_RowCount, i_ColumnCount);

        for (int j = 0; j < i_ColumnCount; j++)
        {
            float answer = -9999.;
            Params_Matrix_DotColumn_float
            params_dot = Matrix.new_Params_DotColumn_float();
            params_dot.matrix = A;
            params_dot.matrix_row_count = i_RowCount;
            params_dot.matrix_column_count = i_ColumnCount;
            params_dot.column_id = column_ids[j];
            params_dot.row_id_eon = row_ids_eon[j];
            params_dot.row_id_end = row_ids_end[j];
            params_dot.result_OUT = &answer;

            int i_RowId = j;
            int index = obj_Id.index(obj_Id, i_RowId, j);
            params_dot.vector = &V[index];
            params_dot.vector_length = vector_length;

            /**************************/
            Matrix.dot_column_float(params_dot);
            /**************************/

            float expect = f_ExpectedArray[j];

            Params_Assert_Equal_float 
            params_assert = {
                .input1 = answer,
                .input2 = expect,
                .tolerance = 1.E-5,
            };
            Assert.equal_float(params_assert);
        }
        return true;
    }

Test Matrix.test_add_scaled_vector_to_column_float
    static bool
    test_add_scaled_vector_to_column_float(bool bool_RunTest)
    {
        if ( ! bool_RunTest ) { return true; }

        puts("\ntest_add_scaled_vector_to_column_float()\n");

        //-------------------------------------
        // Import
        //-------------------------------------
        module_Matrix
        Matrix = import_Matrix();

        module_Assert
        Assert = import_Assert();
        //-------------------------------------



        int i_RowCount = 4;
        int i_ColumnCount = 4;

        float A[16] = 
        {0., 1., 1., 1.,
         1., 2., 0., 0.,
         0., 1., 2., 0.,
         2., 1., 1., 2.};
        float vector[4] = {1., 0., 2., 1.};
        float scalar = 2.;
        float expect1[4] = {2., 1., 4., 4.};
        float expect2[4] = {3., 2., 5., 3.};
        float expect3[4] = {3., 0., 6., 3.};
        float expect4[4] = {3., 0., 4., 4.};

        float *f_ExpectedArray[4];
        f_ExpectedArray[0] = &expect1[0];
        f_ExpectedArray[1] = &expect2[0];
        f_ExpectedArray[2] = &expect3[0];
        f_ExpectedArray[3] = &expect4[0];

        for (int i = 0; i < 4; i++)
        {
            puts("\n--------------------------");
            printf("\ntest case %d\n", i);

            Params_Matrix_AddScaledVectorToColumn_float
            params = 
            {
                .matrix = A,
                .matrix_row_count = i_RowCount,
                .matrix_column_count = i_ColumnCount,
                .row_id_eon = 0,
                .row_id_end = i_RowCount - 1,
                .column_id = i,
                .vector = vector,
                .vector_id_eon = 0,
                .vector_id_end = i_RowCount - 1,
                .scalar = scalar,
            };

            Matrix.add_scaled_vector_to_column_float(params);

            Params_Assert_MatrixColumnEqualVector_float
            params_assert = {
                .matrix = A,
                .matrix_row_count = i_RowCount,
                .matrix_column_count = i_ColumnCount,
                .row_id_eon = 0,
                .row_id_end = i_RowCount - 1,
                .column_id = i,
                .vector = f_ExpectedArray[i],
                .vector_length = i_RowCount,
                .tolerance = 1.E-5,
            };

            Assert.matrix_column_equal_vector_float(params_assert);
        }
        return true;
    }

Test Matrix.reset_float
    static bool
    test_reset_float(bool bool_RunTest)
    {
        if ( ! bool_RunTest ) { return true; }

        puts("\ntest_reset_float()\n");

        //-------------------------------------
        // Import
        //-------------------------------------
        module_Matrix
        Matrix = import_Matrix();

        module_Assert
        Assert = import_Assert();
        //-------------------------------------



        int i_RowCount = 4;
        int i_ColumnCount = 4;
        int matrix_length = i_RowCount * i_ColumnCount;

        float A[matrix_length];
        float new_value = 1.;
        float expect[16] = {0.};

        Class_MatrixIndex2D
        obj_Id = Matrix.new_MatrixIndex2D(i_RowCount, i_ColumnCount);

        for (int i= 0; i < i_RowCount; i++)
        {
            for (int j = 0; j < i_ColumnCount; j++)
            {
                int index = obj_Id.index(obj_Id, i, j);
                expect[index] = new_value;
            }
        }
        
        Params_Matrix_Reset_float
        params = Matrix.new_Params_Reset_float();
        params.matrix = A;
        params.matrix_row_count = i_RowCount;
        params.matrix_column_count = i_ColumnCount;
        params.value = new_value;
        Matrix.reset_float(params);

        Params_Assert_EqualMatrix_float
        params_assert = {
            .matrix1 = A,
            .matrix1_row_count = i_RowCount,
            .matrix1_column_count = i_ColumnCount,
            .matrix1_row_id_eon = 0,
            .matrix1_row_id_end = i_RowCount - 1,
            .matrix1_column_id_eon = 0,
            .matrix1_column_id_end = i_ColumnCount - 1,
            .matrix2 = expect,
            .matrix2_row_count = i_RowCount,
            .matrix2_column_count = i_ColumnCount,
            .matrix2_row_id_eon = 0,
            .matrix2_row_id_end = i_RowCount - 1,
            .matrix2_column_id_eon = 0,
            .matrix2_column_id_end = i_ColumnCount - 1,
            .tolerance = 1.E-5,
        };

        Assert.equal_matrix_float(params_assert);
        return true;
    }

Test Matrix.test_copy_vector_to_column_float
    static bool
    test_copy_vector_to_column_float(bool bool_RunTest)
    {
        if (!bool_RunTest) { return false; };
        puts("\ntest_copy_vector_to_column_float()\n");

        //-------------------------------------
        // Import
        //-------------------------------------
        module_Matrix
        Matrix = import_Matrix();

        module_Assert
        Assert = import_Assert();
        //-------------------------------------



        int i_RowCount = 4;
        int i_ColumnCount = 4;

        float A[16] = 
        {0., 1., 1., 1.,
         1., 2., 0., 0.,
         0., 1., 2., 0.,
         2., 1., 1., 2.};

        float vector[4] = {9., 9., 9., 9.};

        for (int j = 0; j < i_ColumnCount; j++)
        {
            Params_Matrix_CopyVectorToColumn_float
            params = Matrix.new_Params_CopyVectorToColumn_float();
            params.matrix = A;
            params.matrix_row_count = i_RowCount;
            params.matrix_column_count = i_ColumnCount;
            params.row_id_eon = 0;
            params.row_id_end = i_RowCount - 1;
            params.column_id = j,
            params.vector = vector;
            params.vector_length = i_RowCount;
            Matrix.copy_vector_to_column_float(params);


            Params_Assert_MatrixColumnEqualVector_float
            params_assert = {
                .matrix = A,
                .matrix_row_count = i_RowCount,
                .matrix_column_count = i_ColumnCount,
                .row_id_eon = 0,
                .row_id_end = i_RowCount - 1,
                .column_id = j,
                .vector = vector,
                .vector_length = i_RowCount,
                .tolerance = 1.E-5,
            };

            Assert.matrix_column_equal_vector_float(params_assert);
        }
        return true;
    }


Test Matrix.test_copy_vector_to_columns_float
    static bool
    test_copy_vector_to_columns_float(bool bool_RunTest)
    {
        if (!bool_RunTest) { return false; };
        puts("\ntest_copy_vector_to_columns_float()\n");

        //-------------------------------------
        // Import
        //-------------------------------------
        module_Matrix
        Matrix = import_Matrix();

        module_Assert
        Assert = import_Assert();
        //-------------------------------------



        int i_RowCount = 4;
        int i_ColumnCount = 4;

        float A[16] = 
        {0., 1., 1., 1.,
         1., 2., 0., 0.,
         0., 1., 2., 0.,
         2., 1., 1., 2.};

        float vector[8] = {9., 9., 9., 9., 9., 9., 9., 9.};
        int i_VectorLength = 8;

        // copy the vector to the first two columns
        Params_Matrix_CopyVectorToColumns_float
        params = Matrix.new_Params_CopyVectorToColumns_float();
        params.matrix = A;
        params.matrix_row_count = i_RowCount;
        params.matrix_column_count = i_ColumnCount;
        params.row_id_eon = 0;
        params.row_id_end = i_RowCount - 1;
        params.column_id_eon = 0,
        params.column_id_end = 1,
        params.vector = vector;
        params.vector_length = i_VectorLength;
        Matrix.copy_vector_to_columns_float(params);

        // expect changes to the first two columns
        float expect[4] = {9., 9., 9., 9.};
        for (int i = 0; i < 2; i++)
        {
            Params_Assert_MatrixColumnEqualVector_float
            params_assert = {
                .matrix = A,
                .matrix_row_count = i_RowCount,
                .matrix_column_count = i_ColumnCount,
                .row_id_eon = 0,
                .row_id_end = i_RowCount - 1,
                .column_id = i,
                .vector = expect,
                .vector_length = i_RowCount,
                .tolerance = 1.E-5,
            };
            Assert.matrix_column_equal_vector_float(params_assert);
        }

        //expect no changes to other columns
        float expect1[4] = {1., 0., 2., 1.};
        float expect2[4] = {1., 0., 0., 2.};
        float *expected[2];
        expected[0] = expect1;
        expected[1] = expect2;

        int column_index_offset = 2;
        for (int i = 0; i < 2; i++)
        {
            Params_Assert_MatrixColumnEqualVector_float
            params_assert = {
                .matrix = A,
                .matrix_row_count = i_RowCount,
                .matrix_column_count = i_ColumnCount,
                .row_id_eon = 0,
                .row_id_end = i_RowCount - 1,
                .column_id = i + column_index_offset,
                .vector = expected[i],
                .vector_length = i_RowCount,
                .tolerance = 1.E-5,
            };
            Assert.matrix_column_equal_vector_float(params_assert);
        }

        return true;
    }


Test Matrix.test_copy_column_to_vector_float
    static bool
    test_copy_column_to_vector_float(bool bool_RunTest)
    {
        if (!bool_RunTest) { return false; };
        puts("\ntest_copy_column_to_vector_float()\n");

        //-------------------------------------
        // Import
        //-------------------------------------
        module_Matrix
        Matrix = import_Matrix();

        module_Vector
        Vector = import_Vector();

        module_Assert
        Assert = import_Assert();
        //-------------------------------------



        int i_RowCount = 4;
        int i_ColumnCount = 4;

        float A[16] = 
        {0., 1., 1., 1.,
         1., 2., 0., 0.,
         0., 1., 2., 0.,
         2., 1., 1., 2.};

        int number_of_tests = 2;
        float expect0[4] = {0., 1., 0., 2.};
        float expect1[4] = {1., 2., 1., 1.};
        float *f_ExpectedArray[2];
        f_ExpectedArray[0] = &expect0[0];
        f_ExpectedArray[1] = &expect1[0];

        for (int j = 0; j < number_of_tests; j++)
        {
            int i_ColumnId = j;
            int vector_length = i_RowCount;
            float vector[vector_length];

            Params_Matrix_CopyColumnToVector_float
            params = Matrix.new_Params_CopyColumnToVector_float();
            params.matrix = A;
            params.matrix_row_count = i_RowCount;
            params.matrix_column_count = i_ColumnCount;
            params.row_id_eon = 0;
            params.row_id_end = i_RowCount - 1;
            params.column_id = i_ColumnId,
            params.vector_OUT = vector;
            params.vector_length = vector_length;
            Matrix.copy_column_to_vector_float(params);

            float *answer = vector;
            float *expect = f_ExpectedArray[j];
            Params_Assert_EqualArray_float
            params_assert = {
                .input1 = answer,
                .input2 = expect,
                .length = vector_length,
                .tolerance = 1.E-5,
            };
            Assert.equal_array_float(params_assert);

            Params_Vector_Print_float
            params_print_vector = {
                .vector = NULL,
                .length = vector_length,
                .precision = 3,
            };

            puts("answer: ");
            params_print_vector.vector = answer;
            Vector.print_float(params_print_vector);

            puts("expect: ");
            params_print_vector.vector = expect;
            Vector.print_float(params_print_vector);
        }
        return true;
    }

Test Matrix.test_copy_columns_to_vector_float
    static bool
    test_copy_columns_to_vector_float(bool bool_RunTest)
    {
        if (!bool_RunTest) { return false; };
        puts("\ntest_copy_columns_to_vector_float()\n");

        //-------------------------------------
        // Import
        //-------------------------------------
        module_Matrix
        Matrix = import_Matrix();

        module_Vector
        Vector = import_Vector();

        module_Assert
        Assert = import_Assert();
        //-------------------------------------



        int i_RowCount = 4;
        int i_ColumnCount = 4;

        float A[16] = 
        {0., 1., 1., 1.,
         1., 2., 0., 0.,
         0., 1., 2., 0.,
         2., 1., 1., 2.};

        int number_of_tests = 2;
        float expect0[4] = {0., 1., 0., 2.};
        float expect1[8] = {0., 1., 0., 2., 1., 2., 1., 1.};
        float *f_ExpectedArray[2];
        f_ExpectedArray[0] = &expect0[0];
        f_ExpectedArray[1] = &expect1[0];

        for (int j = 0; j < number_of_tests; j++)
        {
            int i_ColumnId = j;
            int vector_length = i_RowCount  * (j + 1);
            float vector[vector_length];

            Params_Matrix_CopyColumnsToVector_float
            params = Matrix.new_Params_CopyColumnsToVector_float();
            params.matrix = A;
            params.matrix_row_count = i_RowCount;
            params.matrix_column_count = i_ColumnCount;
            params.row_id_eon = 0;
            params.row_id_end = i_RowCount - 1;
            params.column_id_eon = 0,
            params.column_id_end = i_ColumnId,
            params.vector_OUT = vector;
            params.vector_length = vector_length;
            Matrix.copy_columns_to_vector_float(params);

            float *answer = vector;
            float *expect = f_ExpectedArray[j];
            Params_Assert_EqualArray_float
            params_assert = {
                .input1 = answer,
                .input2 = expect,
                .length = vector_length,
                .tolerance = 1.E-5,
            };
            Assert.equal_array_float(params_assert);

            Params_Vector_Print_float
            params_print_vector = {
                .vector = NULL,
                .length = vector_length,
                .precision = 3,
            };

            puts("answer: ");
            params_print_vector.vector = answer;
            Vector.print_float(params_print_vector);

            puts("expect: ");
            params_print_vector.vector = expect;
            Vector.print_float(params_print_vector);
        }
        return true;
    }

Test Matrix.write_text_file_float
    static bool
    test_write_text_file_float(bool bool_RunTest)
    {
        if ( ! bool_RunTest ) { return true; }

        puts("\ntest_write_text_file_float()\n");

        //-------------------------------------
        // imports
        //-------------------------------------
        module_Matrix
        Matrix = import_Matrix();
        //-------------------------------------

        int i_RowCount = 2;
        int i_ColumnCount = 2;
        float 
        A[4] = 
        {1., 0.,
         0., 1.};

        char *file_name = "answer_2x2.txt";

        Params_Matrix_WriteTextFile_float
        params = Matrix.new_Params_WriteTextFile_float();
        params.file_name = file_name;
        params.matrix = A;
        params.matrix_row_count = i_RowCount;
        params.matrix_column_count = i_ColumnCount;
        params.precision = 1;
        params.scientific = false;
        Matrix.write_text_file_float(params);

        return true;
    }