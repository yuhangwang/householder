# Module module_Matrix
This module contains tools for manipulating matrices.




#[module_Matrix.dep.h](#module_Matrix.dep.h "save:")

    _"LICENSE"

    #include "module_Type.h"

#[module_Matrix.h](#module_Matrix.h "save:")

    _"LICENSE"

    #ifndef MODULE_MATRIX_H
    #define MODULE_MATRIX_H

    #include "module_Matrix.dep.h"

    typedef struct 
    Params_Matrix_Print
    {
        void *matrix;
        int   matrix_row_count;
        int   matrix_column_count;
        int   precision;
        MyTypeChoice type;
    }
    Params_Matrix_Print;

    typedef struct 
    Params_Matrix_Print_float
    {
        float *matrix;
        int    matrix_row_count;
        int    matrix_column_count;
        int    precision;
    }
    Params_Matrix_Print_float;
    //------------------------------------------------    

    //------------------------------------------------
    // class for manipulating matrix indices
    // for 2D array that is stored as 1D array.
    //------------------------------------------------
    typedef struct
    Class_MatrixIndex2D
    {   int matrix_row_count;
        int matrix_column_count;
        int length; // length of the 1D representation
        int (*index)(struct Class_MatrixIndex2D self, int i,int j);
    }
    Class_MatrixIndex2D;

    //------------------------------------------------
    //  parameters for o2_norm_column
    //------------------------------------------------
    /*
     * Calculate the order-2 norm of selected matrix columns
     */
    typedef struct 
    Params_Matrix_o2NormColumn_float
    {
        float *matrix;
        int    matrix_row_count;   // row count of the matrix
        int    matrix_column_count; // column count of the matrix
        int    row_id_eon; // starting row ID
        int    row_id_end; // ending row ID
        int    column_id;  // corresponding column ID
        float *result_OUT; // pointer to the result storage
    }
    Params_Matrix_o2NormColumn_float;
    //------------------------------------------------

    //------------------------------------------------
    //  parameters for o2_norm_column
    //------------------------------------------------
    /*
     * Calculate the order-2 norm of selected matrix columns
     */
    typedef struct 
    Params_Matrix_o2NormColumns_float
    {
        float *matrix;
        int    matrix_row_count;   // row count of the matrix
        int    matrix_column_count; // column count of the matrix
        int   *row_ids_eon; // starting row IDs
        int   *row_ids_end; // ending row IDs
        int   *column_ids;  // corresponding column IDs
        int    length;      // length of each of the above arrays, e.g. 2
        float *results_OUT; // pointer to the result storage
    }
    Params_Matrix_o2NormColumns_float;
    //------------------------------------------------

    //------------------------------------------------
    // parameters for matrix column dot product
    //------------------------------------------------
    typedef struct 
    Params_Matrix_DotColumn_float
    {
        float *matrix;
        int    matrix_row_count;
        int    matrix_column_count;
        int    row_id_eon;
        int    row_id_end;
        float *vector;
        int    vector_length;
        int    column_id;
        float *result_OUT;
    }
    Params_Matrix_DotColumn_float;

    //------------------------------------------------
    // parameters for adding a scaled vector to a 
    // matrix column (in place)
    //------------------------------------------------
    typedef struct 
    Params_Matrix_AddScaledVectorToColumn_float
    {
        float *matrix;
        int    matrix_row_count;
        int    matrix_column_count;
        int    row_id_eon;
        int    row_id_end;
        int    column_id;
        float *vector;
        int    vector_id_eon;
        int    vector_id_end;
        float  scalar;
    }
    Params_Matrix_AddScaledVectorToColumn_float;

    //-----------------------------
    // parameters for Matrix.reset()
    //-----------------------------
    typedef struct 
    Params_Matrix_Reset_float
    {
        float *matrix;
        int    matrix_row_count;
        int    matrix_column_count;
        float  value;
    }
    Params_Matrix_Reset_float;

    //-----------------------------
    // parameters for Matrix.copy_vector_to_column_float()
    //-----------------------------
    typedef struct 
    Params_Matrix_CopyVectorToColumn_float
    {
        float *matrix;
        int    matrix_row_count;
        int    matrix_column_count;
        int    row_id_eon;
        int    row_id_end;
        int    column_id;
        float *vector;
        int    vector_length;
    }
    Params_Matrix_CopyVectorToColumn_float;

    //-----------------------------
    // parameters for Matrix.copy_vector_to_columns_float()
    //-----------------------------
    typedef struct 
    Params_Matrix_CopyVectorToColumns_float
    {
        float *matrix;
        int    matrix_row_count;
        int    matrix_column_count;
        int    row_id_eon;
        int    row_id_end;
        int    column_id_eon;
        int    column_id_end;
        float *vector;
        int    vector_length;
    }
    Params_Matrix_CopyVectorToColumns_float;

    //-----------------------------
    // parameters for Matrix.copy_column_to_vector_float()
    //-----------------------------
    typedef struct 
    Params_Matrix_CopyColumnToVector_float
    {
        float *matrix;
        int    matrix_row_count;
        int    matrix_column_count;
        int    row_id_eon;
        int    row_id_end;
        int    column_id;
        float *vector_OUT;
        int    vector_length;
    }
    Params_Matrix_CopyColumnToVector_float;

    //-----------------------------
    // parameters for Matrix.copy_column_to_vector_float()
    //-----------------------------
    typedef struct 
    Params_Matrix_CopyColumnsToVector_float
    {
        float *matrix;
        int    matrix_row_count;
        int    matrix_column_count;
        int    row_id_eon;
        int    row_id_end;
        int    column_id_eon;
        int    column_id_end;
        float *vector_OUT;
        int    vector_length;
    }
    Params_Matrix_CopyColumnsToVector_float;

    //------------------------------------------------
    // parameters for Matrix.write_text_file_float()
    //------------------------------------------------
    typedef struct 
    Params_Matrix_WriteTextFile_float
    {
        char *file_name;
        float *matrix;
        int    matrix_row_count;
        int    matrix_column_count;
        int    precision;
        bool   scientific;
    }
    Params_Matrix_WriteTextFile_float;

    //------------------------------------------------
    /**************************************************/  
    /*************  Module Matrix Interface ***********/
    /**************************************************/    
    typedef struct 
    module_Matrix
    {
        void (*hi)(void);
        void (*print)(Params_Matrix_Print);
        void (*print_float)(Params_Matrix_Print_float);
        void (*o2_norm_column_float)(Params_Matrix_o2NormColumn_float);
        void (*o2_norm_columns_float)(Params_Matrix_o2NormColumns_float);
        void (*dot_column_float)(Params_Matrix_DotColumn_float);
        void (*add_scaled_vector_to_column_float)(Params_Matrix_AddScaledVectorToColumn_float);
        void (*reset_float)(Params_Matrix_Reset_float);
        void (*copy_vector_to_column_float)(Params_Matrix_CopyVectorToColumn_float);
        void (*copy_vector_to_columns_float)(Params_Matrix_CopyVectorToColumns_float);
        void (*copy_column_to_vector_float)(Params_Matrix_CopyColumnToVector_float);
        void (*copy_columns_to_vector_float)(Params_Matrix_CopyColumnsToVector_float);
        void (*write_text_file_float)(Params_Matrix_WriteTextFile_float);
        Class_MatrixIndex2D (*new_MatrixIndex2D)(int matrix_row_count, int matrix_column_count);
        Params_Matrix_o2NormColumn_float  (*new_Params_o2NormColumn_float)(void);
        Params_Matrix_o2NormColumns_float (*new_Params_o2NormColumns_float)(void);
        Params_Matrix_DotColumn_float (*new_Params_DotColumn_float)(void);
        Params_Matrix_Reset_float (*new_Params_Reset_float)(void);
        Params_Matrix_CopyVectorToColumn_float (*new_Params_CopyVectorToColumn_float)(void);
        Params_Matrix_CopyVectorToColumns_float (*new_Params_CopyVectorToColumns_float)(void);
        Params_Matrix_CopyColumnToVector_float (*new_Params_CopyColumnToVector_float)(void);
        Params_Matrix_CopyColumnsToVector_float (*new_Params_CopyColumnsToVector_float)(void);
        Params_Matrix_WriteTextFile_float (*new_Params_WriteTextFile_float)(void);
    }
    module_Matrix;

    module_Matrix
    import_Matrix(void);

    #endif
    /**************************************************/    
    //------------------------------------------------    





#[module_Matrix.dep.c](#module_Matrix.dep.c "save:")

    _"LICENSE"

    #include "module_Matrix.h"
    #include <stdio.h>
    #include <assert.h>
    #include <math.h>

#[module_Matrix.c](#module_Matrix.c "save:")

    _"LICENSE"

    #include "module_Matrix.dep.c"

    _"module_Matrix parameter initializer"

    _"module_Matrix worker functions"

    module_Matrix
    import_Matrix(void)
    {
        module_Matrix obj = {
            .hi = hi,
            .print = print,
            .print_float = print_float,
            .o2_norm_column_float = o2_norm_column_float,
            .o2_norm_columns_float = o2_norm_columns_float,
            .dot_column_float = dot_column_float,
            .add_scaled_vector_to_column_float = add_scaled_vector_to_column_float,
            .reset_float = reset_float,
            .copy_vector_to_column_float = copy_vector_to_column_float,
            .copy_vector_to_columns_float = copy_vector_to_columns_float,
            .copy_column_to_vector_float = copy_column_to_vector_float,
            .copy_columns_to_vector_float = copy_columns_to_vector_float,
            .write_text_file_float = write_text_file_float,
            .new_MatrixIndex2D = new_MatrixIndex2D,
            .new_Params_o2NormColumn_float = new_Params_o2NormColumn_float,
            .new_Params_o2NormColumns_float = new_Params_o2NormColumns_float,
            .new_Params_DotColumn_float = new_Params_DotColumn_float,
            .new_Params_Reset_float = new_Params_Reset_float,
            .new_Params_CopyVectorToColumn_float = new_Params_CopyVectorToColumn_float,
            .new_Params_CopyVectorToColumns_float = new_Params_CopyVectorToColumns_float,
            .new_Params_CopyColumnToVector_float = new_Params_CopyColumnToVector_float,
            .new_Params_CopyColumnsToVector_float = new_Params_CopyColumnsToVector_float,
            .new_Params_WriteTextFile_float = new_Params_WriteTextFile_float,
        };

        return obj;
    }

## module_Matrix parameter initializer

Default parameter creator for Params_Matrix_o2NormColumn_float
    static Params_Matrix_o2NormColumn_float
    new_Params_o2NormColumn_float(void)
    {
        Params_Matrix_o2NormColumn_float
        obj = {
            .matrix = NULL,
            .row_id_eon = -1,
            .row_id_end = -1,
            .column_id  = -1,
            .matrix_row_count  = -1,
            .matrix_column_count = -1,
            .result_OUT = NULL,
        };
        return obj;
    }

Default parameter creator for Params_Matrix_o2NormColumns_float
    static Params_Matrix_o2NormColumns_float
    new_Params_o2NormColumns_float(void)
    {
        Params_Matrix_o2NormColumns_float
        obj = {
            .matrix = NULL,
            .row_ids_eon = NULL,
            .row_ids_end = NULL,
            .column_ids  = NULL,
            .matrix_row_count  = -1,
            .matrix_column_count = -1,
            .results_OUT = NULL,
        };
        return obj;
    }

Default parameter creator for Params_Matrix_DotColumn_float
    static Params_Matrix_DotColumn_float
    new_Params_DotColumn_float(void)
    {
        Params_Matrix_DotColumn_float
        obj = {
            .matrix = NULL,
            .matrix_row_count = -1,
            .matrix_column_count = -1,
            .column_id = -1,
            .vector = NULL,
            .vector_length = -1,
            .result_OUT = NULL,
        };
        return obj;
    }

Default parameter creator for Params_Matrix_Reset_float
    static Params_Matrix_Reset_float
    new_Params_Reset_float(void)
    {
        Params_Matrix_Reset_float
        obj = {
            .matrix = NULL,
            .matrix_row_count = -1,
            .matrix_column_count = -1,
            .value = 0.,
        };
        return obj;
    }

Default parameter creator for Params_Matrix_CopyVectorToColumn_float
    static Params_Matrix_CopyVectorToColumn_float
    new_Params_CopyVectorToColumn_float(void)
    {
        Params_Matrix_CopyVectorToColumn_float
        obj = {
            .matrix = NULL,
            .matrix_row_count = -1,
            .matrix_column_count = -1,
            .row_id_eon = -1,
            .row_id_end = -1,
            .column_id = -1,
            .vector = NULL,
            .vector_length = -1,
        };
        return obj;
    }

Default parameter creator for Params_Matrix_CopyVectorToColumns_float
    static Params_Matrix_CopyVectorToColumns_float
    new_Params_CopyVectorToColumns_float(void)
    {
        Params_Matrix_CopyVectorToColumns_float
        obj = {
            .matrix = NULL,
            .matrix_row_count = -1,
            .matrix_column_count = -1,
            .row_id_eon = -1,
            .row_id_end = -1,
            .column_id_eon = -1,
            .column_id_end = -1,
            .vector = NULL,
            .vector_length = -1,
        };
        return obj;
    }


Default parameter creator for Params_Matrix_CopyColumnToVector_float
    static Params_Matrix_CopyColumnToVector_float
    new_Params_CopyColumnToVector_float(void)
    {
        Params_Matrix_CopyColumnToVector_float
        obj = {
            .matrix = NULL,
            .matrix_row_count = -1,
            .matrix_column_count = -1,
            .row_id_eon = -1,
            .row_id_end = -1,
            .column_id = -1,
            .vector_OUT = NULL,
            .vector_length = -1,
        };
        return obj;
    }


Default parameter creator for Params_Matrix_CopyColumnsToVector_float
    static Params_Matrix_CopyColumnsToVector_float
    new_Params_CopyColumnsToVector_float(void)
    {
        Params_Matrix_CopyColumnsToVector_float
        obj = {
            .matrix = NULL,
            .matrix_row_count = -1,
            .matrix_column_count = -1,
            .row_id_eon = -1,
            .row_id_end = -1,
            .column_id_eon = -1,
            .column_id_end = -1,
            .vector_OUT = NULL,
            .vector_length = -1,
        };
        return obj;
    }

Default parameter creator for Params_Matrix_WriteTextFile_float.
    static Params_Matrix_WriteTextFile_float
    new_Params_WriteTextFile_float(void)
    {
        Params_Matrix_WriteTextFile_float
        obj = {
            .file_name = NULL,
            .matrix = NULL,
            .matrix_row_count = -1,
            .matrix_column_count = -1,
            .precision = 3,
            .scientific = true,
        };
        return obj;
    }


## module_Matrix worker functions

A hello-world function.

    static void 
    hi(void)
    {
    	printf("Greetings from module_Matrix!\n");
    }

Function print_float.
    //---------------------------------------------
    /* 
     * function: print_float()
     */
    //---------------------------------------------
    static void
    print_float(Params_Matrix_Print_float params)
    {
        int module_Matrix_print_float__input_matrix_row_count = params.matrix_row_count;
        int module_Matrix_print_float__input_column_count = params.matrix_column_count;
        assert(module_Matrix_print_float__input_matrix_row_count > 0);
        assert(module_Matrix_print_float__input_column_count > 0);

        for (int i = 0; i < params.matrix_row_count; i++)
        {
            for (int j = 0; j < params.matrix_column_count; j++)
            {
                int index = (i * params.matrix_column_count) + j;

                printf("%.*f ", 
                    params.precision,
                    params.matrix[index]);
            }
            puts("");
        }
    }
    //---------------------------------------------


    static void
    print(Params_Matrix_Print params)
    {
        module_Type
        Type = import_Type();

        if (Type.same(params.type, Type.Float))
        {
            float *A = (float*) params.matrix;

            Params_Matrix_Print_float
            inputs = {
                .matrix = &A[0],
                .matrix_row_count = params.matrix_row_count,
                .matrix_column_count = params.matrix_column_count,
                .precision = params.precision,
            };

            print_float(inputs);
        }
    }

    //---------------------------------------------
    
    //---------------------------------------------
    // Class for calculating 2D matrix index 
    // in its 1D storage.
    //---------------------------------------------
    static int 
    get_index(Class_MatrixIndex2D self, int i, int j)
    {
        int MatrixIndex2D_requested_index = (i * self.matrix_column_count) + j;
        int matrix_length_1D = self.length;
        assert(MatrixIndex2D_requested_index < matrix_length_1D);
        return MatrixIndex2D_requested_index;
    }

    Class_MatrixIndex2D
    new_MatrixIndex2D(int matrix_row_count, int matrix_column_count)
    {
        int new_MatrixIndex2D_input1__matrix_row_count = matrix_row_count;
        int new_MatrixIndex2D_input2__column_count = matrix_column_count;
        assert(new_MatrixIndex2D_input1__matrix_row_count > 0);
        assert(new_MatrixIndex2D_input2__column_count > 0);

        Class_MatrixIndex2D
        obj = {
            .matrix_row_count = matrix_row_count,
            .matrix_column_count = matrix_column_count,
            .length = matrix_row_count * matrix_column_count,
            .index = get_index,
        };

        return obj;
    }

    //---------------------------------------------


Order-2 norm for one matrix column   
    //---------------------------------------------
    // o2_norm_column_float
    //---------------------------------------------
    static void
    o2_norm_column_float(Params_Matrix_o2NormColumn_float params)
    {
        Class_MatrixIndex2D
        obj_Id = new_MatrixIndex2D(
            params.matrix_row_count, 
            params.matrix_column_count);

        int row_id_eon  = params.row_id_eon;
        int row_id_end  = params.row_id_end;
        int column_id   = params.column_id;
        float *o2_norm  = params.result_OUT;

        /********************************************/
        // o2_norm will accumulate the 2-norm of the column
        *o2_norm = 0.;
        /********************************************/
        
        for (int i = row_id_eon; i <= row_id_end; i++)
        {
            int index = obj_Id.index(obj_Id, i, column_id);
            float element = params.matrix[index];
            *o2_norm = *o2_norm + (element * element);
        }

        //---------------------------------
        // make sure *o2_norm is non-negative
        // before doing sqrt()
        //---------------------------------
        int module_Matrix_o2_norm_column_float_internal_varialbe_o2_norm
         = *o2_norm;
        assert(module_Matrix_o2_norm_column_float_internal_varialbe_o2_norm >= 0.);
        //---------------------------------
        
        *o2_norm = sqrt(*o2_norm);
    }

Order-2 norm for matrix columns    
    //---------------------------------------------
    // o2_norm_column_float
    //---------------------------------------------
    static void
    o2_norm_columns_float(Params_Matrix_o2NormColumns_float params)
    {
        for (int k = 0; k < params.length; k++)
        {
            Params_Matrix_o2NormColumn_float
            inputs = {
                .matrix = params.matrix,
                .row_id_eon = params.row_ids_eon[k],
                .row_id_end = params.row_ids_end[k],
                .column_id  = params.column_ids[k],
                .matrix_row_count = params.matrix_row_count,
                .matrix_column_count = params.matrix_column_count,
                .result_OUT = &params.results_OUT[k],
            };

            o2_norm_column_float(inputs);
        }
    }

Dot product between a vector and one matrix column.
    static void
    dot_column_float(Params_Matrix_DotColumn_float params)
    {
        float *A = params.matrix;
        float *vector = params.vector;
        int vector_length = params.vector_length;

        int row_id_eon = params.row_id_eon;
        int row_id_end = params.row_id_end;
        int column_id  = params.column_id;
        int i_RowCount = params.matrix_row_count;
        int i_ColumnCount = params.matrix_column_count;
        float *p_Output  = params.result_OUT;

        Class_MatrixIndex2D 
        obj_Id = new_MatrixIndex2D(i_RowCount, i_ColumnCount);

        /**********************************************/
        // make sure *p_Output starts from zero
        /**********************************************/
        *p_Output = 0.;
        /**********************************************/

        for (int i = row_id_eon; i <= row_id_end; i++)
        {
            int index = obj_Id.index(obj_Id, i, column_id);
            float matrix_item = A[index];
            float vector_item = *vector;
            vector++;// move on to the next

            *p_Output = *p_Output + (matrix_item * vector_item);

            /************************************************/
            // SAFETY CHECK
            /************************************************/
            int module_Matrix_dot_column_float_for_loop_i = i - row_id_eon;
            assert(module_Matrix_dot_column_float_for_loop_i < vector_length);
            /************************************************/
        }
    }

Function for adding a scaled vector to a matrix column.
The input vector will not be modified.
    static void
    add_scaled_vector_to_column_float(Params_Matrix_AddScaledVectorToColumn_float params)
    {
        Class_MatrixIndex2D
        obj_Id = new_MatrixIndex2D(params.matrix_row_count, params.matrix_column_count);

        int row_id_range_length = params.row_id_end - params.row_id_eon + 1;
        int vector_id_range_length = params.vector_id_end - params.vector_id_eon + 1;
        assert(row_id_range_length == vector_id_range_length);

        for (int i = 0; i < row_id_range_length; i++)
        {
            int i_RowId = i + params.row_id_eon;
            int matrix_index = obj_Id.index(obj_Id, i_RowId, params.column_id);
            int vector_index = i + params.vector_id_eon;
            params.matrix[matrix_index] += (params.scalar * params.vector[vector_index]);
        }
    }

Function for setting matrix element to a new value.
    static void 
    reset_float(Params_Matrix_Reset_float params)
    {
        Class_MatrixIndex2D
        obj_Id = new_MatrixIndex2D(params.matrix_row_count, params.matrix_column_count);

        for (int i = 0; i < params.matrix_row_count; i++)
        {
            for (int j = 0; j < params.matrix_column_count; j++)
            {
                int index = obj_Id.index(obj_Id, i, j);
                params.matrix[index] = params.value;
            }
        }        
    }

Function for copying a vector the column of a matrix.
    static void 
    copy_vector_to_column_float(Params_Matrix_CopyVectorToColumn_float params)
    {
        int row_id_range_length = params.row_id_end - params.row_id_eon + 1;
        int Matrix_copy_vector_to_column_float__row_id_range_length = row_id_range_length;
        assert(Matrix_copy_vector_to_column_float__row_id_range_length == params.vector_length);

        Class_MatrixIndex2D
        obj_Id = new_MatrixIndex2D(params.matrix_row_count, params.matrix_column_count);

        for (int i = 0; i < params.vector_length; i++)
        {
            int i_RowId = params.row_id_eon + i;
            int index = obj_Id.index(obj_Id, i_RowId, params.column_id);
            float value = *params.vector;
            params.matrix[index] = value;
            params.vector++;
        }        
    }

Function for copying a vector the columns of a matrix.
    static void 
    copy_vector_to_columns_float(Params_Matrix_CopyVectorToColumns_float params)
    {
        int row_id_range_length = params.row_id_end - params.row_id_eon + 1;
        int column_id_range_length = params.column_id_end - params.column_id_eon + 1;
        int Matrix_copy_vector_to_column_float__required_vector_length = 
            column_id_range_length * row_id_range_length;
        assert(Matrix_copy_vector_to_column_float__required_vector_length == params.vector_length);
        
        Params_Matrix_CopyVectorToColumn_float
        inputs = new_Params_CopyVectorToColumn_float();
        inputs.matrix = params.matrix;
        inputs.matrix_row_count = params.matrix_row_count;
        inputs.matrix_column_count = params.matrix_column_count;
        inputs.row_id_eon = params.row_id_eon;
        inputs.row_id_end = params.row_id_end;

        for (int k = 0; k < column_id_range_length; k++)
        { 
            int index_offset = k * row_id_range_length;
            float *vector = &params.vector[index_offset];
            int i_ColumnId = k + params.column_id_eon;
            inputs.column_id = i_ColumnId;
            inputs.vector = vector;
            inputs.vector_length = row_id_range_length;
            copy_vector_to_column_float(inputs);
        }
    }

Function for copying a column of a matrix to a vector.
    static void 
    copy_column_to_vector_float(Params_Matrix_CopyColumnToVector_float params)
    {
        int row_id_range_length = params.row_id_end - params.row_id_eon + 1;
        int Matrix_copy_column_to_vector_float__row_id_range_length = row_id_range_length;
        assert(Matrix_copy_column_to_vector_float__row_id_range_length == params.vector_length);

        Class_MatrixIndex2D
        obj_Id = new_MatrixIndex2D(params.matrix_row_count, params.matrix_column_count);

        for (int i = 0; i < params.vector_length; i++)
        {
            int i_RowId = params.row_id_eon + i;
            int index = obj_Id.index(obj_Id, i_RowId, params.column_id);
            float value = params.matrix[index];
            *params.vector_OUT= value;
            params.vector_OUT++;
        }        
    }


Function for copying a columns of a matrix to a vector.
    static void 
    copy_columns_to_vector_float(Params_Matrix_CopyColumnsToVector_float params)
    {
        int column_id_range_length = params.column_id_end - params.column_id_eon + 1;
        int required_vector_length = column_id_range_length * params.matrix_row_count;
        int Matrix_copy_columns_to_vector_float__required_vector_length = 
            required_vector_length;
        assert(Matrix_copy_columns_to_vector_float__required_vector_length == params.vector_length);

        int column_length = params.row_id_end - params.row_id_eon + 1;

        for (int j = params.column_id_eon; j <= params.column_id_end; j++)
        {
            int i_ColumnId = j;

            Params_Matrix_CopyColumnToVector_float
            inputs = new_Params_CopyColumnToVector_float();
            inputs.matrix = params.matrix;
            inputs.matrix_column_count = params.matrix_column_count;
            inputs.matrix_row_count = params.matrix_row_count;
            inputs.row_id_eon = params.row_id_eon;
            inputs.row_id_end = params.row_id_end;
            inputs.column_id = i_ColumnId;
            inputs.vector_OUT  = params.vector_OUT;
            inputs.vector_length = column_length;
            copy_column_to_vector_float(inputs);

            //shift output vector pointer position by one column length
            for (int i = 0; i < column_length; i++)
            { params.vector_OUT++; }
        }        
    }

A function for writing a matrix to a text file.
    static void 
    write_text_file_float(Params_Matrix_WriteTextFile_float params)
    {
        FILE *p_File = fopen(params.file_name, "w");

        float *A = params.matrix; // alias
        bool  bool_UseScientificNotation = params.scientific;

        Class_MatrixIndex2D
        obj_Id = new_MatrixIndex2D(
            params.matrix_row_count, params.matrix_column_count);

        for ( int i = 0; i < params.matrix_row_count; i++ )
        {
            for ( int j = 0; j < params.matrix_column_count; j++ )
            {
                int index = obj_Id.index(obj_Id, i, j);
                if ( bool_UseScientificNotation )
                {
                    fprintf(p_File, "%.*e", params.precision, A[index]);
                }
                else
                {
                    fprintf(p_File, "%.*f", params.precision, A[index]);
                }

                if ( j != (params.matrix_column_count - 1) )
                    { fprintf(p_File, " "); }
            }
            fprintf(p_File, "\n");
        }

        fclose(p_File);
    }