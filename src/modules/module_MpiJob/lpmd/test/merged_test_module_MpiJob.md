# LICENSE
    /**********************************************************
     Copyright (c) 2015 YUHANG WANG 
     ALL RIGHTS RESERVED

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
    ************************************************************/



# LICENSE
    /**********************************************************
     Copyright (c) 2015 YUHANG WANG 
     ALL RIGHTS RESERVED

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
    ************************************************************/



# Test module_MpiJob 


#[test_module_MpiJob.dep.h](#test_module_MpiJob.dep.h "save:")

    _"LICENSE"

#[test_module_MpiJob.h](#test_module_MpiJob.h "save:")
    
    _"LICENSE"

    #include "test_module_MpiJob.dep.h"


#[test_module_MpiJob.dep.c](#test_module_MpiJob.dep.c "save:")
        
    _"LICENSE" 

    #include "test_module_MpiJob.h"
    #include "module_MpiJob.h"
    #include "module_Assert.h"
    #include "module_Vector.h"
    #include "mpi.h"
    #include <stdbool.h>
    #include <stdio.h>
    #include <assert.h>


#[test_module_MpiJob.c](#test_module_MpiJob.c "save:")

    _"LICENSE"

    #include "test_module_MpiJob.dep.c"

    _"test_module_MpiJob all tests"

    int
    main (int i_ArgumentCount, char *c_ArgumentArray[])
    {
        MPI_Init(&i_ArgumentCount, &c_ArgumentArray);

        MPI_Comm mpi_communicator = MPI_COMM_WORLD;

        //------------------------------
        test_task_count_for_me_block_1D(false, mpi_communicator);
        //------------------------------
        test_task_count_for_all_block_1D(false, mpi_communicator);
        //------------------------------
        test_task_id_range_for_me_block_1D(false, mpi_communicator);
        //------------------------------
        test_task_ids_for_me_block_1D(false, mpi_communicator);
        //------------------------------
        test_task_ids_for_me_cyclic_1D(false, mpi_communicator);
        //------------------------------
        test_is_my_task(false, mpi_communicator);
        //------------------------------
        test_Class_RankIdBlock1D(false, mpi_communicator);
        //------------------------------
        test_Class_RankIdCyclic1D(true, mpi_communicator);
        //------------------------------
        test_gatherv_displacements_block_1D(false, mpi_communicator);
        //------------------------------

        MPI_Finalize();

        return 0;
    }

## test_module_MpiJob all tests

Test MpiJob.task_count_for_me_block_1D.

    static bool
    test_task_count_for_me_block_1D(
        bool bool_RunTest, 
        MPI_Comm mpi_communicator)
    {
        if (!bool_RunTest) { return false; }

        puts("\n test_task_count_for_me_block_1D() \n");
        
        //-----------------------------------
        // import
        //-----------------------------------
        module_MpiJob
        MpiJob = import_MpiJob();

        module_Assert
        Assert = import_Assert();

        //------------------------------------------------------
        //                     MPI INFO
        //------------------------------------------------------
        int i_MPI_ThisRank = -1; // MPI rank of this process
        int i_MPI_Volume   = -1; // MPI process count

        MPI_Comm_rank(mpi_communicator, &i_MPI_ThisRank);
        MPI_Comm_size(mpi_communicator, &i_MPI_Volume);
        //------------------------------------------------------
         
        if (i_MPI_ThisRank == 0)
        {
            printf("\n\t *** MPI this rank = %d/%d ***\n\n", i_MPI_ThisRank, i_MPI_Volume);

            int mpi_volume = 3;
            int mpi_total_task_count = 10;
            int i_ExpectedArray[3] = {4,3,3};

            for (int i = 0; i < mpi_volume; i++)
            {
                int this_rank = i;
                int answer = -1;

                Params_MpiJob_TaskCountForMeBlock1D
                params = MpiJob.new_Params_TaskCountForMeBlock1D();
                params.mpi_volume = mpi_volume;
                params.mpi_this_rank = this_rank;
                params.mpi_total_task_count = mpi_total_task_count;
                params.result_OUT = &answer;
                MpiJob.task_count_for_me_block_1D(params);

                int expect = i_ExpectedArray[i];

                printf("answer: %d\n", answer);
                printf("expect: %d\n", expect);

                Params_Assert_Equal_int 
                params_assert = {
                    .input1 = answer,
                    .input2 = expect,
                };

                Assert.equal_int(params_assert);
            }

        }
        return true;
    }

Test MpiJob.task_count_for_all_block_1D.

    static bool
    test_task_count_for_all_block_1D(
        bool bool_RunTest, 
        MPI_Comm mpi_communicator)
    {
        if (!bool_RunTest) { return false; }

        puts("\n test_task_count_for_all_block_1D() \n");
        
        //-----------------------------------
        // import
        //-----------------------------------
        module_MpiJob
        MpiJob = import_MpiJob();

        module_Vector
        Vector = import_Vector();

        module_Assert
        Assert = import_Assert();

        //------------------------------------------------------
        //                     MPI INFO
        //------------------------------------------------------
        int i_MPI_ThisRank = -1; // MPI rank of this process
        int i_MPI_Volume   = -1; // MPI process count

        MPI_Comm_rank(mpi_communicator, &i_MPI_ThisRank);
        MPI_Comm_size(mpi_communicator, &i_MPI_Volume);
        //------------------------------------------------------
         
        if (i_MPI_ThisRank == 0)
        {
            printf("\n\t *** MPI this rank = %d/%d ***\n\n", i_MPI_ThisRank, i_MPI_Volume);

            int mpi_volume = 3;
            int vector_length = mpi_volume;
            int mpi_total_task_count = 10;
            int answer[3];
            int expect[3] = {4,3,3};

            Params_MpiJob_TaskCountForAllBlock1D
            params = MpiJob.new_Params_TaskCountForAllBlock1D();
            params.mpi_volume = mpi_volume;
            params.mpi_total_task_count = mpi_total_task_count;
            params.results_OUT = answer;
            MpiJob.task_count_for_all_block_1D(params);

            Params_Assert_EqualArray_int
            params_assert = {
                .input1 = answer,
                .input2 = expect,
                .length = vector_length,
            };

            Assert.equal_array_int(params_assert);

            Params_Vector_Print_int
            params_print = {
                .vector = NULL,
                .length = vector_length,
            };

            puts("answer:");
            params_print.vector = answer;
            Vector.print_int(params_print);

            puts("expect:");
            params_print.vector = expect;
            Vector.print_int(params_print);

        }
        return true;
    }


Test MpiJob.task_id_range_for_me_block_1D.

    static bool
    test_task_id_range_for_me_block_1D(
        bool bool_RunTest, 
        MPI_Comm mpi_communicator)
    {
        if (!bool_RunTest) { return false; }

        puts("\n test_task_id_range_for_me_block_1D() \n");

        //-----------------------------------
        // import
        //-----------------------------------
        module_MpiJob
        MpiJob = import_MpiJob();

        module_Assert
        Assert = import_Assert();

        //------------------------------------------------------
        //                     MPI INFO
        //------------------------------------------------------
        int i_MPI_ThisRank = -1; // MPI rank of this process
        int i_MPI_Volume   = -1; // MPI process count

        MPI_Comm_rank(mpi_communicator, &i_MPI_ThisRank);
        MPI_Comm_size(mpi_communicator, &i_MPI_Volume);
        //------------------------------------------------------
         
        if (i_MPI_ThisRank == 0)
        {
            printf("\n\t *** MPI this rank = %d/%d ***\n\n", i_MPI_ThisRank, i_MPI_Volume);

            int mpi_volume = 3;
            int mpi_total_task_count = 10;
            int i_ExpectedArray[3][2] = 
            {
                {0, 3},
                {4, 6},
                {7, 9},
            };

            for (int i = 0; i < mpi_volume; i++)
            {
                int this_rank = i;
                int answer[2] = {-1, -1};

                Params_MpiJob_TaskIdRangeForMeBlock1D
                params = MpiJob.new_Params_TaskIdRangeForMeBlock1D();
                params.mpi_volume = mpi_volume;
                params.mpi_this_rank = this_rank;
                params.mpi_total_task_count = mpi_total_task_count;
                params.results_OUT = answer;
                MpiJob.task_id_range_for_me_block_1D(params);

                int *expect = &i_ExpectedArray[i][0];

                for (int j = 0; j < 2; j++)
                {
                    printf("answer: %d\n", answer[j]);
                    printf("expect: %d\n", expect[j]);

                    Params_Assert_Equal_int 
                    params_assert = {
                        .input1 = answer[j],
                        .input2 = expect[j],
                    };

                    Assert.equal_int(params_assert);
                }
            }

        }
        return true;
    }


Test MpiJob.task_ids_for_me_block_1D.

    static bool
    test_task_ids_for_me_block_1D(
        bool bool_RunTest, 
        MPI_Comm mpi_communicator)
    {
        if (!bool_RunTest) { return false; }

        puts("\n test_task_ids_for_me_block_1D() \n");

        //-----------------------------------
        // import
        //-----------------------------------
        module_MpiJob
        MpiJob = import_MpiJob();

        module_Assert
        Assert = import_Assert();

        //------------------------------------------------------
        //                     MPI INFO
        //------------------------------------------------------
        int i_MPI_ThisRank = -1; // MPI rank of this process
        int i_MPI_Volume   = -1; // MPI process count

        MPI_Comm_rank(mpi_communicator, &i_MPI_ThisRank);
        MPI_Comm_size(mpi_communicator, &i_MPI_Volume);
        //------------------------------------------------------
         
        if (i_MPI_ThisRank == 0)
        {
            printf("\n\t *** MPI this rank = %d/%d ***\n\n", i_MPI_ThisRank, i_MPI_Volume);

            int mpi_volume = 3;
            int mpi_total_task_count = 5;
            int *i_ExpectedArray[3];
            i_ExpectedArray[0] = (int[]) {0, 1};
            i_ExpectedArray[1] = (int[]) {2, 3};
            i_ExpectedArray[2] = (int[]) {4};

            int i_LengthArray[3] = {2, 2, 1};

            for (int i = 0; i < mpi_volume; i++)
            {
                int this_rank = i;
                int vector_length = i_LengthArray[i];
                int answer[vector_length];

                Params_MpiJob_TaskIdsForMeBlock1D
                params = MpiJob.new_Params_TaskIdsForMeBlock1D();
                params.mpi_volume = mpi_volume;
                params.mpi_this_rank = this_rank;
                params.mpi_total_task_count = mpi_total_task_count;
                params.results_OUT = answer;
                params.results_length = vector_length;
                MpiJob.task_ids_for_me_block_1D(params);

                int *expect = i_ExpectedArray[i];

                Params_Assert_EqualArray_int
                params_assert = {
                    .input1 = answer,
                    .input2 = expect,
                    .length = vector_length,
                };
                Assert.equal_array_int(params_assert);
            }

        }
        return true;
    }


Test MpiJob.task_ids_for_me_block_1D.

    static bool
    test_task_ids_for_me_cyclic_1D(
        bool bool_RunTest, 
        MPI_Comm mpi_communicator)
    {
        if (!bool_RunTest) { return false; }

        puts("\n test_task_ids_for_me_cyclic_1D() \n");

        //-----------------------------------
        // import
        //-----------------------------------
        module_MpiJob
        MpiJob = import_MpiJob();

        module_Assert
        Assert = import_Assert();

        //------------------------------------------------------
        //                     MPI INFO
        //------------------------------------------------------
        int i_MPI_ThisRank = -1; // MPI rank of this process
        int i_MPI_Volume   = -1; // MPI process count

        MPI_Comm_rank(mpi_communicator, &i_MPI_ThisRank);
        MPI_Comm_size(mpi_communicator, &i_MPI_Volume);
        //------------------------------------------------------
         
        if (i_MPI_ThisRank == 0)
        {
            printf("\n\t *** MPI this rank = %d/%d ***\n\n", i_MPI_ThisRank, i_MPI_Volume);

            int mpi_volume = 3;
            int mpi_total_task_count = 5;
            int *i_ExpectedArray[3];
            i_ExpectedArray[0] = (int[]) {0, 3};
            i_ExpectedArray[1] = (int[]) {1, 4};
            i_ExpectedArray[2] = (int[]) {2};

            int i_LengthArray[3] = {2, 2, 1};

            for (int i = 0; i < mpi_volume; i++)
            {
                int this_rank = i;
                int vector_length = i_LengthArray[i];
                int answer[vector_length];

                Params_MpiJob_TaskIdsForMeCyclic1D
                params = MpiJob.new_Params_TaskIdsForMeCyclic1D();
                params.mpi_volume = mpi_volume;
                params.mpi_this_rank = this_rank;
                params.mpi_total_task_count = mpi_total_task_count;
                params.results_OUT = answer;
                params.results_length = vector_length;
                MpiJob.task_ids_for_me_cyclic_1D(params);

                int *expect = i_ExpectedArray[i];

                Params_Assert_EqualArray_int
                params_assert = {
                    .input1 = answer,
                    .input2 = expect,
                    .length = vector_length,
                };
                Assert.equal_array_int(params_assert);
            }

        }
        return true;
    }


Test MpiJob.is_my_task 

    static bool
    test_is_my_task(
        bool bool_RunTest, 
        MPI_Comm mpi_communicator)
    {
        if (!bool_RunTest) { return false; }

        puts("\n test_is_my_task() \n");

        //-----------------------------------
        // import
        //-----------------------------------
        module_MpiJob
        MpiJob = import_MpiJob();

        module_Assert
        Assert = import_Assert();

        //------------------------------------------------------
        //                     MPI INFO
        //------------------------------------------------------
        int i_MPI_ThisRank = -1; // MPI rank of this process
        int i_MPI_Volume   = -1; // MPI process count

        MPI_Comm_rank(mpi_communicator, &i_MPI_ThisRank);
        MPI_Comm_size(mpi_communicator, &i_MPI_Volume);
        //------------------------------------------------------
         
        if (i_MPI_ThisRank == 0)
        {
            printf("\n\t *** MPI this rank = %d/%d ***\n\n", i_MPI_ThisRank, i_MPI_Volume);

            int mpi_volume = 3;
            int mpi_total_task_count = 5;
            int *i_ExpectedArray[3];
            i_ExpectedArray[0] = (int[]) {1, 1, 0, 0, 0};
            i_ExpectedArray[1] = (int[]) {0, 0, 1, 1, 0};
            i_ExpectedArray[2] = (int[]) {0, 0, 0, 0,1};

            int i_TaskCountArray[3] = {2, 2, 1};

            for (int i = 0; i < mpi_volume; i++)
            {
                int this_rank = i;
                int vector_length = i_TaskCountArray[i];
                int i_MyTaskIdArray[vector_length];

                Params_MpiJob_TaskIdsForMeBlock1D
                params = MpiJob.new_Params_TaskIdsForMeBlock1D();
                params.mpi_volume = mpi_volume;
                params.mpi_this_rank = this_rank;
                params.mpi_total_task_count = mpi_total_task_count;
                params.results_OUT = i_MyTaskIdArray;
                params.results_length = vector_length;
                MpiJob.task_ids_for_me_block_1D(params);

                int answer[mpi_total_task_count];
                for ( int i = 0; i < mpi_total_task_count; i++ ) { answer[i] = 0; }

                Params_MpiJob_IsMyTask
                params_mpijob = MpiJob.new_Params_IsMyTask();
                params_mpijob.my_task_ids = i_MyTaskIdArray;
                params_mpijob.my_task_count = vector_length;
                params_mpijob.task_id = -1;


                for ( int i = 0; i < mpi_total_task_count; i++ )
                {
                    params_mpijob.task_id = i;
                    answer[i] = MpiJob.is_my_task(params_mpijob);
                }

                int *expect = i_ExpectedArray[i];

                Params_Assert_EqualArray_int
                params_assert = {
                    .input1 = answer,
                    .input2 = expect,
                    .length = mpi_total_task_count,
                };
                Assert.equal_array_int(params_assert);
            }

        }
        return true;
    }



Test MpiJob.task_id_range_for_me_block_1D.

    static bool
    test_Class_RankIdBlock1D(
        bool bool_RunTest, 
        MPI_Comm mpi_communicator)
    {
        if (!bool_RunTest) { return false; }

        puts("\n test_Class_RankIdBlock1D() \n");

        //-----------------------------------
        // import
        //-----------------------------------
        module_MpiJob
        MpiJob = import_MpiJob();

        module_Assert
        Assert = import_Assert();

        //------------------------------------------------------
        //                     MPI INFO
        //------------------------------------------------------
        int i_MPI_ThisRank = -1; // MPI rank of this process
        int i_MPI_Volume   = -1; // MPI process count

        MPI_Comm_rank(mpi_communicator, &i_MPI_ThisRank);
        MPI_Comm_size(mpi_communicator, &i_MPI_Volume);
        //------------------------------------------------------
         
        if (i_MPI_ThisRank == 0)
        {
            printf("\n\t *** MPI this rank = %d/%d ***\n\n", i_MPI_ThisRank, i_MPI_Volume);

            int mpi_volume = 3;
            int mpi_total_task_count = 5;
            int i_ExpectedArray[5] = {0, 0, 1, 1, 2};
            int storage[mpi_volume];

            Class_MpiJob_RankIdBlock1D
            obj_rank = MpiJob.new_RankIdBlock1D(
                mpi_volume,
                mpi_total_task_count,
                storage);

            obj_rank.mpi_volume = 3;
            obj_rank.mpi_total_task_count = 5;

            for (int i = 0; i < mpi_total_task_count; i++)
            {
                puts("------------------");
                printf("\ni = %d\n", i);
                int task_id = i;
                int answer = obj_rank.rank(obj_rank, task_id);
                int expect = i_ExpectedArray[i];

                printf("rank answer: %d\n", answer);
                printf("rank expect: %d\n", expect);

                Params_Assert_Equal_int 
                params_assert = {
                    .input1 = answer,
                    .input2 = expect,
                };

                Assert.equal_int(params_assert);
            }
            puts("-------------------");

            //----------------------------------
            // check for non-existent task_id
            //----------------------------------
            puts("");
            puts("check the case of non-existent task_id");
            int task_id = mpi_total_task_count;
            int answer = obj_rank.rank(obj_rank, task_id);
            int expect = -1;
            printf("rank answer: %d\n", answer);
            printf("rank expect: %d\n", expect);

            Params_Assert_Equal_int 
            params_assert = {
                .input1 = answer,
                .input2 = expect,
            };

            Assert.equal_int(params_assert);
            puts("-------------------");

            //------------------------------
            // check for the insidious case
            // where #cpu cores > #tasks
            //------------------------------
            puts("-------------------");
            puts("\nCheck for the case when #cores > #tasks");
            puts("(note: expected failure)\n");
            Class_MpiJob_RankIdBlock1D
            obj_rank2 = MpiJob.new_RankIdBlock1D(
                1000,
                1,
                storage);

            bool success = obj_rank2.success;
            bool expected_flag = false;

            printf("result: %d\n", success);
            printf("expect: %d\n", expected_flag);
            assert(success == expected_flag);
        }
        return true;
    }




Test MpiJob.Class_MpiJob_RankIdCyclic1D

    static bool
    test_Class_RankIdCyclic1D(
        bool bool_RunTest, 
        MPI_Comm mpi_communicator)
    {
        if (!bool_RunTest) { return false; }

        puts("\n test_Class_RankIdBlock1D() \n");

        //-----------------------------------
        // import
        //-----------------------------------
        module_MpiJob
        MpiJob = import_MpiJob();

        module_Assert
        Assert = import_Assert();

        //------------------------------------------------------
        //                     MPI INFO
        //------------------------------------------------------
        int i_MPI_ThisRank = -1; // MPI rank of this process
        int i_MPI_Volume   = -1; // MPI process count

        MPI_Comm_rank(mpi_communicator, &i_MPI_ThisRank);
        MPI_Comm_size(mpi_communicator, &i_MPI_Volume);
        //------------------------------------------------------
         
        if (i_MPI_ThisRank == 0)
        {
            printf("\n\t *** MPI this rank = %d/%d ***\n\n", i_MPI_ThisRank, i_MPI_Volume);

            int mpi_volume = 3;
            int mpi_total_task_count = 5;
            int i_ExpectedArray[5] = {0, 1, 2, 0, 1};

            Class_MpiJob_RankIdCyclic1D
            obj_rank = MpiJob.new_RankIdCyclic1D(
                mpi_volume,
                mpi_total_task_count);

            obj_rank.mpi_volume = 3;
            obj_rank.mpi_total_task_count = 5;

            for (int i = 0; i < mpi_total_task_count; i++)
            {
                puts("------------------");
                printf("\ni = %d\n", i);
                int task_id = i;
                int answer = obj_rank.rank(obj_rank, task_id);
                int expect = i_ExpectedArray[i];

                printf("rank answer: %d\n", answer);
                printf("rank expect: %d\n", expect);

                Params_Assert_Equal_int 
                params_assert = {
                    .input1 = answer,
                    .input2 = expect,
                };

                Assert.equal_int(params_assert);
            }
            puts("-------------------");

            //----------------------------------
            // check for non-existent task_id
            //----------------------------------
            puts("");
            puts("check the case of non-existent task_id");
            int task_id = mpi_total_task_count;
            int answer = obj_rank.rank(obj_rank, task_id);
            int expect = -1;
            printf("rank answer: %d\n", answer);
            printf("rank expect: %d\n", expect);

            Params_Assert_Equal_int 
            params_assert = {
                .input1 = answer,
                .input2 = expect,
            };

            Assert.equal_int(params_assert);
            puts("-------------------");

            //------------------------------
            // check for the insidious case
            // where #cpu cores > #tasks
            //------------------------------
            puts("-------------------");
            puts("\nCheck for the case when #cores > #tasks");
            puts("(note: expected failure)\n");
            Class_MpiJob_RankIdCyclic1D
            obj_rank2 = MpiJob.new_RankIdCyclic1D(
                1000,
                1);

            bool success = obj_rank2.success;
            bool expected_flag = false;

            printf("result: %d\n", success);
            printf("expect: %d\n", expected_flag);
            assert(success == expected_flag);
        }
        return true;
    }




Test MpiJob.mpi_gatherv_displacement_array_block_1D.

    static bool
    test_gatherv_displacements_block_1D(
        bool bool_RunTest, 
        MPI_Comm mpi_communicator)
    {
        if (!bool_RunTest) { return false; }

        puts("\n test_gatherv_displacements_block_1D() \n");

        //-----------------------------------
        // import
        //-----------------------------------
        module_MpiJob
        MpiJob = import_MpiJob();

        module_Vector
        Vector = import_Vector();

        module_Assert
        Assert = import_Assert();

        //------------------------------------------------------
        //                     MPI INFO
        //------------------------------------------------------
        int i_MPI_ThisRank = -1; // MPI rank of this process
        int i_MPI_Volume   = -1; // MPI process count

        MPI_Comm_rank(mpi_communicator, &i_MPI_ThisRank);
        MPI_Comm_size(mpi_communicator, &i_MPI_Volume);
        //------------------------------------------------------
         
        if (i_MPI_ThisRank == 0)
        {
            printf("\n\t *** MPI this rank = %d/%d ***\n\n", i_MPI_ThisRank, i_MPI_Volume);

            int vector_length = 3;
            int mpi_volume = 3;
            int mpi_total_task_count = 5;
            int answer[3];
            int expect[3] = {0, 2, 4};

            Params_MpiJob_GathervDisplacementsBlock1D
            params_mpijob = MpiJob.new_Params_GathervDisplacementsBlock1D();
            params_mpijob.mpi_total_task_count = mpi_total_task_count;
            params_mpijob.mpi_volume = mpi_volume;
            params_mpijob.results_OUT = answer;
            MpiJob.gatherv_displacements_block_1D(params_mpijob);

            Params_Assert_EqualArray_int
            params_assert  = {
                .input1 = answer,
                .input2 = expect,
                .length = vector_length,
            };

            Assert.equal_array_int(params_assert);

            Params_Vector_Print_int
            params_print = {
                .vector = NULL,
                .length = vector_length,
            };

            puts("answer:");
            params_print.vector = answer;
            Vector.print_int(params_print);

            puts("expect:");
            params_print.vector = expect;
            Vector.print_int(params_print);
        }
        return true;
    }



