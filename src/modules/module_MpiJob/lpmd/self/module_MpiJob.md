#Module module_MpiJob

#[module_MpiJob.dep.h](#module_MpiJob.dep.h "save:")
    
    _"LICENSE"

    #include <stdbool.h>

#[module_MpiJob.h](#module_MpiJob.h "save:")
    
    _"LICENSE"

    #include "module_MpiJob.dep.h"
    
    _"module_MpiJob parameter types"

    _"module_MpiJob interface"



#[module_MpiJob.dep.c](#module_MpiJob.dep.c "save:")
    
    _"LICENSE"

    #include "module_MpiJob.h"
    #include <stdio.h>
    #include <assert.h>
    #include <stdbool.h>

#[module_MpiJob.c](#module_MpiJob.c "save:")
    
    _"LICENSE"

    #include "module_MpiJob.dep.c"

    _"module_MpiJob internal functions"

    _"module_MpiJob parameter initializer"

    _"module_MpiJob front-end functions"

    _"module_MpiJob import function"


## module_MpiJob parameter types

Parameter type for function task_count_for_me_block_1D.

    typedef struct 
    Params_MpiJob_TaskCountForMeBlock1D
    {
        int mpi_this_rank;
        int mpi_volume;
        int mpi_total_task_count;
        int *result_OUT;
    }
    Params_MpiJob_TaskCountForMeBlock1D;

Parameter type for function task_count_for_all_block_1D.

    typedef struct 
    Params_MpiJob_TaskCountForAllBlock1D
    {
        int mpi_volume;
        int mpi_total_task_count;
        int *results_OUT;
    }
    Params_MpiJob_TaskCountForAllBlock1D;

Parameter type for function task_id_range_for_me_block_1D.

    typedef struct 
    Params_MpiJob_TaskIdRangeForMeBlock1D
    {
        int mpi_this_rank;
        int mpi_volume;
        int mpi_total_task_count;
        int *results_OUT;
    }
    Params_MpiJob_TaskIdRangeForMeBlock1D;

Parameter type for function task_ids_for_me_block_1D.

    typedef struct 
    Params_MpiJob_TaskIdsForMeBlock1D
    {
        int mpi_this_rank;
        int mpi_volume;
        int mpi_total_task_count;
        int *results_OUT;
        int results_length;
    }
    Params_MpiJob_TaskIdsForMeBlock1D;

Parameter type for function task_ids_for_me_cyclic_1D.

    typedef struct 
    Params_MpiJob_TaskIdsForMeCyclic1D
    {
        int mpi_this_rank;
        int mpi_volume;
        int mpi_total_task_count;
        int *results_OUT;
        int results_length;
    }
    Params_MpiJob_TaskIdsForMeCyclic1D;

Class for getting MPI rank corresonding to a task ID.
    typedef struct 
    Class_MpiJob_RankIdBlock1D
    {
        int mpi_volume;
        int mpi_total_task_count;
        int (*rank)(struct Class_MpiJob_RankIdBlock1D, int task_id);
        int *_internal_storage;
        bool success;
    }
    Class_MpiJob_RankIdBlock1D;

Class for getting MPI rank corresonding to a task ID.
    typedef struct 
    Class_MpiJob_RankIdCyclic1D
    {
        int mpi_volume;
        int mpi_total_task_count;
        int (*rank)(struct Class_MpiJob_RankIdCyclic1D, int task_id);
        int *_internal_storage;
        bool success;
    }
    Class_MpiJob_RankIdCyclic1D;

Parameters for gatherv_displacements_block_1D
    typedef struct 
    Params_MpiJob_GathervDisplacementsBlock1D
    {
        int mpi_volume;
        int mpi_total_task_count;
        int *results_OUT;
    }
    Params_MpiJob_GathervDisplacementsBlock1D;

Parameters for is_my_task
    typedef struct 
    Params_MpiJob_IsMyTask
    {
        int *my_task_ids;
        int  my_task_count;
        int task_id;
    }
    Params_MpiJob_IsMyTask;

## module_MpiJob interface

    typedef struct 
    module_MpiJob
    {
        Class_MpiJob_RankIdBlock1D (*new_RankIdBlock1D)(int mpi_volume, int mpi_total_task_count, int _internal_storage[mpi_volume]);
        Class_MpiJob_RankIdCyclic1D (*new_RankIdCyclic1D)(int mpi_volume, int mpi_total_task_count);
        void (*task_count_for_me_block_1D)(Params_MpiJob_TaskCountForMeBlock1D);
        void (*task_count_for_all_block_1D)(Params_MpiJob_TaskCountForAllBlock1D);
        void (*task_id_range_for_me_block_1D)(Params_MpiJob_TaskIdRangeForMeBlock1D);
        void (*task_ids_for_me_block_1D)(Params_MpiJob_TaskIdsForMeBlock1D);
        void (*task_ids_for_me_cyclic_1D)(Params_MpiJob_TaskIdsForMeCyclic1D);
        void (*gatherv_displacements_block_1D)(Params_MpiJob_GathervDisplacementsBlock1D);
        bool (*is_my_task)(Params_MpiJob_IsMyTask);
        Params_MpiJob_TaskCountForMeBlock1D (*new_Params_TaskCountForMeBlock1D)(void);
        Params_MpiJob_TaskCountForAllBlock1D (*new_Params_TaskCountForAllBlock1D)(void);
        Params_MpiJob_TaskIdRangeForMeBlock1D (*new_Params_TaskIdRangeForMeBlock1D)(void);
        Params_MpiJob_TaskIdsForMeBlock1D (*new_Params_TaskIdsForMeBlock1D)(void);
        Params_MpiJob_TaskIdsForMeCyclic1D (*new_Params_TaskIdsForMeCyclic1D)(void);
        Params_MpiJob_GathervDisplacementsBlock1D (*new_Params_GathervDisplacementsBlock1D)(void);
        Params_MpiJob_IsMyTask (*new_Params_IsMyTask)(void);
    }
    module_MpiJob;
    
    module_MpiJob 
    import_MpiJob(void);

## module_MpiJob import function 
    module_MpiJob
    import_MpiJob(void)
    {
        module_MpiJob
        obj = {
            .new_RankIdBlock1D = new_RankIdBlock1D,
            .new_RankIdCyclic1D = new_RankIdCyclic1D,
            .task_count_for_me_block_1D = task_count_for_me_block_1D,
            .task_count_for_all_block_1D = task_count_for_all_block_1D,
            .task_id_range_for_me_block_1D = task_id_range_for_me_block_1D,
            .task_ids_for_me_block_1D = task_ids_for_me_block_1D,
            .task_ids_for_me_cyclic_1D = task_ids_for_me_cyclic_1D,
            .gatherv_displacements_block_1D = gatherv_displacements_block_1D,
            .is_my_task = is_my_task,
            .new_Params_TaskCountForMeBlock1D = new_Params_TaskCountForMeBlock1D,
            .new_Params_TaskCountForAllBlock1D = new_Params_TaskCountForAllBlock1D,
            .new_Params_TaskIdRangeForMeBlock1D = new_Params_TaskIdRangeForMeBlock1D,
            .new_Params_TaskIdsForMeBlock1D = new_Params_TaskIdsForMeBlock1D,
            .new_Params_TaskIdsForMeCyclic1D = new_Params_TaskIdsForMeCyclic1D,
            .new_Params_GathervDisplacementsBlock1D = new_Params_GathervDisplacementsBlock1D,
            .new_Params_IsMyTask = new_Params_IsMyTask,
        };
        return obj;
    }

## module_MpiJob parameter initializer
    
    Class_MpiJob_RankIdBlock1D
    new_RankIdBlock1D(
        int mpi_volume, 
        int mpi_total_task_count, 
        int _internal_storage[mpi_volume])
    {
        bool bool_success = true;

        if (mpi_total_task_count >= mpi_volume)
        {
            /*****************************************/
            // VERY IMPORTANT: reset to zero before 
            // the accumulation steps
            /*****************************************/
            for (int i = 0; i < mpi_volume; i++)
            { _internal_storage[i] = 0; }
        
            /*****************************************/
            /* _internal_storage is a place to store the 
             *  prefix sum array of the task assignment list.
             */
            // do task assignment
            for (int i = 0; i < mpi_total_task_count; i++)
            {
                int index = i % mpi_volume;
                _internal_storage[index] += 1;
            }

            // prefix sum
            for (int i = 1; i < mpi_volume; i++)
            {
                _internal_storage[i] += _internal_storage[i-1];
            }
        }
        else
        {
            printf("WARNING (from new_RankIdBlock1D()): "
                   "your input #mpi_volume (got %d) > #mpi_total_task_count (got %d)\n", 
                   mpi_volume, mpi_total_task_count);
            bool_success = false;
        }


        Class_MpiJob_RankIdBlock1D
        obj = {
            .mpi_volume = mpi_volume,
            .mpi_total_task_count = mpi_total_task_count,
            .rank = get_rank_block_1D,
            ._internal_storage = _internal_storage,
            .success = bool_success,
        };
        return obj;
    }   

    Class_MpiJob_RankIdCyclic1D
    new_RankIdCyclic1D(
        int mpi_volume, 
        int mpi_total_task_count)
    {
        bool bool_success = true;
        if ( mpi_volume > mpi_total_task_count )
        {
            bool_success = false;
        }
        Class_MpiJob_RankIdCyclic1D
        obj = {
            .mpi_volume = mpi_volume,
            .mpi_total_task_count = mpi_total_task_count,
            .rank = get_rank_cyclic_1D,
            .success = bool_success,
        };
        return obj;
    }

    Params_MpiJob_TaskCountForMeBlock1D
    new_Params_TaskCountForMeBlock1D(void)
    {
        Params_MpiJob_TaskCountForMeBlock1D
        obj = {
            .mpi_this_rank = -1,
            .mpi_volume = -1,
            .mpi_total_task_count =-1,
            .result_OUT = NULL,
        };
        return obj;
    } 


    Params_MpiJob_TaskCountForAllBlock1D
    new_Params_TaskCountForAllBlock1D(void)
    {
        Params_MpiJob_TaskCountForAllBlock1D
        obj = {
            .mpi_volume = -1,
            .mpi_total_task_count =-1,
            .results_OUT = NULL,
        };
        return obj;
    } 
       
    Params_MpiJob_TaskIdRangeForMeBlock1D
    new_Params_TaskIdRangeForMeBlock1D(void)
    {
        Params_MpiJob_TaskIdRangeForMeBlock1D
        obj = {
            .mpi_this_rank = -1,
            .mpi_volume = -1,
            .mpi_total_task_count =-1,
            .results_OUT = NULL,
        };
        return obj;
    }
       
    Params_MpiJob_TaskIdsForMeBlock1D
    new_Params_TaskIdsForMeBlock1D(void)
    {
        Params_MpiJob_TaskIdsForMeBlock1D
        obj = {
            .mpi_this_rank = -1,
            .mpi_volume = -1,
            .mpi_total_task_count =-1,
            .results_OUT = NULL,
            .results_length = -1,
        };
        return obj;
    }  

    Params_MpiJob_TaskIdsForMeCyclic1D
    new_Params_TaskIdsForMeCyclic1D(void)
    {
        Params_MpiJob_TaskIdsForMeCyclic1D
        obj = {
            .mpi_this_rank = -1,
            .mpi_volume = -1,
            .mpi_total_task_count =-1,
            .results_OUT = NULL,
            .results_length = -1,
        };
        return obj;
    }

       
    Params_MpiJob_GathervDisplacementsBlock1D
    new_Params_GathervDisplacementsBlock1D(void)
    {
        Params_MpiJob_GathervDisplacementsBlock1D
        obj = {
            .mpi_volume = -1,
            .mpi_total_task_count =-1,
            .results_OUT = NULL,
        };
        return obj;
    }


    Params_MpiJob_IsMyTask
    new_Params_IsMyTask(void)
    {
        Params_MpiJob_IsMyTask
        obj = {
            .my_task_ids = NULL,
            .my_task_count = 0,
            .task_id = -1,
        };
        return obj;
    }

## module_MpiJob front-end functions

This function will find out how many tasks are assigned to one MPI process. 
    static void 
    task_count_for_me_block_1D(Params_MpiJob_TaskCountForMeBlock1D params)
    {
        int this_rank = params.mpi_this_rank;

        int MpiJob_task_count_for_me_block_1D_this_rank = this_rank;
        assert(MpiJob_task_count_for_me_block_1D_this_rank < params.mpi_volume);

        int i_TaskCountArrayLength = params.mpi_volume;
        int i_TaskCountArray[i_TaskCountArrayLength];

        //------------------------------------------------
        // 1. count the number of tasks for each MPI rank
        //------------------------------------------------
        IN_CountTasksBlock1D
        params_count_tasks = new_IN_CountTasksBlock1D();
        params_count_tasks.mpi_volume = params.mpi_volume;
        params_count_tasks.mpi_total_task_count = params.mpi_total_task_count;
        params_count_tasks.results_OUT = i_TaskCountArray;
        count_tasks_block_1D(params_count_tasks);
        //------------------------------------------------
        
        *params.result_OUT = i_TaskCountArray[this_rank];
    }


This function will find out how many tasks are assigned to all MPI processes. 
    static void 
    task_count_for_all_block_1D(Params_MpiJob_TaskCountForAllBlock1D params)
    {
        int *i_TaskCountArray       = params.results_OUT; // alias

        //------------------------------------------------
        //  count the number of tasks for each MPI rank
        //------------------------------------------------
        IN_CountTasksBlock1D
        params_count_tasks = new_IN_CountTasksBlock1D();
        params_count_tasks.mpi_volume = params.mpi_volume;
        params_count_tasks.mpi_total_task_count = params.mpi_total_task_count;
        params_count_tasks.results_OUT = i_TaskCountArray;
        count_tasks_block_1D(params_count_tasks);
        //------------------------------------------------
    }    

    static void 
    task_id_range_for_me_block_1D(Params_MpiJob_TaskIdRangeForMeBlock1D params)
    {
        int this_rank = params.mpi_this_rank;

        int MpiJob_task_id_range_for_me_block_1D_this_rank = this_rank;
        assert(MpiJob_task_id_range_for_me_block_1D_this_rank < params.mpi_volume);

        int i_TaskCountArrayLength = params.mpi_volume;
        int i_TaskCountArray[i_TaskCountArrayLength];

        //------------------------------------------------
        // 1. count the number of tasks for each MPI rank
        //------------------------------------------------
        IN_CountTasksBlock1D
        params_count_tasks = new_IN_CountTasksBlock1D();
        params_count_tasks.mpi_volume = params.mpi_volume;
        params_count_tasks.mpi_total_task_count = params.mpi_total_task_count;
        params_count_tasks.results_OUT = i_TaskCountArray;
        count_tasks_block_1D(params_count_tasks);
        //------------------------------------------------

        //------------------------------------------------
        // 2. do prefix sum of the task count array
        //------------------------------------------------
        IN_PrefixSumTaskAssignments
        params_prefix_sum = new_IN_PrefixSumTaskAssignments();
        params_prefix_sum.task_count_array = i_TaskCountArray;
        params_prefix_sum.task_count_array_length = i_TaskCountArrayLength;
        prefix_sum_task_counts(params_prefix_sum);
        //------------------------------------------------

        //------------------------------------------------
        // 3. compute results
        //------------------------------------------------
        params.results_OUT[1] = i_TaskCountArray[this_rank] - 1;

        if (this_rank == 0) 
        {
            params.results_OUT[0] = 0;
        }
        else
        {
            params.results_OUT[0] = i_TaskCountArray[this_rank - 1];
        }
        //------------------------------------------------

        // Done!
    }


Calculate the MPI_Gatherv displacement array argument.
    static void 
    gatherv_displacements_block_1D(Params_MpiJob_GathervDisplacementsBlock1D params)
    {
        assert(params.mpi_total_task_count >= params.mpi_volume);

        int i_TaskCountArrayLength = params.mpi_volume;
        int *i_TaskCountArray = params.results_OUT;

        //------------------------------------------------
        // 1. count the number of tasks for each MPI rank
        //------------------------------------------------
        IN_CountTasksBlock1D
        params_count_tasks = new_IN_CountTasksBlock1D();
        params_count_tasks.mpi_volume = params.mpi_volume;
        params_count_tasks.mpi_total_task_count = params.mpi_total_task_count;
        params_count_tasks.results_OUT = i_TaskCountArray;
        count_tasks_block_1D(params_count_tasks);
        //------------------------------------------------

        //------------------------------------------------
        // 2. do prefix sum of the task count array
        //------------------------------------------------
        IN_PrefixSumTaskAssignments
        params_prefix_sum = new_IN_PrefixSumTaskAssignments();
        params_prefix_sum.task_count_array = i_TaskCountArray;
        params_prefix_sum.task_count_array_length = i_TaskCountArrayLength;
        prefix_sum_task_counts(params_prefix_sum);
        //------------------------------------------------

        //------------------------------------------------
        // 3. shift by one to get the gatherv displacements
        //------------------------------------------------
        for (int i = (i_TaskCountArrayLength - 1); i > 0; i--)
        {
            i_TaskCountArray[i] = i_TaskCountArray[i-1];
        }
        i_TaskCountArray[0] = 0;
        //------------------------------------------------
        // Done!
        //------------------------------------------------
    }

    static void 
    task_ids_for_me_block_1D(Params_MpiJob_TaskIdsForMeBlock1D params)
    {
        int this_rank = params.mpi_this_rank;

        int MpiJob_task_id_range_for_me_block_1D_this_rank = this_rank;
        assert(MpiJob_task_id_range_for_me_block_1D_this_rank < params.mpi_volume);

        int i_TaskCountArrayLength = params.mpi_volume;
        int i_TaskCountArray[i_TaskCountArrayLength];

        //------------------------------------------------
        // 1. count the number of tasks for each MPI rank
        //------------------------------------------------
        IN_CountTasksBlock1D
        params_count_tasks = new_IN_CountTasksBlock1D();
        params_count_tasks.mpi_volume = params.mpi_volume;
        params_count_tasks.mpi_total_task_count = params.mpi_total_task_count;
        params_count_tasks.results_OUT = i_TaskCountArray;
        count_tasks_block_1D(params_count_tasks);

        // length of the output task ID array
        int my_task_count = i_TaskCountArray[this_rank];
        assert(my_task_count == params.results_length);
        //------------------------------------------------

        //------------------------------------------------
        // 2. do prefix sum of the task count array
        //------------------------------------------------
        IN_PrefixSumTaskAssignments
        params_prefix_sum = new_IN_PrefixSumTaskAssignments();
        params_prefix_sum.task_count_array = i_TaskCountArray;
        params_prefix_sum.task_count_array_length = i_TaskCountArrayLength;
        prefix_sum_task_counts(params_prefix_sum);
        //------------------------------------------------

        //------------------------------------------------
        // 3. compute results
        //------------------------------------------------
        int i_TaskIdEon = 0;
        if (this_rank != 0) { i_TaskIdEon = i_TaskCountArray[this_rank - 1]; }
        

        // write to output
        for ( int i = 0; i < my_task_count; i++ )
        {
            int i_TaskId = i + i_TaskIdEon;
            params.results_OUT[i] = i_TaskId;
        }

        //------------------------------------------------

        // Done!
    }

    static void 
    task_ids_for_me_cyclic_1D(Params_MpiJob_TaskIdsForMeCyclic1D params)
    {
        int this_rank = params.mpi_this_rank;

        int MpiJob_task_id_range_for_me_block_1D_this_rank = this_rank;
        assert(MpiJob_task_id_range_for_me_block_1D_this_rank < params.mpi_volume);

        int i_TaskCountArrayLength = params.mpi_volume;
        int i_TaskCountArray[i_TaskCountArrayLength];

        //------------------------------------------------
        // 1. count the number of tasks for each MPI rank
        //------------------------------------------------
        IN_CountTasksBlock1D
        params_count_tasks = new_IN_CountTasksBlock1D();
        params_count_tasks.mpi_volume = params.mpi_volume;
        params_count_tasks.mpi_total_task_count = params.mpi_total_task_count;
        params_count_tasks.results_OUT = i_TaskCountArray;
        count_tasks_block_1D(params_count_tasks);

        // length of the output task ID array
        int my_task_count = i_TaskCountArray[this_rank];
        assert(my_task_count == params.results_length);
        //------------------------------------------------

        //------------------------------------------------
        // 3. compute results
        //------------------------------------------------
        // write to output
        int ccc = 0; //counter
        for ( int i = 0; i < params.mpi_total_task_count; i++ )
        {
            int task_id = i;
            int rank = task_id % params.mpi_volume; // which rank this task belongs to 
            if  ( rank == params.mpi_this_rank )
            {
                params.results_OUT[ccc] = task_id;
                ccc = ccc + 1;
            }
        }

        //------------------------------------------------

        // Done!
    }

This function checks whether a task belongs to me.
    static bool 
    is_my_task(Params_MpiJob_IsMyTask params)
    {
        for ( int i = 0; i < params.my_task_count; i++ )
        {
            if ( params.my_task_ids[i] == params.task_id ) { return true; }
        }
        return false;
    }

## module_MpiJob internal functions
    static int 
    get_rank_block_1D(Class_MpiJob_RankIdBlock1D self, int task_id)
    {
        for (int rank = 0; rank < self.mpi_volume; rank++)
        {
            int i_UpperBound = self._internal_storage[rank] - 1;
            int i_LowerBound = 0;
            if (rank > 0) { i_LowerBound = self._internal_storage[rank - 1]; }

            if ((task_id >= i_LowerBound) && 
                (task_id <= i_UpperBound))
            {
                return rank;
            }
        }

        int NOT_FOUND = -1;
        return NOT_FOUND;
    }

    static int 
    get_rank_cyclic_1D(Class_MpiJob_RankIdCyclic1D self, int task_id)
    {

        
        int rank = task_id % self.mpi_volume;
        if (task_id >= self.mpi_total_task_count) { rank = -1; }
        return rank;
    }

This internal function will calculate the task assignments
using 1D-block mapping.
    typedef struct 
    IN_CountTasksBlock1D
    {
        int  mpi_volume;
        int  mpi_total_task_count;
        int *results_OUT;
    }
    IN_CountTasksBlock1D;

    static IN_CountTasksBlock1D
    new_IN_CountTasksBlock1D(void)
    {
        IN_CountTasksBlock1D
        obj = {
            .mpi_volume = -1,
            .mpi_total_task_count = -1,
            .results_OUT = NULL,
        };
        return obj;
    }

    static void 
    count_tasks_block_1D(IN_CountTasksBlock1D params)
    {
        int *i_TaskCountArray = params.results_OUT;

        /**************************************
         * VERY IMPORTANT: reset input array!
         **************************************/
        // reset to zero
        for (int i = 0; i < params.mpi_volume; i++)
        {
            i_TaskCountArray[i] = 0;
        }
        //-------------------------------------

        // do task assignment
        for (int i = 0; i < params.mpi_total_task_count; i++)
        {
            int index = i % params.mpi_volume;
            i_TaskCountArray[index] += 1;
        }
    }


This internal function will find out the prefix sum array 
of the MPI tasks assignments.
    typedef struct 
    IN_PrefixSumTaskAssignments
    {
        int *task_count_array;
        int  task_count_array_length;
    }
    IN_PrefixSumTaskAssignments;

    static IN_PrefixSumTaskAssignments
    new_IN_PrefixSumTaskAssignments(void)
    {
        IN_PrefixSumTaskAssignments
        obj = {
            .task_count_array = NULL,
            .task_count_array_length = -1,
        };
        return obj;
    }

    static void
    prefix_sum_task_counts(IN_PrefixSumTaskAssignments params)
    {
        for (int i = 1; i < params.task_count_array_length; i++)
        {
            int left_neighbour_value = params.task_count_array[i-1];
            params.task_count_array[i] += left_neighbour_value;
        }
    }


