# Module module_Vector
This module provides tools for manipulating vectors.

#[module_Vector.dep.h](#module_Vector.dep.h "save:")
    _"LICENSE"
    
    #include "module_Type.h"


#[module_Vector.h](#module_Vector.h "save:")
    _"LICENSE"
    
    #ifndef MODULE_VECTOR_H
    #define MODULE_VECTOR_H

    #include "module_Vector.dep.h"

    _"module_Vector function parameters"

    _"module_Vector interface API"

    #endif

#[module_Vector.dep.c](#module_Vector.dep.c "save:")

    _"LICENSE"

    #include "module_Vector.h"
    #include <stdio.h>
    #include <math.h>
    #include <assert.h>
    #include <omp.h>

#[module_Vector.c](#module_Vector.c "save:")

    _"LICENSE"

    #include "module_Vector.dep.c"

    _"module_Vector parameter initializers"

    _"module_Vector worker functions"

    _"module_Vector front-end functions"

Front-end import_Vector.

    module_Vector
    import_Vector(void)
    {
        module_Vector
        obj = {
            .hi = hi,
            .print_int = print_int,
            .print_float = print_float,
            .reset_float = reset_float,
            .dot = dot,
            .dot_float = dot_float,
            .omp_dot_float = omp_dot_float,
            .norm = norm,
            .o2_norm_float = o2_norm_float,
            .add = add,
            .add_float = add_float,
            .scale_int = scale_int,
            .scale_float = scale_float,
            .copy_float = copy_float,
            .new_Params_Dot_float = new_Params_Dot_float,
        };
        return obj;
    }

## module_Vector interface API

    typedef struct 
    module_Vector
    {
        void (*hi)(void);
        void (*print_int)(Params_Vector_Print_int);
        void (*print_float)(Params_Vector_Print_float);
        void (*reset_float)(Params_Vector_Reset_float);
        void (*dot)(Params_Vector_Dot);
        void (*dot_float)(Params_Vector_Dot_float);
        void (*omp_dot_float)(Params_Vector_Dot_float);
        void (*norm)(Params_Vector_o2Norm);
        void (*o2_norm_float)(Params_Vector_o2Norm_float);
        void (*add)(Params_Vector_Add);
        void (*add_float)(Params_Vector_Add_float);
        void (*scale_int)(Params_Vector_Scale_int);
        void (*scale_float)(Params_Vector_Scale_float);
        void (*copy_float)(Params_Vector_Copy_float);
        Params_Vector_Dot_float (*new_Params_Dot_float)(void);
    }
    module_Vector;

    module_Vector
    import_Vector(void);



## module_Vector function parameters

    //-----------------------------
    // parameters for Vector.print_int()
    //-----------------------------
    typedef struct 
    Params_Vector_Print_int
    {
        int *vector;
        int  length;
    }
    Params_Vector_Print_int;
    //-----------------------------


    //-----------------------------
    // parameters for Vector.print_float()
    //-----------------------------
    typedef struct 
    Params_Vector_Print_float
    {
        float *vector;
        int    length;
        int    precision;
    }
    Params_Vector_Print_float;
    //-----------------------------

    //-----------------------------
    // parameters for Vector.reset()
    //-----------------------------
    typedef struct 
    Params_Vector_Reset_float
    {
        float *vector;
        float  value;
        int    length;
    }
    Params_Vector_Reset_float;
    

    //-----------------------------
    // parameters for Vector.dot()
    //-----------------------------
    typedef struct
    Params_Vector_Dot
    {
        void *vector1;
        void *vector2;
        int   length;
        void *result_OUT;
        MyTypeChoice type;
    }
    Params_Vector_Dot;
    //-----------------------------

    //-----------------------------
    // parameters for Vector.norm()
    //-----------------------------
    typedef struct 
    Params_Vector_o2Norm
    {
        void *vector;
        int   length;
        void *result_OUT;
        MyTypeChoice type;
    }
    Params_Vector_o2Norm;
    //-----------------------------

    //-----------------------------
    // parameters for Vector.add()
    //-----------------------------
    typedef struct 
    Params_Vector_Add
    {
        void *vector1;
        void *vector2;
        int   length;
        void *result_OUT;
        MyTypeChoice type;
    }
    Params_Vector_Add;
    //-----------------------------

    typedef struct 
    Params_Vector_Dot_float
    {
        float *vector1;
        float *vector2;
        int    length;
        int    threads;
        float *result_OUT;
    }
    Params_Vector_Dot_float;


    typedef struct 
    Params_Vector_o2Norm_float
    {
        float *vector;
        int    length;
        float *result_OUT;
    }
    Params_Vector_o2Norm_float;


    typedef struct
    Params_Vector_Add_float
    {
        float *vector1;
        float *vector2;
        int    length;
        float *result_OUT;
    }
    Params_Vector_Add_float;

    typedef struct 
    Params_Vector_Scale_int
    {
        int *vector;
        int  length;
        int  scalar;
    }
    Params_Vector_Scale_int;


    typedef struct 
    Params_Vector_Scale_float
    {
        float *vector;
        int    length;
        float  scalar;
    }
    Params_Vector_Scale_float;

    typedef struct 
    Params_Vector_Copy_float
    {
        float *vector_from;
        float *vector_to;
        int    length;
    }
    Params_Vector_Copy_float;


## module_Vector parameter initializers

    Params_Vector_Dot_float
    new_Params_Dot_float(void)
    {
        Params_Vector_Dot_float
        obj = {
            .vector1 = NULL,
            .vector2 = NULL,
            .length = 0,
            .threads = 1,
            .result_OUT = NULL,
        };
        return obj;
    }

## module_Vector front-end functions

A hello-world function.

    static void
    hi(void)
    {
        printf("Greetings from module_Vector!\n");
    }


Front-end function dot
    static void
    dot(Params_Vector_Dot input)
    {
        module_Type
        Type = import_Type();

        if (Type.same(input.type, Type.Float))
        {
            printf("(from Vector.dot) your input type is float\n");

            Params_Vector_Dot_float
            inputs = {
                .vector1 = (float*) input.vector1,
                .vector2 = (float*) input.vector2,
                .length  = input.length,
                .result_OUT = (float*) input.result_OUT,
            };

            dot_float(inputs);
        }

    }


Front-end function norm.
    static void
    norm(Params_Vector_o2Norm input)
    {
        module_Type
        Type = import_Type();

        if (Type.same(input.type, Type.Float))
        {
            printf("(from Vector.norm) your input type is float\n");

            Params_Vector_o2Norm_float
            inputs = {
                .vector = (float*) input.vector,
                .length = input.length,
                .result_OUT = (float*) input.result_OUT,
            };

            o2_norm_float(inputs);
        }
    }


Front-end function add.
    static void
    add(Params_Vector_Add input)
    {
        module_Type 
        Type = import_Type();

        if (Type.same(input.type, Type.Float))
        {
            printf("(from Vector.add) your input type is float\n");

            Params_Vector_Add_float
            inputs = {
                .vector1 = (float*) input.vector1,
                .vector2 = (float*) input.vector2,
                .length  = input.length,
                .result_OUT = (float*) input.result_OUT,
            };

            add_float(inputs);
        }
    }



## module_Vector worker functions
    
Worker function print_int 
    //------------------------------------
    // print_int()
    //------------------------------------
    static void
    print_int(Params_Vector_Print_int input)
    {
        for (int i = 0; i < input.length; i++)
        {
            printf("%d ", input.vector[i]);
        }
        puts("");
    }

Worker function print_float 
    //------------------------------------
    // print_float()
    //------------------------------------
    static void
    print_float(Params_Vector_Print_float input)
    {
        for (int i = 0; i < input.length; i++)
        {
            printf("%.*f ", input.precision, input.vector[i]);
        }
        puts("");
    }


Worker function reset_float
    static void
    reset_float(Params_Vector_Reset_float input)
    {
        for (int i = 0; i < input.length; i++)
        {
            input.vector[i] = input.value;
        }
    }

Worker function dot_float
    //------------------------------------
    // dot_float()
    //------------------------------------


    static void
    dot_float(Params_Vector_Dot_float input)
    {
        float sum = 0.;

        for (int i = 0; i < input.length; i++)
        {
            sum += input.vector1[i] * input.vector2[i];
        }

        *input.result_OUT = sum;
    }
    //------------------------------------



Worker function omp_dot_float
    //------------------------------------
    // dot_float()
    //------------------------------------


    static void
    omp_dot_float(Params_Vector_Dot_float input)
    {
        int vector_length = input.length;

        /***********************************
         *         OpenMP setup            *
         ***********************************/
        omp_set_num_threads(input.threads);

        int i_OMP_ThreadCount = input.threads;

        if ( i_OMP_ThreadCount > vector_length )
        {
            i_OMP_ThreadCount = vector_length;
        }

        /***********************************/

        float sum = 0.;

        #pragma omp parallel for reduction(+:sum)
        for ( int i = 0; i < input.length; i++ )
        {
            sum += input.vector1[i] * input.vector2[i];
        }

        // int i_OMP_BlockSize = vector_length / i_OMP_ThreadCount;
        // float f_LocalSumArray[i_OMP_ThreadCount];

        // #pragma omp parallel for 
        // for ( int i = 0; i < i_OMP_ThreadCount; i++ )
        // {
        //     int i_OMP_ThisId = omp_get_thread_num();
        //     int i_Eon = i_OMP_ThisId * i_OMP_BlockSize;
        //     int i_End = ( i_OMP_ThisId + 1 ) * i_OMP_BlockSize;

        //     if ( ( i_OMP_ThisId == (i_OMP_ThreadCount - 1) ) &&
        //          ( i_End < vector_length ) )
        //     { 
        //         i_End = vector_length; 
        //     }

        //     int i_LocalSum = 0;
        //     for ( int j = i_Eon; j < i_End; j++ )
        //     {
        //         i_LocalSum += input.vector1[j] * input.vector2[j];
        //     }

        //     f_LocalSumArray[i] = i_LocalSum;
        // }        

        // for ( int i = 0; i < i_OMP_ThreadCount; i++ )
        // {
        //     sum += f_LocalSumArray[i];
        // }
        *input.result_OUT = sum;
    }
    //------------------------------------


Worker function o2_norm_float
    //------------------------------------
    // o2_norm_float()
    //------------------------------------

    static void
    o2_norm_float(Params_Vector_o2Norm_float input)
    {
        float *output = input.result_OUT;

        /**************************
         * must start from zero
         **************************/
        *output = 0.;

        for (int i = 0; i < input.length; i++)
        {
            *output += input.vector[i] * input.vector[i];
        }
        // note: the sqrt function provided by math.h
        // takes a double and output a double.
        // There's why I have to do a cast.

        int module_Vector_o2_norm_float_tmp_output = *output;
        assert(module_Vector_o2_norm_float_tmp_output > 0);
        *output = (float)sqrt((double) *output);
    }

Workder function add_float
    //------------------------------------
    // add_float()
    //------------------------------------


    static void
    add_float(Params_Vector_Add_float input)
    {
        for (int i = 0; i < input.length; i++)
        {
            input.result_OUT[i] = 
                input.vector1[i] + input.vector2[i];
        }
    }
    //------------------------------------


Workder function scale_int
    static void
    scale_int(Params_Vector_Scale_int input)
    {
        for (int i = 0; i < input.length; i++)
        {
            input.vector[i] *= input.scalar;
        }
    }

Workder function scale_float
    static void
    scale_float(Params_Vector_Scale_float input)
    {
        for (int i = 0; i < input.length; i++)
        {
            input.vector[i] *= input.scalar;
        }
    }

Workder function copy_float.
    static void
    copy_float(Params_Vector_Copy_float input)
    {
        int module_Vector_copy_float__input_length = input.length;
        assert(module_Vector_copy_float__input_length > 0);
        
        for (int i = 0; i < input.length; i++)
        {
            *input.vector_to = *input.vector_from;
            input.vector_from++;
            input.vector_to++;
        }
    }

    //===========================================

