/**********************************************************
 Copyright (c) 2015 YUHANG WANG 
 ALL RIGHTS RESERVED

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
************************************************************/

#ifndef MODULE_VECTOR_H
#define MODULE_VECTOR_H

#include "module_Vector.dep.h"

//-----------------------------
// parameters for Vector.print_int()
//-----------------------------
typedef struct 
Params_Vector_Print_int
{
    int *vector;
    int  length;
}
Params_Vector_Print_int;
//-----------------------------

//-----------------------------
// parameters for Vector.print_float()
//-----------------------------
typedef struct 
Params_Vector_Print_float
{
    float *vector;
    int    length;
    int    precision;
}
Params_Vector_Print_float;
//-----------------------------

//-----------------------------
// parameters for Vector.reset()
//-----------------------------
typedef struct 
Params_Vector_Reset_float
{
    float *vector;
    float  value;
    int    length;
}
Params_Vector_Reset_float;

//-----------------------------
// parameters for Vector.dot()
//-----------------------------
typedef struct
Params_Vector_Dot
{
    void *vector1;
    void *vector2;
    int   length;
    void *result_OUT;
    MyTypeChoice type;
}
Params_Vector_Dot;
//-----------------------------

//-----------------------------
// parameters for Vector.norm()
//-----------------------------
typedef struct 
Params_Vector_o2Norm
{
    void *vector;
    int   length;
    void *result_OUT;
    MyTypeChoice type;
}
Params_Vector_o2Norm;
//-----------------------------

//-----------------------------
// parameters for Vector.add()
//-----------------------------
typedef struct 
Params_Vector_Add
{
    void *vector1;
    void *vector2;
    int   length;
    void *result_OUT;
    MyTypeChoice type;
}
Params_Vector_Add;
//-----------------------------

typedef struct 
Params_Vector_Dot_float
{
    float *vector1;
    float *vector2;
    int    length;
    int    threads;
    float *result_OUT;
}
Params_Vector_Dot_float;

typedef struct 
Params_Vector_o2Norm_float
{
    float *vector;
    int    length;
    float *result_OUT;
}
Params_Vector_o2Norm_float;

typedef struct
Params_Vector_Add_float
{
    float *vector1;
    float *vector2;
    int    length;
    float *result_OUT;
}
Params_Vector_Add_float;

typedef struct 
Params_Vector_Scale_int
{
    int *vector;
    int  length;
    int  scalar;
}
Params_Vector_Scale_int;

typedef struct 
Params_Vector_Scale_float
{
    float *vector;
    int    length;
    float  scalar;
}
Params_Vector_Scale_float;

typedef struct 
Params_Vector_Copy_float
{
    float *vector_from;
    float *vector_to;
    int    length;
}
Params_Vector_Copy_float;

typedef struct 
module_Vector
{
    void (*hi)(void);
    void (*print_int)(Params_Vector_Print_int);
    void (*print_float)(Params_Vector_Print_float);
    void (*reset_float)(Params_Vector_Reset_float);
    void (*dot)(Params_Vector_Dot);
    void (*dot_float)(Params_Vector_Dot_float);
    void (*omp_dot_float)(Params_Vector_Dot_float);
    void (*norm)(Params_Vector_o2Norm);
    void (*o2_norm_float)(Params_Vector_o2Norm_float);
    void (*add)(Params_Vector_Add);
    void (*add_float)(Params_Vector_Add_float);
    void (*scale_int)(Params_Vector_Scale_int);
    void (*scale_float)(Params_Vector_Scale_float);
    void (*copy_float)(Params_Vector_Copy_float);
    Params_Vector_Dot_float (*new_Params_Dot_float)(void);
}
module_Vector;

module_Vector
import_Vector(void);

#endif