#include "test_module_Vector.dep.c"

static int 
microsecond(clock_t clock_TimeCost)
{
    long int i_TimeCost = (clock_TimeCost * 1000000) / CLOCKS_PER_SEC;
    return i_TimeCost;
}

static bool
test_hi(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return true; }

    puts("\nTest Vector.hi()\n");

    module_Vector
    Vector = import_Vector();
    Vector.hi();
    return true;
}


static bool
test_print_int(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return true; }

    puts("\ntest Vector.print_int()\n");

    module_Vector
    Vector = import_Vector();

    int length = 3;
    int vector[] = {1, 2, 3};
    Params_Vector_Print_int
    params = {
        .vector = vector,
        .length = length,
    };

    Vector.print_int(params);
    return true;
}


static bool
test_print_float(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return true; }

    puts("\ntest Vector.print_float()\n");

    module_Vector
    Vector = import_Vector();

    int length = 3;
    float vector[] = {1., 2., 3.};
    Params_Vector_Print_float
    params = {
        .vector = vector,
        .length = length,
        .precision = 0,
    };

    Vector.print_float(params);
    return true;
}

static bool
test_reset_float(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return true; }

    puts("\ntest Vector.reset_float()\n");

    module_Vector
    Vector = import_Vector();

    module_Assert
    Assert = import_Assert();

    float answer[3];
    int   length = 3;
    float new_value = 0.;

    Params_Vector_Reset_float
    input = {
        .vector = answer,
        .length = length,
        .value  = new_value,
    };

    Vector.reset_float(input);

    float expect[3] = {0.};
    
    Params_Vector_Print_float
    params_print = {
        .vector = answer,
        .length = length,
        .precision = 3,
    };
    printf("answer: "); Vector.print_float(params_print);
    
    params_print.vector = expect;
    printf("expect: "); Vector.print_float(params_print);
    
    Params_Assert_EqualArray_float
    input_assert = {
        .input1 = answer,
        .input2 = expect,
        .tolerance = 1.E-5,
    };
    Assert.equal_array_float(input_assert);
    return true;
}

static bool 
test_dot(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return true; }

    puts("\nTest Vector.dot()\n");

    module_Type
    Type = import_Type();

    module_Vector
    Vector = import_Vector();

    module_Assert
    Assert = import_Assert();

    int   length    = 2;
    float vector1[] = {1., 2.};
    float vector2[] = {1., 2.};
    float expect =  5.;
    float result =  0.;
    float tolerance  = 1.0E-5;
    MyTypeChoice type = Type.Float;

    Params_Vector_Dot
    params = {
        .vector1 = vector1,
        .vector2 = vector2,
        .length  = length,
        .result_OUT = &result,
        .type  = type,
    };

    Vector.dot(params);

    printf("result = %f key = %f\n", result, expect);

    Params_Assert_Equal_float 
    inputs = {
        .input1 = result,
        .input2 = expect,
        .tolerance = tolerance,
    };

    Assert.equal_float(inputs);
    return true;
}

static bool 
test_dot_float(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return true; }

    puts("\nTest Vector.dot_float()\n");

    //----------------------------------
    // import
    //----------------------------------
    module_Vector
    Vector = import_Vector();

    module_Assert
    Assert = import_Assert();
    //----------------------------------

    int   length    = 1000000;

    float vector1[length];
    for ( int i = 0; i < length; i++ ) { vector1[i] = 1.; }
        
    float vector2[length];
    for ( int i = 0; i < length; i++ ) { vector2[i] = 1.; }

    int   omp_thread_count = 1;
    float expect =  length;
    float result =  0.;
    float tolerance  = 1.0E-5;

    Params_Vector_Dot_float
    params = Vector.new_Params_Dot_float();
    params.vector1 = vector1;
    params.vector2 = vector2;
    params.length  = length;
    params.threads = omp_thread_count;
    params.result_OUT = &result;

    clock_t clock_eon = clock();
    Vector.dot_float(params);
    clock_t clock_end = clock();

    int i_TimeCost = microsecond(clock_end - clock_eon);

    printf("No OMP; t = %d microseconds\n", i_TimeCost);

    printf("result = %f key = %f\n", result, expect);

    Params_Assert_Equal_float 
    inputs = {
        .input1 = result,
        .input2 = expect,
        .tolerance = tolerance,
    };

    Assert.equal_float(inputs);
    return true;
}

static bool 
test_omp_dot_float(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return true; }

    puts("\nTest Vector.omp_dot_float()\n");

    //----------------------------------
    // import
    //----------------------------------
    module_Vector
    Vector = import_Vector();

    module_Assert
    Assert = import_Assert();
    //----------------------------------

    int   length    = 1000000;

    float vector1[length];
    for ( int i = 0; i < length; i++ ) { vector1[i] = 1.; }
        
    float vector2[length];
    for ( int i = 0; i < length; i++ ) { vector2[i] = 1.; }

    int   omp_thread_count = 4;
    float expect =  length;
    float result =  0.;
    float tolerance  = 1.0E-5;

    Params_Vector_Dot_float
    params = Vector.new_Params_Dot_float();
    params.vector1 = vector1;
    params.vector2 = vector2;
    params.length  = length;
    params.threads = omp_thread_count;
    params.result_OUT = &result;

    clock_t clock_eon = clock();
    Vector.omp_dot_float(params);
    clock_t clock_end = clock();

    int i_TimeCost = microsecond(clock_end - clock_eon);

    printf("OpenMP %d threads; t = %d microseconds\n",
        omp_thread_count, i_TimeCost);

    printf("result = %f key = %f\n", result, expect);

    Params_Assert_Equal_float 
    inputs = {
        .input1 = result,
        .input2 = expect,
        .tolerance = tolerance,
    };

    Assert.equal_float(inputs);
    return true;
}

static bool
test_norm(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return true; }

    puts("\n Test Vector.norm()\n");
    module_Type
    Type = import_Type();

    module_Vector
    Vector = import_Vector();

    module_Assert
    Assert = import_Assert();
    
    int length = 2;
    float vector[] = {1., 1.};
    float expect = (float)sqrt(2.);
    float result = 0.;
    float tolerance = 1.0E-5;
    MyTypeChoice type = Type.Float;

    Params_Vector_o2Norm
    params = {
        .vector = vector,
        .length = length,
        .result_OUT = &result,
        .type = type,
    };

    Vector.norm(params);

    printf("result = %f key = %f\n", result, expect);

    Params_Assert_Equal_float 
    inputs = {
        .input1 = result,
        .input2 = expect,
        .tolerance = tolerance,
    };

    Assert.equal_float(inputs);
    return true;
}

static bool
test_o2_norm_float(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return true; }

    puts("\n Test Vector.o2_norm_float()\n");

    module_Vector
    Vector = import_Vector();

    module_Assert
    Assert = import_Assert();
    
    int length = 2;
    float vector[] = {1., 1.};
    float expect = (float)sqrt(2.);
    float result = -9999999.;
    float tolerance = 1.0E-5;

    Params_Vector_o2Norm_float
    params = {
        .vector = vector,
        .length = length,
        .result_OUT = &result,
    };

    Vector.o2_norm_float(params);

    printf("result = %f key = %f\n", result, expect);

    Params_Assert_Equal_float 
    inputs = {
        .input1 = result,
        .input2 = expect,
        .tolerance = tolerance,
    };

    Assert.equal_float(inputs);
    return true;
}

static bool
test_add(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return true; }

    puts("\n Test Vector.add()\n");

    module_Type
    Type = import_Type();

    module_Vector
    Vector = import_Vector();

    module_Assert
    Assert = import_Assert();
    
    int length = 2;
    float vector1[] = {1., 1.};
    float vector2[] = {0., 1.};
    float result[] = {0., 0.};
    float expect[]  = {1., 2.};
    float tolerance = 1.E-5;
    MyTypeChoice type = Type.Float;

    Params_Vector_Add
    params = {
        .vector1 = vector1,
        .vector2 = vector2,
        .length  = length,
        .result_OUT = result,
        .type = type,
    };

    Vector.add(params);

    Params_Assert_EqualArray_float
    inputs = {
        .input1 = result,
        .input2 = expect,
        .length = length,
        .tolerance = tolerance,
    };

    Assert.equal_array_float(inputs);
    return true;
}

static bool
test_add_float(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return true; }

    puts("\n Test Vector.add_float()\n");

    module_Vector
    Vector = import_Vector();

    module_Assert
    Assert = import_Assert();
    
    int length = 2;
    float vector1[] = {1., 1.};
    float vector2[] = {0., 1.};
    float expect[]  = {1., 2.};
    float result[]  = {0., 0.};
    float tolerance = 1.E-5;

    Params_Vector_Add_float
    params = {
        .vector1 = vector1,
        .vector2 = vector2,
        .length  = length,
        .result_OUT = result,
    };

    Vector.add_float(params);

    Params_Assert_EqualArray_float
    inputs = {
        .input1 = result,
        .input2 = expect,
        .length = length,
        .tolerance = tolerance,
    };

    Assert.equal_array_float(inputs);
    return true;
}

static bool
test_scale_int(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return true; }

    puts("\n Test Vector.scale_int()\n");

    module_Vector
    Vector = import_Vector();

    module_Assert
    Assert = import_Assert();
    
    int length = 2;
    int vector[] = {1, 2};
    int scalar   = 2.;
    int expect[]  = {2, 4};

    Params_Vector_Scale_int
    params = {
        .vector  = vector,
        .length  = length,
        .scalar  = scalar,
    };

    Vector.scale_int(params);

    int *result = vector;
 
    Params_Assert_EqualArray_int
    inputs = {
        .input1 = result,
        .input2 = expect,
        .length = length,
    };

    Assert.equal_array_int(inputs);
    return true;
}

static bool
test_scale_float(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return true; }

    puts("\n Test Vector.scale_float()\n");

    module_Vector
    Vector = import_Vector();

    module_Assert
    Assert = import_Assert();
    
    int length = 2;
    float vector[] = {1., 1.};
    float scalar   = 2.;
    float expect[]  = {2., 2.};
    float tolerance = 1.E-5;

    Params_Vector_Scale_float
    params = {
        .vector  = vector,
        .length  = length,
        .scalar  = scalar,
    };

    Vector.scale_float(params);

    float *result = vector;
 
    Params_Assert_EqualArray_float
    inputs = {
        .input1 = result,
        .input2 = expect,
        .length = length,
        .tolerance = tolerance,
    };

    Assert.equal_array_float(inputs);
    return true;
}

static bool 
test_copy_float(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return true; }

    puts("\nTest Vector.copy_float()\n");

    module_Vector
    Vector = import_Vector();

    module_Assert
    Assert = import_Assert();

    int   length = 3;
    float answer[3];
    float source[5] = {0., 0., 1., 2., 3.};
    float expect[3] = {1., 2., 3.};

    Params_Vector_Copy_float
    input = {
        .vector_from = &source[2],
        .vector_to   = &answer[0],
        .length      = length,
    };

    Vector.copy_float(input);

    Params_Vector_Print_float
    params_print = {
        .vector = answer,
        .length = length,
    };
    printf("answer: "); Vector.print_float(params_print);
    params_print.vector = expect;
    printf("expect: "); Vector.print_float(params_print);
    
    Params_Assert_EqualArray_float
    params_assert = {
        .input1 = answer,
        .input2 = expect,
        .length = length,
        .tolerance = 1.E-5,
    };
    Assert.equal_array_float(params_assert);

    return true;
}

int
main(void)
{
    puts("-------------------------");
    test_hi(false);
    puts("-------------------------");
    test_print_int(false);
    puts("-------------------------");
    test_print_float(false);
    puts("-------------------------");
    test_reset_float(false);
    puts("-------------------------");
    test_dot(false);
    puts("-------------------------");
    test_dot_float(true);
    puts("-------------------------");
    test_omp_dot_float(true);
    puts("-------------------------");
    test_norm(false);
    puts("-------------------------");
    test_o2_norm_float(false);
    puts("-------------------------");
    test_add(false);
    puts("-------------------------");
    test_add_float(false);
    puts("-------------------------");
    test_scale_int(false);
    puts("-------------------------");
    test_scale_float(false);
    puts("-------------------------");
    test_copy_float(false);
    puts("-------------------------");
    return 0;
}