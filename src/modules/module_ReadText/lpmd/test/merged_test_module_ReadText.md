# LICENSE
    /**********************************************************
     Copyright (c) 2015 YUHANG WANG 
     ALL RIGHTS RESERVED

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
    ************************************************************/



# LICENSE
    /**********************************************************
     Copyright (c) 2015 YUHANG WANG 
     ALL RIGHTS RESERVED

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
    ************************************************************/



# Module test_module_ReadText

#[test_module_ReadText.dep.h](#test_module_ReadText.dep.h "save:")
    _"LICENSE"

    
#[test_module_ReadText.h](#test_module_ReadText.h "save:")
    _"LICENSE"

    #include "test_module_ReadText.dep.h"
    #include "module_ReadText.h"


#[test_module_ReadText.dep.c](#test_module_ReadText.dep.c "save:")
    _"LICENSE"

    #include "test_module_ReadText.h"
    #include "module_Assert.h"
    #include <stdio.h>
    #include <stdbool.h>

#[test_module_ReadText.c](#test_module_ReadText.c "save:")
    _"LICENSE"

    #include "test_module_ReadText.dep.c"

    _"test_module_ReadText all tests"

    int
    main(int i_ArgumentCount, char *c_ArgumentArray[])
    {
        puts("----------------------------");
        test_to_array_float(true);
        puts("----------------------------");

        return 0;
    }

## test_module_ReadText all tests

Test ReadText.to_array_float 
    static bool 
    test_to_array_float(bool bool_RunTest)
    {
        if ( ! bool_RunTest ) { return true; }

        puts("\nRun test_to_array_float()\n");

        //------------------------------------
        // Import
        //------------------------------------
        module_ReadText
        ReadText = import_ReadText();

        module_Assert
        Assert = import_Assert();

        //------------------------------------

        char *c_FileName = "matrix_1x1.txt";
        float answer[1] = {9.};

        Params_ReadText_ToArray_float
        params = ReadText.new_Params_ToArray_float();
        params.file_name = c_FileName;
        params.results_OUT = answer;
        params.results_length = 1;

        int status = ReadText.to_array_float(params);

        if ( status == 0 ) 
        {
            puts("File reading OK!");
        }
        else
        {
            printf("Cannot read the file %s\n",
                c_FileName);
            printf("error status: %d\n", status);
        }


        float expect = 1.0;

        Params_Assert_Equal_float
        params_assert = {
            .input1 = answer[0],
            .input2 = expect,
            .tolerance = 1.E-5,
        };
        Assert.equal_float(params_assert);

        printf("answer: %f\n", answer[0]);
        printf("expect: %f\n", expect);

        
        return true;
    }