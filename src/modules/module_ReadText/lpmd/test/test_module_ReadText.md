# Module test_module_ReadText

#[test_module_ReadText.dep.h](#test_module_ReadText.dep.h "save:")
    _"LICENSE"

    
#[test_module_ReadText.h](#test_module_ReadText.h "save:")
    _"LICENSE"

    #include "test_module_ReadText.dep.h"
    #include "module_ReadText.h"


#[test_module_ReadText.dep.c](#test_module_ReadText.dep.c "save:")
    _"LICENSE"

    #include "test_module_ReadText.h"
    #include "module_Assert.h"
    #include <stdio.h>
    #include <stdbool.h>

#[test_module_ReadText.c](#test_module_ReadText.c "save:")
    _"LICENSE"

    #include "test_module_ReadText.dep.c"

    _"test_module_ReadText all tests"

    int
    main(int i_ArgumentCount, char *c_ArgumentArray[])
    {
        puts("----------------------------");
        test_to_array_float(true);
        puts("----------------------------");

        return 0;
    }

## test_module_ReadText all tests

Test ReadText.to_array_float 
    static bool 
    test_to_array_float(bool bool_RunTest)
    {
        if ( ! bool_RunTest ) { return true; }

        puts("\nRun test_to_array_float()\n");

        //------------------------------------
        // Import
        //------------------------------------
        module_ReadText
        ReadText = import_ReadText();

        module_Assert
        Assert = import_Assert();

        //------------------------------------

        char *c_FileName = "matrix_1x1.txt";
        float answer[1] = {9.};

        Params_ReadText_ToArray_float
        params = ReadText.new_Params_ToArray_float();
        params.file_name = c_FileName;
        params.results_OUT = answer;
        params.results_length = 1;

        int status = ReadText.to_array_float(params);

        if ( status == 0 ) 
        {
            puts("File reading OK!");
        }
        else
        {
            printf("Cannot read the file %s\n",
                c_FileName);
            printf("error status: %d\n", status);
        }


        float expect = 1.0;

        Params_Assert_Equal_float
        params_assert = {
            .input1 = answer[0],
            .input2 = expect,
            .tolerance = 1.E-5,
        };
        Assert.equal_float(params_assert);

        printf("answer: %f\n", answer[0]);
        printf("expect: %f\n", expect);

        
        return true;
    }