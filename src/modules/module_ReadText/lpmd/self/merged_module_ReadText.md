# LICENSE
    /**********************************************************
     Copyright (c) 2015 YUHANG WANG 
     ALL RIGHTS RESERVED

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
    ************************************************************/



# LICENSE
    /**********************************************************
     Copyright (c) 2015 YUHANG WANG 
     ALL RIGHTS RESERVED

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
    ************************************************************/



# Module module_ReadText

#[module_ReadText.dep.h](#module_ReadText.dep.h "save:")
    _"LICENSE"

    #include <stdbool.h>


#[module_ReadText.h](#module_ReadText.h "save:")
    _"LICENSE"

    #include "module_ReadText.dep.h"

    _"module_ReadText function parameter types"

    _"module_ReadText interface"


#[module_ReadText.dep.c](#module_ReadText.dep.c "save:")
    _"LICENSE"

    #include "module_ReadText.h"
    #include <stdio.h>
    #include <stdbool.h>

#[module_ReadText.c](#module_ReadText.c "save:")
    _"LICENSE"

    #include "module_ReadText.dep.c"

    _"module_ReadText parameter initializers"

    _"module_ReadText front-end functions"

    _"module_ReadText internal functions"

    _"module_ReadText export"



## module_ReadText function parameter types
    typedef struct 
    Params_ReadText_ToArray_float
    {
        char  *file_name;
        int    results_length;
        float *results_OUT;
    }
    Params_ReadText_ToArray_float;

## module_ReadText interface
    typedef struct 
    module_ReadText
    {
        int (*to_array_float)(Params_ReadText_ToArray_float);
        Params_ReadText_ToArray_float (*new_Params_ToArray_float)(void);
    }
    module_ReadText;

    module_ReadText
    import_ReadText(void);


## module_ReadText export
    module_ReadText
    import_ReadText(void)
    {
        module_ReadText
        obj = {
            .to_array_float = to_array_float,
            .new_Params_ToArray_float = new_Params_ToArray_float,
        };
        return obj;
    }


## module_ReadText parameter initializers
    Params_ReadText_ToArray_float
    new_Params_ToArray_float(void)
    {
        Params_ReadText_ToArray_float
        obj = {
            .file_name = NULL,
            .results_length = -1,
            .results_OUT = NULL,
        };
        return obj;
    }

## module_ReadText front-end functions
    static int
    to_array_float(Params_ReadText_ToArray_float params)
    {
        FILE *p_File = fopen(params.file_name, "r");
        
        float *output = params.results_OUT; // alias

        int index = 0;

        // cannot open the file
        if ( p_File == NULL ) 
        {
            puts("cannot open file");
            return 1;
        }

        int OK = 1;
        while ( fscanf(p_File, "%f", &output[index]) != OK )
        {
            // read error
            if ( ferror(p_File) )
            {
                fclose(p_File);
                return 2;
            }

            // end of file
            if ( feof(p_File) )
            {
                fclose(p_File);
                return 0;
            }

            // consume the newline
            fscanf(p_File, "%*[\n]");

            index++;

            // already filled ip the result array
            if ( index >= params.results_length )
            {
                fclose(p_File);
                return 0;
            }
        }

        fclose(p_File);

        return 0;
    }

## module_ReadText internal functions
    //

