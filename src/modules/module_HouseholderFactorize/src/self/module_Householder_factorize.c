#include "module_Householder_factorize.dep.c"

static void
hi(void)
{
    puts("Hello world!");
}

module_Householder_factorize 
import_Householder_factorize(void)
{
    module_Householder_factorize obj = {
        .hi = hi,
    };

    return obj;
}