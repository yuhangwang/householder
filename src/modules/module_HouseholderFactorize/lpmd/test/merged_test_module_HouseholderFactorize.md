# LICENSE
    /**********************************************************
     Copyright (c) 2015 YUHANG WANG 
     ALL RIGHTS RESERVED

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
    ************************************************************/



# LICENSE
    /**********************************************************
     Copyright (c) 2015 YUHANG WANG 
     ALL RIGHTS RESERVED

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
    ************************************************************/



# Test module_HouseholderFactorize

#[test_module_HouseholderFactorize.dep](#test_module_HouseholderFactorize.dep "save:")
    "mpi.h"
    ===
    "mpi.h"
    <stdio.h>

#[test_module_HouseholderFactorize.dep.h](#test_module_HouseholderFactorize.dep.h "save:")
    
    _"LICENSE"


#[test_module_HouseholderFactorize.h](#test_module_HouseholderFactorize.h "save:")
    
    _"LICENSE"
    
    #include "test_module_HouseholderFactorize.dep.h"


#[test_module_HouseholderFactorize.dep.c](#test_module_HouseholderFactorize.dep.c "save:")
    
    _"LICENSE"

    #include "module_HouseholderFactorize.h"
    #include "module_Matrix.h"
    #include "module_Vector.h"
    #include "module_Assert.h"
    #include <stdio.h>
    #include <math.h>
    #include <stdbool.h>
    #include <time.h>
    #include "mpi.h"
    #include <stdlib.h>

#[test_module_HouseholderFactorize.c](#test_module_HouseholderFactorize.c "save:")
    
    _"LICENSE"

    #include "test_module_HouseholderFactorize.dep.c"

    _"module_HouseholderFactorize test fixture"

    _"module_HouseholderFactorize all tests"



    int
    main(int i_ArgumentCount, char *c_ArgumentArray[])
    {
        int matrix_row_count = 4;
        int omp_thread_count = 1;
        int mpi_task_mapping_style = 1;

        if ( i_ArgumentCount > 1 )
        {
            matrix_row_count = atoi(c_ArgumentArray[1]);
        }

        if ( i_ArgumentCount > 2 )
        {
            omp_thread_count = atoi(c_ArgumentArray[2]);
        }

        if ( i_ArgumentCount > 3 )
        {
            mpi_task_mapping_style = atoi(c_ArgumentArray[3]);
        }

        printf("matrix size: %d\n", matrix_row_count);

        // Replace macro by a meaningful and safe variable
        MPI_Comm mpi_communicator = MPI_COMM_WORLD;

        MPI_Init(&i_ArgumentCount, &c_ArgumentArray);

        puts("--------------------------------");
        test_hi(false);
        puts("--------------------------------");
        test_factorize_float_internals(false);
        puts("--------------------------------");
        test_factorize_float(false, omp_thread_count);
        puts("--------------------------------");
        test_factorize_float_Time(false, matrix_row_count, omp_thread_count);
        puts("--------------------------------");
        test_factorize_mpi_1D_float(false, mpi_communicator, omp_thread_count, mpi_task_mapping_style);
        puts("--------------------------------");
        test_factorize_mpi_1D_float_L(false, mpi_communicator, omp_thread_count, mpi_task_mapping_style);
        puts("--------------------------------");
        test_factorize_mpi_1D_float_Time(true, mpi_communicator, matrix_row_count, omp_thread_count, mpi_task_mapping_style);
        puts("--------------------------------");

        MPI_Finalize();
        return 0;
    }    

## module_HouseholderFactorize test fixture
    typedef struct 
    Params_Fixture
    {
        float *A;
        float *V_expect;
        float *R_expect;
        int    matrix_row_count;
        int    matrix_column_count;
        void (*factorize)(Params_HouseholderFactorize_float);
        module_HouseholderFactorize factorizer;
        Params_HouseholderFactorize_float *p_Params;
    }
    Params_Fixture;

    static int 
    microsecond(clock_t clock_TimeCost)
    {
        long int i_TimeCost = (clock_TimeCost * 1000000) / CLOCKS_PER_SEC;
        return i_TimeCost;
    }


    static void
    case1_3x3_matrix(Params_Fixture fixture)
    {
        module_Matrix
        Matrix = import_Matrix();


        //---------------------------------------------
        // define test case content
        //---------------------------------------------
        float 
        A[9] = 
            {1., 0., 0., 
             0., 1., 0.,
             0., 0., 1.};

        float 
        V[9] = 
            {2., 0., 0., 
             0., 2., 0.,
             0., 0., 0.};

        float 
        R[9] =
            {-1., 0., 0., 
             0., -1., 0.,
             0., 0.,  1.};

        int i_RowCount = 3;
        int i_ColumnCount = 3;

        //---------------------------------------------
        // update the content of matrix A and V_expect
        //---------------------------------------------
        Class_MatrixIndex2D
        obj_Id = Matrix.new_MatrixIndex2D(i_RowCount, i_ColumnCount);
        
        for (int i = 0; i < i_RowCount; i++)
        {
            for (int j = 0; j < i_ColumnCount; j++)
            {
                int index = obj_Id.index(obj_Id, i, j);
                fixture.A[index] = A[index];
                fixture.V_expect[index] = V[index];   
                fixture.R_expect[index] = R[index];
            }
        }
        //---------------------------------------------
    }


    static void
    case2_3x3_matrix(Params_Fixture fixture)
    {
        module_Matrix
        Matrix = import_Matrix();


        //---------------------------------------------
        // define test case content
        //---------------------------------------------
        int i_RowCount = 3;
        int i_ColumnCount = 3;
        Class_MatrixIndex2D
        obj_Id = Matrix.new_MatrixIndex2D(i_RowCount, i_ColumnCount);
        
        float 
        A[9] = 
            {1., 0., 0., 
             1.2, 1., 0.,
             1.3, 1.1, 1.};
       
        float 
        R[9] = 
        {
            -2.032240, -1.294138, -0.639688, 
            -0.000000, -0.731578, -0.372011, 
            -0.000000, 0.000000, 0.672612 
        };

        float 
        V[9] = 
        {
            3.032240, 0.000000, 0.000000, 
            1.200000, 1.219426, 0.000000, 
            1.300000, 0.545169, 0.000000 
        };

        //---------------------------------------------
        // update the content of matrix A and V_expect
        //---------------------------------------------
        for (int i = 0; i < i_RowCount; i++)
        {
            for (int j = 0; j < i_ColumnCount; j++)
            {
                int index = obj_Id.index(obj_Id, i, j);
                fixture.A[index] = A[index];
                fixture.V_expect[index] = V[index];
                fixture.R_expect[index] = R[index];
            }
        }
        //---------------------------------------------
    }

    static void
    case3_4x4_matrix(Params_Fixture fixture)
    {
        module_Matrix
        Matrix = import_Matrix();


        //---------------------------------------------
        // define test case content
        //---------------------------------------------
        int i_RowCount = 4;
        int i_ColumnCount = 4;
        Class_MatrixIndex2D
        obj_Id = Matrix.new_MatrixIndex2D(i_RowCount, i_ColumnCount);
        
        float 
        A[16] = 
        {1.000000, 0.000000, 0.000000, 0.000000, 
        1.000000, 1.000000, 0.000000, 0.000000, 
        0.000000, 1.000000, 1.000000, 0.000000, 
        0.000000, 0.000000, 1.000000, 1.000000
        };
       
        float 
        R[16] = 
        {-1.414214, -0.707107, 0.000000, 0.000000, 
        0.000000, -1.224745, -0.816497, 0.000000, 
        0.000000, 0.000000, -1.154701, -0.866025, 
        0.000000, 0.000000, -0.000000, 0.500000
        };

        float 
        V[16] = 
        {2.414214, 0.000000, 0.000000, 0.000000, 
        1.000000, 1.931852, 0.000000, 0.000000, 
        0.000000, 1.000000, 1.732051, 0.000000, 
        0.000000, 0.000000, 1.000000, 0.000000
        };

        //---------------------------------------------
        // update the content of matrix A and V_expect
        //---------------------------------------------
        for (int i = 0; i < i_RowCount; i++)
        {
            for (int j = 0; j < i_ColumnCount; j++)
            {
                int index = obj_Id.index(obj_Id, i, j);
                fixture.A[index] = A[index];
                fixture.V_expect[index] = V[index];
                fixture.R_expect[index] = R[index];
            }
        }
        //---------------------------------------------
    }

    static void
    case1_NxN_matrix(Params_Fixture fixture)
    {
        //---------------------------------
        /* create matrix with 1 on the diagonal
         * and the first lower sub-diagonal
         * Example A:
         * 1 0 0 0 0
         * 1 1 0 0 0
         * 0 1 1 0 0
         * 0 0 1 1 0
         * 0 0 0 1 1
         */
        //---------------------------------
        module_Matrix
        Matrix = import_Matrix();


        //---------------------------------------------
        // define test case content
        //---------------------------------------------
        int i_RowCount = fixture.matrix_row_count;
        int i_ColumnCount = i_RowCount;
        int matrix_length1D = i_RowCount * i_ColumnCount;
        Class_MatrixIndex2D
        obj_Id = Matrix.new_MatrixIndex2D(i_RowCount, i_ColumnCount);
        
        // Create matrix A
        float *A = calloc(matrix_length1D, sizeof(float));
        if ( A == NULL ) { puts("(case1_NxN_matrix): cannot get heap for A"); exit(1); }

        for ( int i = 0; i < i_RowCount; i++ )
        {
            for ( int j = 0; j < i_ColumnCount; j++ )
            {
                int index = obj_Id.index(obj_Id, i, j);
                if ( i == j )
                {
                    A[index] = 1.;
                }
                else if ( (i - 1) == j )
                {
                    A[index] = 1.;
                }
                else
                {
                    A[index] = 0.;
                }
            }
        }

        //################################################
        // Right now I don't have the code ready for 
        // initializing the expected R and V matrix.
        // I will put it on the to-do list.
        //################################################
        float *R = calloc(matrix_length1D, sizeof(float));
        if ( R == NULL ) { puts("(from case1_NxN_matrix): cannot get heap for R"); exit(1); }

        float *V = calloc(matrix_length1D, sizeof(float));
        if ( V == NULL ) { puts("(from case1_NxN_matrix): cannot get heap for V"); exit(1); }
        //################################################
        
        //---------------------------------------------
        // update the content of matrix A and V_expect
        //---------------------------------------------
        for (int i = 0; i < i_RowCount; i++)
        {
            for (int j = 0; j < i_ColumnCount; j++)
            {
                int index = obj_Id.index(obj_Id, i, j);
                fixture.A[index] = A[index];
                fixture.V_expect[index] = V[index];
                fixture.R_expect[index] = R[index];
            }
        }
        //---------------------------------------------
        free(A);
        free(R);
        free(V);
    }

## module_HouseholderFactorize all tests 

    static bool
    test_hi(bool bool_RunTest)
    {
        if (!bool_RunTest) { return true; }

        module_HouseholderFactorize 
                Householder = import_HouseholderFactorize();

        Householder.hi();
        return true;
    }

Test module_HouseholderFactorize.get_householder_vector_float
    static bool
    test_factorize_float_internals(bool bool_RunTest)
    {
        if (!bool_RunTest) { return true; }

        puts("\ntest get_factorize_float_internals()\n");
        
        bool output = false;

        int TEST_CASE_ID = 2;
        printf("TEST CASE: %d\n\n", TEST_CASE_ID);

        //----------------------------------------
        // import
        //----------------------------------------
        module_Vector
        Vector = import_Vector();

        module_Matrix
        Matrix = import_Matrix();

        module_Assert
        Assert = import_Assert();
        
        module_HouseholderFactorize
        HouseholderFactorize = import_HouseholderFactorize();


        //----------------------------------------
        //--------------------------------------
        // Create a test fixture
        //--------------------------------------
        //--------------------------------------
        // Allocate common storage of inputs/outputs
        // for all tests
        //--------------------------------------
        int   i_RowCount    = 3;
        int   i_ColumnCount = 3;
        int matrix_length1D = i_RowCount * i_ColumnCount;
        float A[matrix_length1D]; // target matrix
        // float V[matrix_length1D]; // Householder vector
        // float R[matrix_length1D]; // upper triangular matrix
        float V_expect[matrix_length1D];
        float R_expect[matrix_length1D];
        Class_MatrixIndex2D 
        obj_Id = Matrix.new_MatrixIndex2D(i_RowCount,i_ColumnCount);
        //--------------------------------------

        Params_Fixture
        fixture = {
            .A             = A,
            .V_expect      = V_expect,
            .R_expect      = R_expect,
            .matrix_row_count    = i_RowCount,
            .matrix_column_count = i_ColumnCount,
            .factorizer    = HouseholderFactorize,
            .factorize     = HouseholderFactorize.factorize_float,
        };

        // case1_3x3_matrix(fixture);
        switch (TEST_CASE_ID) 
        {
            case 1: case1_3x3_matrix(fixture); break;
            case 2: case2_3x3_matrix(fixture); break;
            default: printf("Illegal TEST_CASE_ID\n"); break;
        }


        /************************************/
        /*****  aliasing ****/
        /************************************/
        typedef Params_HouseholderFactorize_GetHouseholderBeta_float Params_GetBeta;
        typedef Params_HouseholderFactorize_GetHouseholderGamma_float Params_GetGamma;
        typedef Params_HouseholderFactorize_UpdateMatrixColumn_float Params_UpdateColumn;
        Params_GetBeta (*new_Params_GetBeta)(void) =
            fixture.factorizer.new_Params_GetHouseholderBeta_float;
        Params_GetGamma (*new_Params_GetGamma)(void) =
            fixture.factorizer.new_Params_GetHouseholderGamma_float;
        Params_UpdateColumn (*new_Params_UpdateColumn)(void) = 
            fixture.factorizer.new_Params_UpdateMatrixColumn_float;
        /************************************/


        //----------------------------------------
        // print out input matrix A
        //----------------------------------------
        Params_Matrix_Print_float
        matrix_print_params = {
            .matrix = fixture.A,
            .matrix_row_count    = i_RowCount,
            .matrix_column_count = i_ColumnCount,
            .precision = 1,
        };
        puts("------------------------------");
        puts("input matrix A");
        Matrix.print_float(matrix_print_params);
        puts("------------------------------");
        //----------------------------------------

        //----------------------------------------
        Params_HouseholderFactorize_GetHouseholderVector_float
        inputs = fixture.factorizer.new_Params_GetHouseholderVector_float();
        inputs.matrix = fixture.A;
        inputs.current_column_id = 0;
        inputs.matrix_row_count    = i_RowCount;
        inputs.matrix_column_count = i_ColumnCount;
        inputs.result_OUT = NULL;

        //--------------------------------------------------
        // define the upper bound for column index k
        int i_kSupremum = i_RowCount - 1;
        if (i_kSupremum > i_ColumnCount) 
        {
            i_kSupremum = i_ColumnCount;
        }
        printf("=== i_kSupremum = %d\n", i_kSupremum);
        //------------------------------------------------

        for (int k = 0; k < i_kSupremum; k++)
        {
            // Use C99's variable length array (VLA) feature
            // no longer need malloc.
            // reminder: VLA cannot be initialized.

            //note: since k is zero-based index, there is
            // no (+1) needed to calculate the length of
            // of the Householder vector.

            //------------------------------------------
            // check Householder vector
            //------------------------------------------
            int vector_length = i_RowCount - k;
            float householder_vector[vector_length];
            inputs.result_OUT = householder_vector;
            inputs.current_column_id = k;

            fixture.factorizer.get_householder_vector_float(inputs);

            Params_Vector_Print_float
            params_print_vector = {
                .vector = householder_vector,
                .length = vector_length,
                .precision = 3,
            };
            printf("[k=%d]\nhouseholder_vector: \n", k); 
            Vector.print_float(params_print_vector);

            //-----------------------------------
            // define the expected results
            //-----------------------------------
            float expect[vector_length];
            for (int i = 0; i < vector_length; i++)
            {
                int i_RowId = k + i;
                int i_ColumnId = k;
                printf("%d %d\n", i_RowId, i_ColumnId);
                int index = obj_Id.index(
                    obj_Id, i_RowId, i_ColumnId);
                expect[i] = fixture.V_expect[index];
            }

            puts("expected householder_vector");
            params_print_vector.vector = expect;
            Vector.print_float(params_print_vector);


            //-----------------------------------
            // Print out the results and expected
            //-----------------------------------
            params_print_vector.vector = expect;
            printf("[k=%d] expect: ", k);
            Vector.print_float(params_print_vector);

            Params_Assert_EqualArray_float
            params_assert_equal_array = {
                .input1 = householder_vector,
                .input2 = expect,
                .length = vector_length,
                .tolerance = 1.E-5,
            };

            puts("check validity of the householder_vector");
            Assert.equal_array_float(params_assert_equal_array);
            puts("------------\n");


            //----------------------------
            // test Householder beta
            //----------------------------
            puts("\ncheck householder_beta\n");
            float householder_beta = -9999.;
            Params_GetBeta 
            params_get_beta = new_Params_GetBeta();
            params_get_beta.householder_vector = householder_vector;
            params_get_beta.householder_vector_length = vector_length;
            params_get_beta.result_OUT = &householder_beta;
            fixture.factorizer.get_householder_beta_float(params_get_beta);

            // expected householder beta
            float expect_householder_beta = -99999.;
            Params_Vector_Dot_float
            params_dot = {
                .vector1 = householder_vector,
                .vector2 = householder_vector,
                .length = vector_length,
                .result_OUT = &expect_householder_beta,
            };

            Vector.dot_float(params_dot);

            printf("Householder beta\n");
            printf("answer: %f\n", householder_beta);
            printf("expect: %f\n", expect_householder_beta);

            Params_Assert_Equal_float 
            params_assert_equal = {
                .input1 = householder_beta,
                .input2 = expect_householder_beta,
                .tolerance = 1.E-5,
            };
            Assert.equal_float(params_assert_equal);

            for (int j = k; j < i_ColumnCount; j++)
            {
                printf("\t k=%d, j=%d\n", k, j);

                int row_id_eon = k;
                int i_ColumnId = j;
                
                //------------------------------------------
                // test Householder gamma
                //------------------------------------------
                puts("\ncheck householder_gamma");
                float householder_gamma = -999.;
                Params_GetGamma 
                params_get_gamma = new_Params_GetGamma();
                params_get_gamma.matrix = fixture.A;
                params_get_gamma.matrix_row_count = i_RowCount;
                params_get_gamma.matrix_column_count = i_ColumnCount;
                params_get_gamma.row_id_eon = row_id_eon;
                params_get_gamma.column_id = i_ColumnId;
                params_get_gamma.householder_vector = householder_vector;
                params_get_gamma.householder_vector_length = vector_length;
                params_get_gamma.result_OUT = &householder_gamma;
                fixture.factorizer.get_householder_gamma_float(params_get_gamma);

                // copy the sub-diagonal of column-k to tmp_vector
                float tmp_vector[vector_length];
                for (int i = 0; i < vector_length; i++)
                {   
                    int i_RowId = k + i;
                    int i_ColumnId = j;
                    int index = obj_Id.index(obj_Id, i_RowId, i_ColumnId);
                    tmp_vector[i] = fixture.A[index];
                }

                //--------------------------------------
                // print out the tmp_vector (i.e. from column j)
                //--------------------------------------
                puts("householder_vector=");
                params_print_vector.vector = householder_vector;
                params_print_vector.length = vector_length;
                Vector.print_float(params_print_vector);
                
                puts("copied_vector=");
                params_print_vector.vector = tmp_vector;
                params_print_vector.length = vector_length;
                Vector.print_float(params_print_vector);
                //--------------------------------------

                float expect_householder_gamma = -999.;
                Params_Vector_Dot_float
                params_vector_dot = {
                    .vector1 = householder_vector,
                    .vector2 = tmp_vector,
                    .length  = vector_length,
                    .result_OUT = &expect_householder_gamma,
                };

                Vector.dot_float(params_vector_dot);

                params_assert_equal.input1 = householder_gamma;
                params_assert_equal.input2 = expect_householder_gamma;
                Assert.equal_float(params_assert_equal);
                printf("gamma answer:   %f\n", householder_gamma);
                printf("gamma expected: %f\n", expect_householder_gamma);
                
                // ----------------------------------------
                // test update_matrix_column()
                // ----------------------------------------
                Params_UpdateColumn
                params_update_column = new_Params_UpdateColumn();
                params_update_column.matrix = fixture.A;
                params_update_column.matrix_row_count = i_RowCount;
                params_update_column.matrix_column_count = i_ColumnCount;
                params_update_column.row_id_eon = row_id_eon;
                params_update_column.column_id = i_ColumnId;
                params_update_column.householder_beta = householder_beta;
                params_update_column.householder_gamma = householder_gamma;
                params_update_column.householder_vector = householder_vector;
                params_update_column.householder_vector_length = vector_length;
                fixture.factorizer.update_matrix_column(params_update_column);
            }

            //-------------------------------------
            // check the result of column k in R
            //-------------------------------------
            Params_Assert_EqualMatrix_float
            params_assert_equal_matrix = {
                .matrix1 = fixture.A,
                .matrix1_row_count = i_RowCount,
                .matrix1_column_count = i_ColumnCount,
                .matrix1_row_id_eon = k,
                .matrix1_row_id_end = i_RowCount - 1,
                .matrix1_column_id_eon = k,
                .matrix1_column_id_end = k,
                .matrix2 = fixture.R_expect,
                .matrix2_row_count = i_RowCount,
                .matrix2_column_count = i_ColumnCount,
                .matrix2_row_id_eon = k,
                .matrix2_row_id_end = i_RowCount - 1,
                .matrix2_column_id_eon = k,
                .matrix2_column_id_end = k,
                .tolerance = 1.E-5,
            };
            
            Params_Matrix_Print_float
            params_print_matrix = {
                .matrix = fixture.A,
                .matrix_row_count = i_RowCount,
                .matrix_column_count = i_ColumnCount,
                .precision = 2,
            };

            printf("\nstep k = %d; result: modified A =\n", k);
            Matrix.print_float(params_print_matrix);

            output = Assert.equal_matrix_float(params_assert_equal_matrix);
            puts("==================================");
        }

        return output;
    }

Test module_HouseholderFactorize.factorize function
    static bool
    test_factorize_float(bool bool_RunTest, int omp_thread_count)
    {
        if (!bool_RunTest) { return true; }

        puts("\n test factorize_float()\n");
        
        bool output = true;

        int TEST_CASE_ID = 2;
        printf("TEST CASE: %d\n\n", TEST_CASE_ID);


        //------------------------------------------------------
        //                    OpenMP Info.
        //------------------------------------------------------
        int i_OMP_Volume = omp_thread_count;
        //------------------------------------------------------

        //-----------------------------
        // Import
        //-----------------------------
        module_Matrix
        Matrix = import_Matrix();

        module_Assert
        Assert = import_Assert();

        module_HouseholderFactorize
        HouseholderFactorize = import_HouseholderFactorize();


        //-----------------------------
        // initialize
        //-----------------------------
        //--------------------------------------
        // Create a test fixture
        //--------------------------------------
        //--------------------------------------
        // Allocate common storage of inputs/outputs
        // for all tests
        //--------------------------------------
        int   i_RowCount    = 3;
        int   i_ColumnCount = 3;
        int matrix_length1D = i_RowCount * i_ColumnCount;
        float A[matrix_length1D]; // target matrix
        float V[matrix_length1D]; // Householder vector
        float V_expect[matrix_length1D];
        float R_expect[matrix_length1D];

        // reset all items in V to zero.
        Params_Matrix_Reset_float
        params_matrix_reset = Matrix.new_Params_Reset_float();
        params_matrix_reset.matrix = V;
        params_matrix_reset.matrix_row_count = i_RowCount;
        params_matrix_reset.matrix_column_count = i_ColumnCount;
        params_matrix_reset.value = 0.;
        Matrix.reset_float(params_matrix_reset);
       //--------------------------------------

        Params_Fixture
        fixture = {
            .A             = A,
            .V_expect      = V_expect,
            .R_expect      = R_expect,
            .matrix_row_count    = i_RowCount,
            .matrix_column_count = i_ColumnCount,
            .factorizer    = HouseholderFactorize,
            .factorize     = HouseholderFactorize.factorize_float,
        };

        // case1_3x3_matrix(fixture);
        switch (TEST_CASE_ID) 
        {
            case 1: case1_3x3_matrix(fixture); break;
            case 2: case2_3x3_matrix(fixture); break;
            default: printf("Illegal TEST_CASE_ID\n"); break;
        }


        //-----------------------------
        // do Householder factorization
        //-----------------------------

        Params_HouseholderFactorize_float
        params_factorizer = 
        HouseholderFactorize.new_Params_HouseholderFactorize_float();
        params_factorizer.matrix = fixture.A;
        params_factorizer.V_OUT  = V;
        params_factorizer.matrix_row_count = i_RowCount;
        params_factorizer.matrix_column_count = i_ColumnCount;
        params_factorizer.omp_thread_count = i_OMP_Volume;

        fixture.factorize(params_factorizer);

        //-----------------------------
        // show results
        //-----------------------------
        Params_Matrix_Print_float
        params_print_matrix = {
            .matrix = fixture.A,
            .matrix_row_count = i_RowCount,
            .matrix_column_count = i_ColumnCount,
            .precision = 3,
        };

        // -----------------------------
        // check results R
        // -----------------------------
        printf("results: R (modified A) = \n");
        params_print_matrix.matrix = fixture.A;
        Matrix.print_float(params_print_matrix);

        printf("expected: R = \n");
        params_print_matrix.matrix = fixture.R_expect;
        Matrix.print_float(params_print_matrix);

        Params_Assert_EqualMatrix_float
            params_assert_equal_matrix = {
                .matrix1 = fixture.A,
                .matrix1_row_count = i_RowCount,
                .matrix1_column_count = i_ColumnCount,
                .matrix1_row_id_eon = 0,
                .matrix1_row_id_end = i_RowCount - 1,
                .matrix1_column_id_eon = 0,
                .matrix1_column_id_end = i_ColumnCount - 1,
                .matrix2 = fixture.R_expect,
                .matrix2_row_count = i_RowCount,
                .matrix2_column_count = i_ColumnCount,
                .matrix2_row_id_eon = 0,
                .matrix2_row_id_end = i_RowCount - 1,
                .matrix2_column_id_eon = 0,
                .matrix2_column_id_end = i_ColumnCount - 1,
                .tolerance = 1.E-5,
            };

        Assert.equal_matrix_float(params_assert_equal_matrix);

        // -----------------------------------------------
        // check results Householder vectors stored in V
        // -----------------------------------------------
        puts("-------------------");
        printf("results: V (Householder vectors) = \n");
        params_print_matrix.matrix = V;
        Matrix.print_float(params_print_matrix);

        printf("expected: V = \n");
        params_print_matrix.matrix = fixture.V_expect;
        Matrix.print_float(params_print_matrix);

        params_assert_equal_matrix.matrix1 = V;
        params_assert_equal_matrix.matrix2 = fixture.V_expect;
        
        output = Assert.equal_matrix_float(params_assert_equal_matrix);
        return output;
    }

Test module_HouseholderFactorize.factorize function
    static bool
    test_factorize_float_Time(
        bool bool_RunTest, 
        int matrix_row_count,
        int omp_thread_count)
    {
        if (!bool_RunTest) { return true; }

        puts("\n test (serial) factorize_float_Time()\n");
        
        bool output = true;

        int TEST_CASE_ID = 1;
        printf("TEST CASE: %d\n\n", TEST_CASE_ID);

        //------------------------------------------------------
        //                    OpenMP Info.
        //------------------------------------------------------
        int i_OMP_Volume = omp_thread_count;
        //------------------------------------------------------


        //-----------------------------
        // Import
        //-----------------------------
        module_Matrix
        Matrix = import_Matrix();

        module_HouseholderFactorize
        HouseholderFactorize = import_HouseholderFactorize();


        //-----------------------------
        // initialize
        //-----------------------------
        //--------------------------------------
        // Create a test fixture
        //--------------------------------------
        //--------------------------------------
        // Allocate common storage of inputs/outputs
        // for all tests
        //--------------------------------------
        int   i_RowCount    = matrix_row_count;
        int   i_ColumnCount = i_RowCount;
        int matrix_length1D = i_RowCount * i_ColumnCount;
        
        //-----------------------------------------------
        // Get memory for A, V, V_expect, and R_expect
        //-----------------------------------------------
        float *A = calloc(matrix_length1D, sizeof(float)); // target matrix
        if ( A == NULL ) { puts("### cannot get heap for A"); exit(1); }
        
        float *V = calloc(matrix_length1D, sizeof(float)); // output householder vector matrix
        if ( V == NULL ) { puts("### cannot get heap for V"); exit(1); }

        float *V_expect = calloc(matrix_length1D, sizeof(float));
        if ( V_expect == NULL ) { puts("### cannot get heap for V_expect"); exit(1); }
        
        float *R_expect = calloc(matrix_length1D, sizeof(float));
        if ( R_expect == NULL ) { puts("### cannot get heap for R_expect"); exit(1); }
        //-----------------------------------------------

        // reset all items in V to zero.
        Params_Matrix_Reset_float
        params_matrix_reset = Matrix.new_Params_Reset_float();
        params_matrix_reset.matrix = V;
        params_matrix_reset.matrix_row_count = i_RowCount;
        params_matrix_reset.matrix_column_count = i_ColumnCount;
        params_matrix_reset.value = 0.;
        Matrix.reset_float(params_matrix_reset);
       //--------------------------------------

        Params_Fixture
        fixture = {
            .A             = A,
            .V_expect      = V_expect,
            .R_expect      = R_expect,
            .matrix_row_count    = i_RowCount,
            .matrix_column_count = i_ColumnCount,
            .factorizer    = HouseholderFactorize,
            .factorize     = HouseholderFactorize.factorize_float,
        };

        // case1_3x3_matrix(fixture);
        switch (TEST_CASE_ID) 
        {
            case 1: case1_NxN_matrix(fixture); break;
            default: printf("Illegal TEST_CASE_ID\n"); break;
        }

        //-----------------------------
        // do Householder factorization
        //-----------------------------

        Params_HouseholderFactorize_float
        params_factorizer = 
        HouseholderFactorize.new_Params_HouseholderFactorize_float();
        params_factorizer.matrix = fixture.A;
        params_factorizer.V_OUT  = V;
        params_factorizer.matrix_row_count = i_RowCount;
        params_factorizer.matrix_column_count = i_ColumnCount;
        params_factorizer.omp_thread_count = i_OMP_Volume;

        clock_t clock_Eon = clock();
        fixture.factorize(params_factorizer);
        clock_t clock_End = clock();
        int i_TimeCostOfMe_us = microsecond(clock_End - clock_Eon);
        //#############################
        puts("factorize ok!");
        //#############################
        printf("serial: time cost %d us\n", i_TimeCostOfMe_us);
        int i_TotalTimeCost_us = i_TimeCostOfMe_us;

        // -----------------------------
        // write outputs
        // -----------------------------
        printf("time cost: %d us\n", i_TotalTimeCost_us);

        char file_name[100];

        //--------------------------------------------
        // write time cost to output
        sprintf(file_name, "time_cost_sum_microsecond_%dx%d_serial_omp_%d.dat",
           i_RowCount, i_ColumnCount, i_OMP_Volume);
        FILE *p_File = fopen(file_name, "w");
        fprintf(p_File, "1 %d %d\n", i_TotalTimeCost_us, i_TotalTimeCost_us);
        fclose(p_File);
        //--------------------------------------------

        //--------------------------------------------
        // write R (modified A) to output
        //--------------------------------------------
        sprintf(file_name, "answer_R_%dx%d_serial_omp_%d.dat",
            i_RowCount, i_ColumnCount, i_OMP_Volume);
        Params_Matrix_WriteTextFile_float
        params_write_matrix = Matrix.new_Params_WriteTextFile_float();
        params_write_matrix.matrix = fixture.A;
        params_write_matrix.matrix_row_count = i_RowCount;
        params_write_matrix.matrix_column_count = i_ColumnCount;
        params_write_matrix.precision = 6;
        params_write_matrix.scientific = false;
        params_write_matrix.file_name = file_name;
        Matrix.write_text_file_float(params_write_matrix);
        //--------------------------------------------

        //--------------------------------------------
        // write V (Householder vectors) to output
        //--------------------------------------------
        sprintf(file_name, "answer_V_%dx%d_serial_omp_%d.dat",
            i_RowCount, i_ColumnCount, i_OMP_Volume);
        params_write_matrix.matrix = V;
        params_write_matrix.matrix_row_count = i_RowCount;
        params_write_matrix.matrix_column_count = i_ColumnCount;
        params_write_matrix.precision = 6;
        params_write_matrix.scientific = false;
        params_write_matrix.file_name = file_name;
        Matrix.write_text_file_float(params_write_matrix);
        //--------------------------------------------
        
        free(A);
        free(V);
        free(R_expect);
        free(V_expect);
        return output;
    }

Test module_HouseholderFactorize.factorize_mpi_1D_float function
    static bool
    test_factorize_mpi_1D_float(
        bool bool_RunTest, 
        MPI_Comm mpi_communicator,
        int omp_thread_count, 
        int mpi_task_mapping_style)
    {
        if (!bool_RunTest) { return true; }

        int TEST_CASE_ID = 2;

        //------------------------------------------------------
        //                    OpenMP Info.
        //------------------------------------------------------
        int i_OMP_Volume = omp_thread_count;
        //------------------------------------------------------

        //------------------------------------------------------
        //                     MPI INFO
        //------------------------------------------------------
        int i_MPI_ThisRank = -1; // MPI rank of this process
        int i_MPI_Volume   = -1; // MPI process count

        MPI_Comm_rank(mpi_communicator, &i_MPI_ThisRank);
        MPI_Comm_size(mpi_communicator, &i_MPI_Volume);
        //------------------------------------------------------

        if (i_MPI_ThisRank == 0) 
        {
            puts("\n test factorize_mpi_1D_float()\n");
            printf("TEST CASE: %d\n\n", TEST_CASE_ID);
        }
        

        //-----------------------------
        // Import
        //-----------------------------
        module_Matrix
        Matrix = import_Matrix();

        module_Assert
        Assert = import_Assert();

        module_HouseholderFactorize
        HouseholderFactorize = import_HouseholderFactorize();


        //-----------------------------
        // initialize
        //-----------------------------
        //--------------------------------------
        // Create a test fixture
        //--------------------------------------
        //--------------------------------------
        // Allocate common storage of inputs/outputs
        // for all tests
        //--------------------------------------
        int   i_RowCount    = 3;
        int   i_ColumnCount = 3;
        int matrix_length1D = i_RowCount * i_ColumnCount;
        float A[matrix_length1D]; // target matrix
        float V[matrix_length1D]; // Householder vector
        float V_expect[matrix_length1D];
        float R_expect[matrix_length1D];

        // reset all items in V to zero.
        Params_Matrix_Reset_float
        params_matrix_reset = Matrix.new_Params_Reset_float();
        params_matrix_reset.matrix = V;
        params_matrix_reset.matrix_row_count = i_RowCount;
        params_matrix_reset.matrix_column_count = i_ColumnCount;
        params_matrix_reset.value = 0.;
        Matrix.reset_float(params_matrix_reset);
       //--------------------------------------

        Params_Fixture
        fixture = {
            .A             = A,
            .V_expect      = V_expect,
            .R_expect      = R_expect,
            .matrix_row_count    = i_RowCount,
            .matrix_column_count = i_ColumnCount,
            .factorizer    = HouseholderFactorize,
            .factorize     = HouseholderFactorize.factorize_mpi_1D_float,
        };

        // case1_3x3_matrix(fixture);
        switch (TEST_CASE_ID) 
        {
            case 1: case1_3x3_matrix(fixture); break;
            case 2: case2_3x3_matrix(fixture); break;
            default: printf("Illegal TEST_CASE_ID\n"); break;
        }


        //-----------------------------
        // do Householder factorization
        //-----------------------------

        Params_HouseholderFactorize_float
        params_factorizer = 
        HouseholderFactorize.new_Params_HouseholderFactorize_float();
        params_factorizer.matrix = fixture.A;
        params_factorizer.V_OUT  = V;
        params_factorizer.matrix_row_count = i_RowCount;
        params_factorizer.matrix_column_count = i_ColumnCount;
        params_factorizer.parallel_level = 1;
        params_factorizer.mpi_communicator = mpi_communicator;
        params_factorizer.omp_thread_count = i_OMP_Volume;
        params_factorizer.mpi_task_mapping_style = mpi_task_mapping_style;

        fixture.factorize(params_factorizer);

        //-----------------------------
        // show results
        //-----------------------------
        Params_Matrix_Print_float
        params_print_matrix = {
            .matrix = NULL,
            .matrix_row_count = i_RowCount,
            .matrix_column_count = i_ColumnCount,
            .precision = 3,
        };

        // -----------------------------
        // check results R
        // -----------------------------
        if (i_MPI_ThisRank == 0)
        {
            printf("results: R (modified A) = \n");
            params_print_matrix.matrix = fixture.A;
            Matrix.print_float(params_print_matrix);

            printf("expected: R = \n");
            params_print_matrix.matrix = fixture.R_expect;
            Matrix.print_float(params_print_matrix);

            Params_Assert_EqualMatrix_float
                params_assert_equal_matrix = {
                    .matrix1 = fixture.A,
                    .matrix1_row_count = i_RowCount,
                    .matrix1_column_count = i_ColumnCount,
                    .matrix1_row_id_eon = 0,
                    .matrix1_row_id_end = i_RowCount - 1,
                    .matrix1_column_id_eon = 0,
                    .matrix1_column_id_end = i_ColumnCount - 1,
                    .matrix2 = fixture.R_expect,
                    .matrix2_row_count = i_RowCount,
                    .matrix2_column_count = i_ColumnCount,
                    .matrix2_row_id_eon = 0,
                    .matrix2_row_id_end = i_RowCount - 1,
                    .matrix2_column_id_eon = 0,
                    .matrix2_column_id_end = i_ColumnCount - 1,
                    .tolerance = 1.E-5,
                };

            Assert.equal_matrix_float(params_assert_equal_matrix);

            // -----------------------------------------------
            // check results Householder vectors stored in V
            // -----------------------------------------------
            puts("-------------------");
            printf("results: V (Householder vectors) = \n");
            params_print_matrix.matrix = V;
            Matrix.print_float(params_print_matrix);

            printf("expected: V = \n");
            params_print_matrix.matrix = fixture.V_expect;
            Matrix.print_float(params_print_matrix);

            params_assert_equal_matrix.matrix1 = V;
            params_assert_equal_matrix.matrix2 = fixture.V_expect;
            Assert.equal_matrix_float(params_assert_equal_matrix);
        }

        return true;
    }


Test module_HouseholderFactorize.factorize_mpi_1D_float function
    static bool
    test_factorize_mpi_1D_float_L(
        bool bool_RunTest, 
        MPI_Comm mpi_communicator,
        int omp_thread_count,
        int mpi_task_mapping_style)
    {
        if (!bool_RunTest) { return true; }

        int TEST_CASE_ID = 1;

        //------------------------------------------------------
        //                    OpenMP Info.
        //------------------------------------------------------
        int i_OMP_Volume = omp_thread_count;
        //------------------------------------------------------

        //------------------------------------------------------
        //                     MPI INFO
        //------------------------------------------------------
        int i_MPI_ThisRank = -1; // MPI rank of this process
        int i_MPI_Volume   = -1; // MPI process count

        MPI_Comm_rank(mpi_communicator, &i_MPI_ThisRank);
        MPI_Comm_size(mpi_communicator, &i_MPI_Volume);
        //------------------------------------------------------

        if (i_MPI_ThisRank == 0) 
        {
            puts("\n test factorize_mpi_1D_float_L()\n");
            printf("TEST CASE: %d\n\n", TEST_CASE_ID);
        }
        

        //-----------------------------
        // Import
        //-----------------------------
        module_Matrix
        Matrix = import_Matrix();

        module_Assert
        Assert = import_Assert();

        module_HouseholderFactorize
        HouseholderFactorize = import_HouseholderFactorize();


        //-----------------------------
        // initialize
        //-----------------------------
        //--------------------------------------
        // Create a test fixture
        //--------------------------------------
        //--------------------------------------
        // Allocate common storage of inputs/outputs
        // for all tests
        //--------------------------------------
        int   i_RowCount    = 4;
        int   i_ColumnCount = 4;
        int matrix_length1D = i_RowCount * i_ColumnCount;
        float A[matrix_length1D]; // target matrix
        float V[matrix_length1D]; // Householder vector
        float V_expect[matrix_length1D];
        float R_expect[matrix_length1D];

        // reset all items in V to zero.
        Params_Matrix_Reset_float
        params_matrix_reset = Matrix.new_Params_Reset_float();
        params_matrix_reset.matrix = V;
        params_matrix_reset.matrix_row_count = i_RowCount;
        params_matrix_reset.matrix_column_count = i_ColumnCount;
        params_matrix_reset.value = 0.;
        Matrix.reset_float(params_matrix_reset);
       //--------------------------------------

        Params_Fixture
        fixture = {
            .A             = A,
            .V_expect      = V_expect,
            .R_expect      = R_expect,
            .matrix_row_count    = i_RowCount,
            .matrix_column_count = i_ColumnCount,
            .factorizer    = HouseholderFactorize,
            .factorize     = HouseholderFactorize.factorize_mpi_1D_float,
        };

        // case1_3x3_matrix(fixture);
        switch (TEST_CASE_ID) 
        {
            case 1: case3_4x4_matrix(fixture); break;
            default: printf("Illegal TEST_CASE_ID\n"); break;
        }


        //-----------------------------
        // do Householder factorization
        //-----------------------------

        Params_HouseholderFactorize_float
        params_factorizer = 
        HouseholderFactorize.new_Params_HouseholderFactorize_float();
        params_factorizer.matrix = fixture.A;
        params_factorizer.V_OUT  = V;
        params_factorizer.matrix_row_count = i_RowCount;
        params_factorizer.matrix_column_count = i_ColumnCount;
        params_factorizer.parallel_level = 1;
        params_factorizer.mpi_communicator = mpi_communicator;
        params_factorizer.omp_thread_count = i_OMP_Volume;
        params_factorizer.mpi_task_mapping_style = mpi_task_mapping_style;

        fixture.factorize(params_factorizer);

        //-----------------------------
        // show results
        //-----------------------------
        Params_Matrix_Print_float
        params_print_matrix = {
            .matrix = NULL,
            .matrix_row_count = i_RowCount,
            .matrix_column_count = i_ColumnCount,
            .precision = 3,
        };

        // -----------------------------
        // check results R
        // -----------------------------
        if (i_MPI_ThisRank == 0)
        {
            printf("results: R (modified A) = \n");
            params_print_matrix.matrix = fixture.A;
            Matrix.print_float(params_print_matrix);

            printf("expected: R = \n");
            params_print_matrix.matrix = fixture.R_expect;
            Matrix.print_float(params_print_matrix);

            Params_Assert_EqualMatrix_float
                params_assert_equal_matrix = {
                    .matrix1 = fixture.A,
                    .matrix1_row_count = i_RowCount,
                    .matrix1_column_count = i_ColumnCount,
                    .matrix1_row_id_eon = 0,
                    .matrix1_row_id_end = i_RowCount - 1,
                    .matrix1_column_id_eon = 0,
                    .matrix1_column_id_end = i_ColumnCount - 1,
                    .matrix2 = fixture.R_expect,
                    .matrix2_row_count = i_RowCount,
                    .matrix2_column_count = i_ColumnCount,
                    .matrix2_row_id_eon = 0,
                    .matrix2_row_id_end = i_RowCount - 1,
                    .matrix2_column_id_eon = 0,
                    .matrix2_column_id_end = i_ColumnCount - 1,
                    .tolerance = 1.E-5,
                };

            Assert.equal_matrix_float(params_assert_equal_matrix);

            // -----------------------------------------------
            // check results Householder vectors stored in V
            // -----------------------------------------------
            puts("-------------------");
            printf("results: V (Householder vectors) = \n");
            params_print_matrix.matrix = V;
            Matrix.print_float(params_print_matrix);

            printf("expected: V = \n");
            params_print_matrix.matrix = fixture.V_expect;
            Matrix.print_float(params_print_matrix);

            params_assert_equal_matrix.matrix1 = V;
            params_assert_equal_matrix.matrix2 = fixture.V_expect;
            Assert.equal_matrix_float(params_assert_equal_matrix);
        }

        return true;
    }

Test module_HouseholderFactorize.factorize_mpi_1D_float function
    static bool
    test_factorize_mpi_1D_float_Time(
        bool bool_RunTest, 
        MPI_Comm mpi_communicator,
        int matrix_row_count,
        int omp_thread_count,
        int mpi_task_mapping_style)
    {
        // Time the execution time
        if (!bool_RunTest) { return true; }

        int TEST_CASE_ID = 1;


        //------------------------------------------------------
        //                    OpenMP Info.
        //------------------------------------------------------
        int i_OMP_Volume = omp_thread_count;
        //------------------------------------------------------

        //------------------------------------------------------
        //                     MPI INFO
        //------------------------------------------------------
        int i_MPI_ThisRank = -1; // MPI rank of this process
        int i_MPI_Volume   = -1; // MPI process count

        MPI_Comm_rank(mpi_communicator, &i_MPI_ThisRank);
        MPI_Comm_size(mpi_communicator, &i_MPI_Volume);
        //------------------------------------------------------


        /********************************************************/
        /** Switch to serial mode if using mpirun -np 1 **/
        /********************************************************/
        if ( i_MPI_Volume == 1 )
        {
            test_factorize_float_Time(true, matrix_row_count, omp_thread_count);
            return true;
        }
        /********************************************************/

        if (i_MPI_ThisRank == 0) 
        {
            printf("\n test factorize_mpi_1D_float_Time() np %d omp %d\n",
                i_MPI_Volume, i_OMP_Volume);
            printf("TEST CASE: %d\n\n", TEST_CASE_ID);
        }
        

        //-----------------------------
        // Import
        //-----------------------------
        module_Matrix
        Matrix = import_Matrix();


        module_HouseholderFactorize
        HouseholderFactorize = import_HouseholderFactorize();

        //------------------------------------------
        //-----------------------------
        // initialize
        //-----------------------------
        //--------------------------------------
        // Create a test fixture
        //--------------------------------------
        //--------------------------------------
        // Allocate common storage of inputs/outputs
        // for all tests
        //--------------------------------------
        int   i_RowCount    = matrix_row_count;
        int   i_ColumnCount = i_RowCount;
        int matrix_length1D = i_RowCount * i_ColumnCount;
       
        //-----------------------------------------------
        // Get memory for A, V, V_expect, and R_expect
        //-----------------------------------------------
        float *A = calloc(matrix_length1D, sizeof(float)); // target matrix
        if ( A == NULL ) { puts("### cannot get heap for A"); exit(1); }
        
        float *V = calloc(matrix_length1D, sizeof(float)); // output householder vector matrix
        if ( V == NULL ) { puts("### cannot get heap for V"); exit(1); }

        float *V_expect = calloc(matrix_length1D, sizeof(float));
        if ( V_expect == NULL ) { puts("### cannot get heap for V_expect"); exit(1); }
        
        float *R_expect = calloc(matrix_length1D, sizeof(float));
        if ( R_expect == NULL ) { puts("### cannot get heap for R_expect"); exit(1); }
        //-----------------------------------------------

        // reset all items in V to zero.
        Params_Matrix_Reset_float
        params_matrix_reset = Matrix.new_Params_Reset_float();
        params_matrix_reset.matrix = V;
        params_matrix_reset.matrix_row_count = i_RowCount;
        params_matrix_reset.matrix_column_count = i_ColumnCount;
        params_matrix_reset.value = 0.;
        Matrix.reset_float(params_matrix_reset);
       //--------------------------------------

        Params_Fixture
        fixture = {
            .A             = A,
            .V_expect      = V_expect,
            .R_expect      = R_expect,
            .matrix_row_count    = i_RowCount,
            .matrix_column_count = i_ColumnCount,
            .factorizer    = HouseholderFactorize,
            .factorize     = HouseholderFactorize.factorize_mpi_1D_float,
        };

        // case1_3x3_matrix(fixture);
        switch (TEST_CASE_ID) 
        {
            case 1: case1_NxN_matrix(fixture); break;
            default: printf("Illegal TEST_CASE_ID\n"); break;
        }


        //-----------------------------
        // do Householder factorization
        //-----------------------------

        Params_HouseholderFactorize_float
        params_factorizer = 
        HouseholderFactorize.new_Params_HouseholderFactorize_float();
        params_factorizer.matrix = fixture.A;
        params_factorizer.V_OUT  = V;
        params_factorizer.matrix_row_count = i_RowCount;
        params_factorizer.matrix_column_count = i_ColumnCount;
        params_factorizer.parallel_level = 1;
        params_factorizer.mpi_communicator = mpi_communicator;
        params_factorizer.omp_thread_count = i_OMP_Volume;
        params_factorizer.mpi_task_mapping_style = mpi_task_mapping_style;

        clock_t clock_Eon = clock();
        fixture.factorize(params_factorizer);
        clock_t clock_End = clock();
        int i_TimeCostOfMe_us = microsecond(clock_End - clock_Eon);

        printf("P[%d]: time cost %d us\n", 
            i_MPI_ThisRank, i_TimeCostOfMe_us);
        int i_TotalTimeCost_us = 0;

        MPI_Reduce(
            &i_TimeCostOfMe_us,
            &i_TotalTimeCost_us,
            1,
            MPI_INT,
            MPI_SUM,
            0,
            mpi_communicator);

        // -----------------------------
        // check results R
        // -----------------------------
        if (i_MPI_ThisRank == 0)
        {
            printf("sum. time cost: %d us \n", i_TotalTimeCost_us);
            printf("avg. time cost: %.1f us \n", i_TotalTimeCost_us / ((float) i_MPI_Volume));

            char file_name[100];

            //--------------------------------------------
            // write time cost to output
            sprintf(file_name, "time_cost_sum_microsecond_%dx%d_mpi_%d_omp_%d_map_%d.dat",
               i_RowCount, i_ColumnCount, i_MPI_Volume, i_OMP_Volume, mpi_task_mapping_style);
            FILE *p_File = fopen(file_name, "w");
            fprintf(p_File, "%d %d %.1f\n", 
                i_MPI_Volume, 
                i_TotalTimeCost_us, 
                i_TotalTimeCost_us/((float) i_MPI_Volume));
            fclose(p_File);
            //--------------------------------------------

            //--------------------------------------------
            // write R (modified A) to output
            //--------------------------------------------
            sprintf(file_name, "answer_R_%dx%d_mpi_%d_omp_%d_map_%d.dat",
                i_RowCount, i_ColumnCount, i_MPI_Volume, i_OMP_Volume, mpi_task_mapping_style);
            Params_Matrix_WriteTextFile_float
            params_write_matrix = Matrix.new_Params_WriteTextFile_float();
            params_write_matrix.matrix = fixture.A;
            params_write_matrix.matrix_row_count = i_RowCount;
            params_write_matrix.matrix_column_count = i_ColumnCount;
            params_write_matrix.precision = 6;
            params_write_matrix.scientific = false;
            params_write_matrix.file_name = file_name;
            Matrix.write_text_file_float(params_write_matrix);
            //--------------------------------------------

            //--------------------------------------------
            // write V (Householder vectors) to output
            //--------------------------------------------
            sprintf(file_name, "answer_V_%dx%d_mpi_%d_omp_%d_map_%d.dat",
                i_RowCount, i_ColumnCount, i_MPI_Volume, i_OMP_Volume, mpi_task_mapping_style);
            params_write_matrix.matrix = V;
            params_write_matrix.matrix_row_count = i_RowCount;
            params_write_matrix.matrix_column_count = i_ColumnCount;
            params_write_matrix.precision = 6;
            params_write_matrix.scientific = false;
            params_write_matrix.file_name = file_name;
            Matrix.write_text_file_float(params_write_matrix);
            //--------------------------------------------
        }

        free(A);
        free(V);
        free(R_expect);
        free(V_expect);
        return true;
    }