# LICENSE
    /**********************************************************
     Copyright (c) 2015 YUHANG WANG 
     ALL RIGHTS RESERVED

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
    ************************************************************/



# LICENSE
    /**********************************************************
     Copyright (c) 2015 YUHANG WANG 
     ALL RIGHTS RESERVED

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
    ************************************************************/



# This is the QR factorization method using Householder transformation

#[module_HouseholderFactorize.dep.h](#module_HouseholderFactorize.dep.h "save:")
    
    _"LICENSE"

    #include "module_Matrix.h"
    #include <stdbool.h>
    #include "mpi.h"
    
    
#[module_HouseholderFactorize.h](#module_HouseholderFactorize.h "save:")
    
    _"LICENSE"

    #include "module_HouseholderFactorize.dep.h"

    typedef struct
    Params_HouseholderFactorize_float
    {
        int    omp_thread_count;
        float *matrix;
        int    matrix_row_count;
        int    matrix_column_count;
        float *V_OUT;
        int    parallel_level;
        MPI_Comm mpi_communicator;
        int    mpi_task_mapping_style;
    }
    Params_HouseholderFactorize_float;

    //=========================================
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~ temporary ~~~~~~~~~~~~~~~~~~~
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    //-------------------------------------------------
    // parameter type for get_householder_vector_float
    //-------------------------------------------------
    typedef struct 
    Params_HouseholderFactorize_GetHouseholderVector_float
    {
        float *matrix;
        int    matrix_row_count;
        int    matrix_column_count;
        int    current_column_id;
        float *result_OUT;
    }
    Params_HouseholderFactorize_GetHouseholderVector_float;
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    //-------------------------------------------------
    // parameter type for get_householder_beta_float
    //-------------------------------------------------
    typedef struct 
    Params_HouseholderFactorize_GetHouseholderBeta_float
    {
        float *householder_vector;
        int    householder_vector_length;
        float *result_OUT;
    }
    Params_HouseholderFactorize_GetHouseholderBeta_float;
    //-------------------------------------------------

    //-------------------------------------------------
    // parameter type for get_householder_gamma_float
    //-------------------------------------------------
    typedef struct 
    Params_HouseholderFactorize_GetHouseholderGamma_float
    {
        float *matrix; 
        int    matrix_row_count;
        int    matrix_column_count;
        int    row_id_eon;
        int    column_id;
        float *householder_vector;
        int    householder_vector_length;
        float *result_OUT;
    }
    Params_HouseholderFactorize_GetHouseholderGamma_float;
    //-------------------------------------------------
  

    //-------------------------------------------------
    // parameter type for get_householder_gamma_float
    //-------------------------------------------------
    typedef struct 
    Params_HouseholderFactorize_UpdateMatrixColumn_float
    {
        float *matrix; 
        int    matrix_row_count;
        int    matrix_column_count;
        int    row_id_eon;
        int    column_id;
        float  householder_beta;
        float  householder_gamma;
        float *householder_vector;
        int    householder_vector_length;
    }
    Params_HouseholderFactorize_UpdateMatrixColumn_float;


    //-------------------------------------------------
    // parameter type for get_householder_gamma_float
    //-------------------------------------------------
    typedef struct 
    Params_HouseholderFactorize_OMP_UpdateMatrixColumn_float
    {
        int    omp_thread_count;
        float *matrix; 
        int    matrix_row_count;
        int    matrix_column_count;
        int    row_id_eon;
        int   *column_ids;
        int    column_ids_length;
        float  householder_beta;
        float *householder_vector;
        int    householder_vector_length;
        int    rank;
    }
    Params_HouseholderFactorize_OMP_UpdateMatrixColumn_float;
    //-------------------------------------------------

    typedef struct 
    module_HouseholderFactorize 
    {
        void (*hi)(void);
        void (*factorize_float)(Params_HouseholderFactorize_float);
        void (*factorize_mpi_1D_float)(Params_HouseholderFactorize_float);
        void (*get_householder_vector_float)(Params_HouseholderFactorize_GetHouseholderVector_float);
        void (*get_householder_beta_float)(Params_HouseholderFactorize_GetHouseholderBeta_float);
        void (*get_householder_gamma_float)(Params_HouseholderFactorize_GetHouseholderGamma_float);
        void (*update_matrix_column)(Params_HouseholderFactorize_UpdateMatrixColumn_float);
        void (*omp_update_matrix_column)(Params_HouseholderFactorize_OMP_UpdateMatrixColumn_float);
        
        Params_HouseholderFactorize_float
        (*new_Params_HouseholderFactorize_float)(void);
       
        Params_HouseholderFactorize_GetHouseholderVector_float 
        (*new_Params_GetHouseholderVector_float)(void);
        
        Params_HouseholderFactorize_GetHouseholderBeta_float
        (*new_Params_GetHouseholderBeta_float)(void);
        
        Params_HouseholderFactorize_GetHouseholderGamma_float
        (*new_Params_GetHouseholderGamma_float)(void);
        
        Params_HouseholderFactorize_UpdateMatrixColumn_float
        (*new_Params_UpdateMatrixColumn_float)(void);     

        Params_HouseholderFactorize_OMP_UpdateMatrixColumn_float
        (*new_Params_OMP_UpdateMatrixColumn_float)(void);
    }
    module_HouseholderFactorize;

    module_HouseholderFactorize 
    import_HouseholderFactorize(void);



#[module_HouseholderFactorize.dep.c](#module_HouseholderFactorize.dep.c "save:")
    
    _"LICENSE"

    #include "module_HouseholderFactorize.h"
    #include "module_Matrix.h"
    #include "module_Vector.h"
    #include "module_MathSign.h"
    #include "module_MpiJob.h"
    #include <stdio.h>
    #include <stdlib.h>
    #include <math.h>
    #include <assert.h>
    #include "mpi.h"
    #include <omp.h>


#[module_HouseholderFactorize.c](#module_HouseholderFactorize.c "save:")
   
    _"LICENSE"

    #include "module_HouseholderFactorize.dep.c"

    _"module_HouseholderFactorize worker functions"

    _"module_HouseholderFactorize front-end functions"

    module_HouseholderFactorize 
    import_HouseholderFactorize(void)
    {
        module_HouseholderFactorize 
        obj = {
            .hi = hi,
            .factorize_float = factorize_float,
            .factorize_mpi_1D_float = factorize_mpi_1D_float,
            .get_householder_vector_float = get_householder_vector_float,
            .get_householder_beta_float = get_householder_beta_float,
            .get_householder_gamma_float = get_householder_gamma_float,
            .update_matrix_column = update_matrix_column,
            .omp_update_matrix_column = omp_update_matrix_column,
            .new_Params_HouseholderFactorize_float = new_Params_HouseholderFactorize_float,
            .new_Params_GetHouseholderVector_float = new_Params_GetHouseholderVector_float,
            .new_Params_GetHouseholderBeta_float = new_Params_GetHouseholderBeta_float,
            .new_Params_GetHouseholderGamma_float = new_Params_GetHouseholderGamma_float,
            .new_Params_UpdateMatrixColumn_float = new_Params_UpdateMatrixColumn_float,
            .new_Params_OMP_UpdateMatrixColumn_float = new_Params_OMP_UpdateMatrixColumn_float,
        };

        return obj;
    }


## module_HouseholderFactorize front-end functions

Hello world function.

    static void
    hi(void)
    {
        puts("Greetings from module_HouseholderFactorize!\n");
    }


Initializer for Params_HouseholderFactorize_float
    static Params_HouseholderFactorize_float
    new_Params_HouseholderFactorize_float(void)
    {
        Params_HouseholderFactorize_float
        obj = {
            .omp_thread_count = 1,
            .matrix = NULL,
            .V_OUT  = NULL,
            .matrix_row_count = -1,
            .matrix_column_count = -1,
            .parallel_level = 0,
            .mpi_communicator = MPI_COMM_NULL,
            .mpi_task_mapping_style = 0,
        };
        return obj;
    }


Initializer for Params_HouseholderFactorize_GetHouseholderVector_float
    static Params_HouseholderFactorize_GetHouseholderVector_float
    new_Params_GetHouseholderVector_float(void)
    {
        Params_HouseholderFactorize_GetHouseholderVector_float
        obj = {
            .matrix = NULL,
            .current_column_id = -1,
            .matrix_row_count = -1,
            .matrix_column_count = -1,
            .result_OUT = NULL,
        };

        return obj;
    }

Initializer for Params_GetHouseholderBeta_float
    static Params_HouseholderFactorize_GetHouseholderBeta_float
    new_Params_GetHouseholderBeta_float(void)
    {
        Params_HouseholderFactorize_GetHouseholderBeta_float
        obj ={
            .householder_vector = NULL,
            .householder_vector_length = -1,
            .result_OUT = NULL,
        };
        return obj;
    }

Initializer for Params_GetHouseholderGamma_float
    static Params_HouseholderFactorize_GetHouseholderGamma_float
    new_Params_GetHouseholderGamma_float(void)
    {
        Params_HouseholderFactorize_GetHouseholderGamma_float
        obj ={
            .matrix = NULL,
            .matrix_row_count = -1,
            .matrix_column_count = -1,
            .row_id_eon = -1,
            .column_id = -1,
            .householder_vector = NULL,
            .householder_vector_length = -1,
            .result_OUT = NULL,
        };
        return obj;
    }

Initializer for Params_UpdateMatrixColumn_float
    static Params_HouseholderFactorize_UpdateMatrixColumn_float
    new_Params_UpdateMatrixColumn_float(void)
    {
        Params_HouseholderFactorize_UpdateMatrixColumn_float
        obj ={
            .matrix = NULL,
            .matrix_row_count = -1,
            .matrix_column_count = -1,
            .row_id_eon = -1,
            .column_id = -1,
            .householder_beta = -999.,
            .householder_gamma = -999.,
            .householder_vector = NULL,
            .householder_vector_length = -1,
        };
        return obj;
    }


Initializer for Params_OMP_UpdateMatrixColumn_float
    static Params_HouseholderFactorize_OMP_UpdateMatrixColumn_float
    new_Params_OMP_UpdateMatrixColumn_float(void)
    {
        Params_HouseholderFactorize_OMP_UpdateMatrixColumn_float
        obj ={
            .omp_thread_count = 1,
            .matrix = NULL,
            .matrix_row_count = -1,
            .matrix_column_count = -1,
            .row_id_eon = -1,
            .column_ids = NULL,
            .column_ids_length = 0,
            .householder_beta = -999.,
            .householder_vector = NULL,
            .householder_vector_length = -1,
            .rank = -1,
        };
        return obj;
    }


Parallel factorize_float using MPI with 1D domain decomposition.
    static void
    factorize_mpi_1D_float(Params_HouseholderFactorize_float params)
    {
        float TOLERANCE_CLOSE_TO_ZERO = 1.E-5;
        //--------------------------------------------------
        // Imports
        //--------------------------------------------------
        module_Matrix
        Matrix = import_Matrix();

        module_MpiJob
        MpiJob = import_MpiJob();

        
        //------------------------------------------------------
        //                     OpenMP INFO
        //------------------------------------------------------
        int i_OMP_Volume = params.omp_thread_count;
        //------------------------------------------------------

        //------------------------------------------------------
        //                     MPI INFO
        //------------------------------------------------------
        int i_MPI_ThisRank = -1; // MPI rank of this process
        int i_MPI_Volume   = -1; // MPI process count

        MPI_Comm_rank(params.mpi_communicator, &i_MPI_ThisRank);
        MPI_Comm_size(params.mpi_communicator, &i_MPI_Volume);

        int i_MPI_RootRank = 0;

        bool bool_MPI_ThisIsRoot = false;
        if ( i_MPI_ThisRank == i_MPI_RootRank ) 
        { 
            bool_MPI_ThisIsRoot = true; 
        }
        //--------------------------------------------------

        //--------------------------------------------------
        // confirm MPI/OMP level
        //--------------------------------------------------
        if ( bool_MPI_ThisIsRoot )
        {
            printf("### Householder QR will use %d processes %d omp threads\n", 
                i_MPI_Volume, i_OMP_Volume);
        }
        //--------------------------------------------------


        //--------------------------------------------------
        // initialize constants
        //--------------------------------------------------
        const int i_RowCount    = params.matrix_row_count;
        const int i_ColumnCount = params.matrix_column_count;
        float *A = params.matrix;
        const int i_MPI_TotalTaskCount = i_ColumnCount;
        //--------------------------------------------------
        // Make sure the #cores <= #tasks
        //--------------------------------------------------
        int HouseholderFactorize_NumberOfProcesses = i_MPI_Volume;
        int HouseholderFactorize_NumberOfTasks = i_ColumnCount;
        assert(HouseholderFactorize_NumberOfTasks >= 
            HouseholderFactorize_NumberOfProcesses);

        //-----------------------------------------------------
        // find out the number of columns belonging to me
        //-----------------------------------------------------
        int i_MPI_MyColumnCount = -1;

        Params_MpiJob_TaskCountForMeBlock1D
        params_task_count = {
            .mpi_volume = i_MPI_Volume,
            .mpi_this_rank = i_MPI_ThisRank,
            .mpi_total_task_count = i_MPI_TotalTaskCount,
            .result_OUT = &i_MPI_MyColumnCount,
        };
        MpiJob.task_count_for_me_block_1D(params_task_count);
        //-----------------------------------------------------

        //------------------------------------------------------
        // find out which task IDs are for me
        //------------------------------------------------------
        int i_MPI_MyColumnIdArray[i_MPI_MyColumnCount];

        if ( params.mpi_task_mapping_style == 0 )
        {
            if ( i_MPI_ThisRank == 0 ) 
            {
                puts("### Householder factorize_mpi_1D_float: block mapping ");
            }
            Params_MpiJob_TaskIdsForMeBlock1D
            params_task_ids = MpiJob.new_Params_TaskIdsForMeBlock1D();
            params_task_ids.mpi_this_rank = i_MPI_ThisRank;
            params_task_ids.mpi_volume = i_MPI_Volume;
            params_task_ids.mpi_total_task_count = i_MPI_TotalTaskCount;
            params_task_ids.results_OUT = i_MPI_MyColumnIdArray;
            params_task_ids.results_length = i_MPI_MyColumnCount;
            MpiJob.task_ids_for_me_block_1D(params_task_ids);
        }
        else if (params.mpi_task_mapping_style == 1 )
        {
            if ( i_MPI_ThisRank == 0 ) 
            {
                puts("### Householder factorize_mpi_1D_float: cyclic mapping ");
            }
            Params_MpiJob_TaskIdsForMeCyclic1D
            params_task_ids = MpiJob.new_Params_TaskIdsForMeCyclic1D();
            params_task_ids.mpi_this_rank = i_MPI_ThisRank;
            params_task_ids.mpi_volume = i_MPI_Volume;
            params_task_ids.mpi_total_task_count = i_MPI_TotalTaskCount;
            params_task_ids.results_OUT = i_MPI_MyColumnIdArray;
            params_task_ids.results_length = i_MPI_MyColumnCount;
            MpiJob.task_ids_for_me_cyclic_1D(params_task_ids);
        }
        else
        {
            puts("### argument mpi_task_mapping_style is not supported");
            exit(1);
        }


        //------------------------------------------------------

        //---------------------------------------------------
        // create an array to store the columns the belong
        // to me and also need to be updated.
        // Note: the i_ToBeUpdatedColumnIdArrayLength
        // will be updated later to reflect the 
        // actual number of valid column IDs.
        //---------------------------------------------------
        int i_ToBeUpdatedColumnIdArray[i_MPI_MyColumnCount];
        for ( int i = 0; i < i_MPI_MyColumnCount; i++ )
        {
            i_ToBeUpdatedColumnIdArray[i] = 0; // initialize
        }
        int i_ToBeUpdatedColumnIdArrayLength = 0; // to be updated
        //---------------------------------------------------

        


        
        //--------------------------------------------------
        // define the upper bound for column index k
        int i_kSupremum = params.matrix_row_count - 1;
        if (i_kSupremum > params.matrix_column_count) 
        {
            i_kSupremum = params.matrix_column_count;
        }
        //--------------------------------------------------



        // Use C99's variable length array (VLA) feature
        // no longer need malloc.
        // reminder: VLA cannot be initialized.
        // note: since k is zero-based index, there is
        // no (+1) needed to calculate the length of
        // of the Householder vector.

        //-------------------------------------------------
        // prepare the parameters for MpiJob.is_my_task()
        //-------------------------------------------------
        Params_MpiJob_IsMyTask
        params_IsMyTask = MpiJob.new_Params_IsMyTask();
        params_IsMyTask.my_task_ids = i_MPI_MyColumnIdArray;
        params_IsMyTask.my_task_count = i_MPI_MyColumnCount;
        //-------------------------------------------------

        for (int k = 0; k < i_kSupremum; k++)
        {
            /*******************************************
             * check whether this column belongs to me
             *******************************************/
            params_IsMyTask.task_id = k;

            bool bool_MPI_ThisColumnIsMine = 
                MpiJob.is_my_task(params_IsMyTask);

            /*******************************************/

            bool  bool_SkipColumnUpdate = false;
            int   vector_length = i_RowCount - k;
            int   buffer_length = vector_length + 1;
            float householder_beta = -999.;
            float f_HouseholderBuffer[buffer_length];

            // householder_vector is just an alias
            // of the first 'vector_length' elements
            // of the send buffer.
            float *householder_vector = &f_HouseholderBuffer[0];


            /**************************************/
            if ( bool_MPI_ThisColumnIsMine )
            /**************************************/
            {   
                //----------------------------------------
                // Compute Householder vector.
                //----------------------------------------
                Params_HouseholderFactorize_GetHouseholderVector_float
                params_householder_vector = new_Params_GetHouseholderVector_float();
                params_householder_vector.matrix = A;
                params_householder_vector.current_column_id = k;
                params_householder_vector.matrix_row_count    = i_RowCount;
                params_householder_vector.matrix_column_count = i_ColumnCount;
                params_householder_vector.result_OUT = householder_vector;
                get_householder_vector_float(params_householder_vector);

                //----------------------------------
                // compute Householder beta
                //----------------------------------
                Params_HouseholderFactorize_GetHouseholderBeta_float
                params_get_beta = 
                new_Params_GetHouseholderBeta_float();
                params_get_beta.householder_vector = householder_vector;
                params_get_beta.householder_vector_length = vector_length;
                params_get_beta.result_OUT = &householder_beta;
                get_householder_beta_float(params_get_beta);
                //----------------------------------


                /******************************************/
                /** copy householder_beta to buffer      **/
                /******************************************/
                f_HouseholderBuffer[buffer_length - 1] = householder_beta;


                //----------------------------------
                // check beta
                //----------------------------------
                if (is_close_to_zero_float(householder_beta, TOLERANCE_CLOSE_TO_ZERO))
                {
                    // This means the sub-diagnoal is already zero
                    // and also means the diagonal element is zero
                    // and the matrix A is singular
                    bool_SkipColumnUpdate = true; 
                }

            }

            /**********************************************
             * Join the broad cast to receive Householder
             * vector and beta.
             *********************************************/
            int i_BroadcastRootRank = -1;
            if ( params.mpi_task_mapping_style == 0 )
            {
                //------------------------------------------------------
                // make an object to remember which task belongs to which 
                // process.
                //------------------------------------------------------
                int internal_storage_for_RankIdBlock1D[i_MPI_Volume];
                Class_MpiJob_RankIdBlock1D
                    obj_rank = MpiJob.new_RankIdBlock1D(
                        i_MPI_Volume,
                        i_ColumnCount,
                        internal_storage_for_RankIdBlock1D);

                if (obj_rank.success == false)
                {
                    printf("WARNING: ERROR OCCURED WHEN SETTING UP THE PARALLEL DOMAINI DECOMNPOSITION.\n");
                }
                //------------------------------------------------------
                i_BroadcastRootRank = obj_rank.rank(obj_rank, k);
            }
            else if ( params.mpi_task_mapping_style == 1 )
            {
                //------------------------------------------------------
                // make an object to remember which task belongs to which 
                // process.
                //------------------------------------------------------
                Class_MpiJob_RankIdCyclic1D
                    obj_rank = MpiJob.new_RankIdCyclic1D(
                        i_MPI_Volume,
                        i_ColumnCount);

                if (obj_rank.success == false)
                {
                    printf("WARNING: ERROR OCCURED WHEN SETTING UP THE PARALLEL DOMAINI DECOMNPOSITION.\n");
                }
                //------------------------------------------------------
                i_BroadcastRootRank = obj_rank.rank(obj_rank, k);
            }
            else
            {
                puts("### argument mpi_task_mapping_style is not supported");
                exit(1);
            }
            MPI_Bcast(
                f_HouseholderBuffer,
                buffer_length,
                MPI_FLOAT,
                i_BroadcastRootRank,
                params.mpi_communicator);
            /*********************************************/

            /***********************************************/
            /* Get Householder beta from the buffer    */
            /***********************************************/
            householder_beta = f_HouseholderBuffer[buffer_length - 1];
            /*********************************************/

            //-----------------------------------------
            // apply householder vector to columns on the right.
            //-----------------------------------------
            i_ToBeUpdatedColumnIdArrayLength = 0;

            for (int j = k; j < i_ColumnCount; j++)
            {
                /*****************************************/
                params_IsMyTask.task_id = j;
                if ( ! MpiJob.is_my_task(params_IsMyTask) ) { continue; }
                if ( bool_SkipColumnUpdate ) { continue; }
                /*****************************************/

                // note: I use i_ToBeUpdatedColumnIdArrayLength both
                // as index and a length.
                // However, remember that length = max(index) + 1
                // That's why I choose to increase it after performing 
                // the indexing.
                i_ToBeUpdatedColumnIdArray[i_ToBeUpdatedColumnIdArrayLength] = j;
                i_ToBeUpdatedColumnIdArrayLength += 1;
            }

            //---------------------------------------
            // update sub-diagonal matrix column items
            //---------------------------------------
            if ( i_ToBeUpdatedColumnIdArrayLength > 0 )
            {
                Params_HouseholderFactorize_OMP_UpdateMatrixColumn_float
                params_update_column = new_Params_OMP_UpdateMatrixColumn_float();
                params_update_column.omp_thread_count = i_OMP_Volume;
                params_update_column.matrix = A;
                params_update_column.matrix_row_count = i_RowCount;
                params_update_column.matrix_column_count = i_ColumnCount;
                params_update_column.row_id_eon = k;
                params_update_column.column_ids = i_ToBeUpdatedColumnIdArray;
                params_update_column.column_ids_length = i_ToBeUpdatedColumnIdArrayLength;
                params_update_column.householder_beta = householder_beta;
                params_update_column.householder_vector = householder_vector;
                params_update_column.householder_vector_length = vector_length;
                params_update_column.rank = i_MPI_ThisRank;
                omp_update_matrix_column(params_update_column);
            }

            //----------------------------------------------------
            // copy data to V_OUT
            //----------------------------------------------------

            if ( bool_MPI_ThisIsRoot )
            {
                Params_Matrix_CopyVectorToColumn_float
                params_copy_vector = Matrix.new_Params_CopyVectorToColumn_float();
                params_copy_vector.matrix = params.V_OUT;
                params_copy_vector.matrix_row_count = i_RowCount;
                params_copy_vector.matrix_column_count = i_ColumnCount;
                params_copy_vector.row_id_eon = k;
                params_copy_vector.row_id_end = i_RowCount - 1;
                params_copy_vector.column_id = k;
                params_copy_vector.vector = householder_vector;
                params_copy_vector.vector_length = vector_length;
                Matrix.copy_vector_to_column_float(params_copy_vector);
            }
        }


        /*********************************************************
         * MPI gather columns from all processes
         *********************************************************/

        //--------------------------------------------------------
        // Allocate receiver buffer for both senders and receivers
        // (because MPI_Bcast demands this)
        //--------------------------------------------------------
        int   i_MPI_ReceiveBufferLength = i_RowCount * i_ColumnCount;
        float f_MPI_ReceiveBufferArray[i_MPI_ReceiveBufferLength];

        IN_MPI_GatherColumns_1D_float
        params_gather_columns = new_IN_MPI_GatherColumns_1D_float();
        params_gather_columns.omp_thread_count = i_OMP_Volume;
        params_gather_columns.matrix = A;
        params_gather_columns.matrix_row_count = i_RowCount;
        params_gather_columns.matrix_column_count = i_ColumnCount;
        params_gather_columns.my_column_ids = i_MPI_MyColumnIdArray;
        params_gather_columns.my_column_ids_length = i_MPI_MyColumnCount;
        params_gather_columns.mpi_volume = i_MPI_Volume;
        params_gather_columns.mpi_root_rank = i_MPI_RootRank;
        params_gather_columns.mpi_this_rank = i_MPI_ThisRank;
        params_gather_columns.mpi_receive_buffer = f_MPI_ReceiveBufferArray;
        params_gather_columns.mpi_receive_buffer_length = i_MPI_ReceiveBufferLength;
        params_gather_columns.mpi_communicator = params.mpi_communicator;
        mpi_gather_columns_1D_float(params_gather_columns);
        //--------------------------------------------------------

        /*********************************************************/


        /**********************************************/
        /* Copy receive buffer items to output matrix */
        /**********************************************/
        if (bool_MPI_ThisIsRoot)
        {
            if ( params.mpi_task_mapping_style == 0 )
            {
                //-----------------------------------------------------
                // Block mapping
                // find out which index of the receive buffer 
                // from which to copy the content
                //-----------------------------------------------------
                int i_IndexEon = 0;
                int column_id_eon = 0;
                float *my_vector = &f_MPI_ReceiveBufferArray[i_IndexEon];
                float  my_vector_length = i_ColumnCount * i_RowCount;

                Params_Matrix_CopyVectorToColumns_float
                params_copy = Matrix.new_Params_CopyVectorToColumns_float();
                params_copy.matrix = A;
                params_copy.matrix_row_count = i_RowCount;
                params_copy.matrix_column_count = i_ColumnCount;
                params_copy.row_id_eon = 0;
                params_copy.row_id_end = i_RowCount - 1;
                params_copy.column_id_eon = column_id_eon;
                params_copy.column_id_end = i_ColumnCount - 1;
                params_copy.vector = my_vector;
                params_copy.vector_length = my_vector_length;
                Matrix.copy_vector_to_columns_float(params_copy);
            }
            else if ( params.mpi_task_mapping_style == 1 )
            {
                //-----------------------------------------------------
                // Cyclic mapping
                //-----------------------------------------------------

                //-----------------------------------------------
                // first find the correct order of column IDs 
                //-----------------------------------------------
                int i_CyclicTaskIdArrayLength = i_ColumnCount;
                int i_CyclicTaskIdArray[i_CyclicTaskIdArrayLength];

                int ccc = 0;
                for ( int i = 0; i < i_MPI_Volume; i++ )
                {
                    int rank = i;
                    for ( int j = 0; j < i_ColumnCount; j++ )
                    {
                        if ( (j % i_MPI_Volume) == rank )
                        {
                            i_CyclicTaskIdArray[ccc] = j;
                            ccc++;
                        }
                    }
                }


                for ( int i = 0; i < i_ColumnCount; i++ )
                {
                    int i_IndexEon = i * i_ColumnCount;
                    int i_ColumnId = i_CyclicTaskIdArray[i];

                    float *my_vector = &f_MPI_ReceiveBufferArray[i_IndexEon];
                    float  my_vector_length = i_ColumnCount;

                    Params_Matrix_CopyVectorToColumn_float
                    params_copy = Matrix.new_Params_CopyVectorToColumn_float();
                    params_copy.matrix = A;
                    params_copy.matrix_row_count = i_RowCount;
                    params_copy.matrix_column_count = i_ColumnCount;
                    params_copy.row_id_eon = 0;
                    params_copy.row_id_end = i_RowCount - 1;
                    params_copy.column_id  = i_ColumnId;
                    params_copy.vector = my_vector;
                    params_copy.vector_length = my_vector_length;
                    Matrix.copy_vector_to_column_float(params_copy);
                }
            }
            else
            {
                puts("### argument mpi_task_mapping_style is not supported");
                exit(1);
            }
        }
        /**********************************************/

    }

Main function factorize_float.
    static void
    factorize_float(Params_HouseholderFactorize_float params)
    {
        float TOLERANCE_CLOSE_TO_ZERO = 1.E-5;

     
        //------------------------------------------------------
        //                     OpenMP INFO
        //------------------------------------------------------
        int i_OMP_Volume = params.omp_thread_count;
        //------------------------------------------------------

        //--------------------------------------------------
        // confirm MPI/OMP level
        //--------------------------------------------------
        printf("### Serial Householder QR will %d omp threads\n", 
            i_OMP_Volume);
        //--------------------------------------------------


        //--------------------------------------------------
        // Imports
        //--------------------------------------------------
        module_Matrix
        Matrix = import_Matrix();

        //--------------------------------------------------
        // initialize constants
        //--------------------------------------------------
        const int i_RowCount    = params.matrix_row_count;
        const int i_ColumnCount = params.matrix_column_count;
        float *A = params.matrix;
        //--------------------------------------------------
        
        //--------------------------------------------------
        // define the upper bound for column index k
        int i_kSupremum = params.matrix_row_count - 1;
        if (i_kSupremum > params.matrix_column_count) 
        {
            i_kSupremum = params.matrix_column_count;
        }
        //--------------------------------------------------

        //---------------------------------------------------
        // create an array to store the columns the belong
        // to me and also need to be updated.
        // Note: i_ToBeUpdatedColumnIdArrayLength will be
        // updated later to reflect the actual valid 
        // column IDs.
        //---------------------------------------------------
        int i_ToBeUpdatedColumnIdArray[i_ColumnCount];
        for ( int i = 0; i < i_ColumnCount; i++ )
        {
            i_ToBeUpdatedColumnIdArray[i] = 0; // initialize
        }
        int i_ToBeUpdatedColumnIdArrayLength = 0; // to-be changed
        //---------------------------------------------------


        // Use C99's variable length array (VLA) feature
        // no longer need malloc.
        // reminder: VLA cannot be initialized.
        // note: since k is zero-based index, there is
        // no (+1) needed to calculate the length of
        // of the Householder vector.

        for (int k = 0; k < i_kSupremum; k++)
        {
            int   vector_length = i_RowCount - k;

            float householder_vector[vector_length];

            //----------------------------------------
            // Get Householder vector.
            //----------------------------------------
            Params_HouseholderFactorize_GetHouseholderVector_float
            params_householder_vector = new_Params_GetHouseholderVector_float();
            params_householder_vector.matrix = A;
            params_householder_vector.current_column_id = k;
            params_householder_vector.matrix_row_count    = i_RowCount;
            params_householder_vector.matrix_column_count = i_ColumnCount;
            params_householder_vector.result_OUT = householder_vector;
            get_householder_vector_float(params_householder_vector);

            //----------------------------------
            // compute Householder beta
            //----------------------------------
            float householder_beta = -999.;
            
            Params_HouseholderFactorize_GetHouseholderBeta_float
            params_get_beta = 
            new_Params_GetHouseholderBeta_float();
            params_get_beta.householder_vector = householder_vector;
            params_get_beta.householder_vector_length = vector_length;
            params_get_beta.result_OUT = &householder_beta;
            get_householder_beta_float(params_get_beta);
            //----------------------------------

            
            //----------------------------------
            // check beta
            //----------------------------------
            if (is_close_to_zero_float(householder_beta, TOLERANCE_CLOSE_TO_ZERO))
            {
                // This means the sub-diagnoal is already zero
                // and also means the diagonal element is zero
                // and the matrix A is singular
                continue; 
            }

            //-----------------------------------------
            // apply householder vector to columns on the right.
            //-----------------------------------------
            i_ToBeUpdatedColumnIdArrayLength = 0;
            for (int j = k; j < i_ColumnCount; j++)
            {
                // note: I use i_ToBeUpdatedColumnIdArrayLength both
                // as index and a length.
                // However, remember that length = max(index) + 1
                // That's why I choose to increase it after performing 
                // the indexing.
                i_ToBeUpdatedColumnIdArray[i_ToBeUpdatedColumnIdArrayLength] = j;
                i_ToBeUpdatedColumnIdArrayLength += 1;
            }

            //---------------------------------------
            // update sub-diagonal matrix column items
            //---------------------------------------
            Params_HouseholderFactorize_OMP_UpdateMatrixColumn_float
            params_update_column = new_Params_OMP_UpdateMatrixColumn_float();
            params_update_column.omp_thread_count = i_OMP_Volume;
            params_update_column.matrix = A;
            params_update_column.matrix_row_count = i_RowCount;
            params_update_column.matrix_column_count = i_ColumnCount;
            params_update_column.row_id_eon = k;
            params_update_column.column_ids = i_ToBeUpdatedColumnIdArray;
            params_update_column.column_ids_length = i_ToBeUpdatedColumnIdArrayLength;
            params_update_column.householder_beta = householder_beta;
            params_update_column.householder_vector = householder_vector;
            params_update_column.householder_vector_length = vector_length;
            params_update_column.rank = 0;
            omp_update_matrix_column(params_update_column);

            //----------------------------------------------------
            // copy data to V_OUT
            //----------------------------------------------------
            Params_Matrix_CopyVectorToColumn_float
            params_copy_vector = Matrix.new_Params_CopyVectorToColumn_float();
            params_copy_vector.matrix = params.V_OUT;
            params_copy_vector.matrix_row_count = i_RowCount;
            params_copy_vector.matrix_column_count = i_ColumnCount;
            params_copy_vector.row_id_eon = k;
            params_copy_vector.row_id_end = i_RowCount - 1;
            params_copy_vector.column_id = k;
            params_copy_vector.vector = householder_vector;
            params_copy_vector.vector_length = vector_length;
            Matrix.copy_vector_to_column_float(params_copy_vector);
        }
    }



## module_HouseholderFactorize worker functions


Get the Householder vector.
    static void
    get_householder_vector_float(Params_HouseholderFactorize_GetHouseholderVector_float params)
    {
        //note: the output vector params.result_OUT
        // has length (m-k+1) where m is the number
        // of rows and k is the current column ID.

        //----------------------------------
        // External dependencies
        //----------------------------------
        module_Matrix
        Matrix = import_Matrix();

        module_MathSign
        MathSign = import_MathSign();

        int (*sign)(float) = MathSign.sign_float;

        Class_MatrixIndex2D
        obj_Id = Matrix.new_MatrixIndex2D(
            params.matrix_row_count, params.matrix_column_count);
        //----------------------------------
        //----------------------------------
        //----------------------------------

        int column_id  =  -1;
        int row_id_eon =  -1;
        int row_id_end =  -1;
        float f_ColumnNorm = -9999.;

        column_id  = params.current_column_id;
        row_id_eon = params.current_column_id;
        row_id_end = params.matrix_row_count - 1;

        Params_Matrix_o2NormColumn_float
        column_norm_params = Matrix.new_Params_o2NormColumn_float();
        column_norm_params.matrix = params.matrix;
        column_norm_params.row_id_eon = row_id_eon;
        column_norm_params.row_id_end = row_id_end;
        column_norm_params.column_id  = column_id;
        column_norm_params.matrix_row_count    = params.matrix_row_count;
        column_norm_params.matrix_column_count = params.matrix_column_count;
        column_norm_params.result_OUT = &f_ColumnNorm;

        Matrix.o2_norm_column_float(column_norm_params);

        //--------------------------------
        // get matrix element A[k,k]
        //--------------------------------
        int index = obj_Id.index(
            obj_Id, 
            params.current_column_id,
            params.current_column_id);

        float A_kk = params.matrix[index];
        //--------------------------------
        float scalar = -1. * sign(A_kk) * f_ColumnNorm;
        //--------------------------------
        // copy data from matrix A to the
        // Householder vector
        //--------------------------------
        int i_OutputLength = 
            obj_Id.matrix_row_count - params.current_column_id;

        for (int i = 0; i < i_OutputLength; i++)
        {
            int i_RowId = params.current_column_id + i;
            int index = obj_Id.index(obj_Id, 
                i_RowId, params.current_column_id);
            params.result_OUT[i] = params.matrix[index];
        }

        //--------------------------------
        // update the Householder vector
        //--------------------------------
        params.result_OUT[0] -= scalar;
    }

Compute Householder beta 
    static void
    get_householder_beta_float(Params_HouseholderFactorize_GetHouseholderBeta_float params)
    {
        module_Vector
        Vector = import_Vector();

        Params_Vector_Dot_float
        params_dot = {
            .vector1 = params.householder_vector,
            .vector2 = params.householder_vector,
            .length  = params.householder_vector_length,
            .result_OUT = params.result_OUT,
        };

        Vector.dot_float(params_dot);
    }

Compute Householder gamma
    static void
    get_householder_gamma_float(Params_HouseholderFactorize_GetHouseholderGamma_float params)
    {
        //---------------------------
        // External dependency
        //---------------------------
        module_Matrix
        Matrix = import_Matrix();

        Params_Matrix_DotColumn_float
        params_dot_column = {
            .matrix = params.matrix,
            .matrix_row_count = params.matrix_row_count,
            .matrix_column_count = params.matrix_column_count,
            .vector  = params.householder_vector,
            .vector_length = params.householder_vector_length,
            .row_id_eon = params.row_id_eon,
            .row_id_end = params.matrix_row_count - 1,
            .column_id  = params.column_id,
            .result_OUT = params.result_OUT,
        };

        Matrix.dot_column_float(params_dot_column);
    }

Update matrix column with the Householder beta and gamma.
    static void
    update_matrix_column(Params_HouseholderFactorize_UpdateMatrixColumn_float params)
    {
        //-------------------------------
        // import
        //-------------------------------
        module_Matrix
        Matrix = import_Matrix();
        //-------------------------------

        float scalar = -2. * (params.householder_gamma / params.householder_beta);
        
        Params_Matrix_AddScaledVectorToColumn_float
        inputs = {
            .matrix = params.matrix,
            .matrix_row_count = params.matrix_row_count,
            .matrix_column_count = params.matrix_column_count,
            .row_id_eon = params.row_id_eon,
            .row_id_end = params.matrix_row_count - 1,
            .column_id = params.column_id,
            .vector = params.householder_vector,
            .vector_id_eon = 0,
            .vector_id_end = params.householder_vector_length - 1,
            .scalar = scalar,
        };

        Matrix.add_scaled_vector_to_column_float(inputs);
    }

Update matrix column with the Householder beta and gamma.
    static void
    omp_update_matrix_column(Params_HouseholderFactorize_OMP_UpdateMatrixColumn_float params)
    {
        //-------------------------------
        // import
        //-------------------------------
        module_Matrix
        Matrix = import_Matrix();

        module_HouseholderFactorize
        Householder = import_HouseholderFactorize();
        //-------------------------------

        /*********************************************
         *                OpenMP setup               *
         *********************************************/
        int i_OMP_Volume = params.omp_thread_count;

        if ( i_OMP_Volume > params.column_ids_length ) 
            { i_OMP_Volume = params.column_ids_length; }

        omp_set_num_threads(i_OMP_Volume);
        /*********************************************/

        #pragma omp parallel for 
        for ( int i = 0; i < params.column_ids_length; i++ )
        {
            int i_ColumnId = params.column_ids[i];
           
            //---------------------------------------
            // compute householder gamma
            //---------------------------------------
            float householder_gamma = -999.;

            Params_HouseholderFactorize_GetHouseholderGamma_float
            params_get_gamma = Householder.new_Params_GetHouseholderGamma_float();
            params_get_gamma.matrix = params.matrix;
            params_get_gamma.matrix_row_count = params.matrix_row_count;
            params_get_gamma.matrix_column_count = params.matrix_column_count;
            params_get_gamma.row_id_eon = params.row_id_eon;
            params_get_gamma.column_id = i_ColumnId;
            params_get_gamma.householder_vector = params.householder_vector;
            params_get_gamma.householder_vector_length = params.householder_vector_length;
            params_get_gamma.result_OUT = &householder_gamma;
            get_householder_gamma_float(params_get_gamma);
            //---------------------------------------

            float scalar = -2. * (householder_gamma / params.householder_beta);
            
            Params_Matrix_AddScaledVectorToColumn_float
            inputs = {
                .matrix = params.matrix,
                .matrix_row_count = params.matrix_row_count,
                .matrix_column_count = params.matrix_column_count,
                .row_id_eon = params.row_id_eon,
                .row_id_end = params.matrix_row_count - 1,
                .column_id = i_ColumnId,
                .vector = params.householder_vector,
                .vector_id_eon = 0,
                .vector_id_end = params.householder_vector_length - 1,
                .scalar = scalar,
            };
            Matrix.add_scaled_vector_to_column_float(inputs);
        }
    }

A function checks whether a number is close to zero.
    static bool
    is_close_to_zero_float(float number, float tolerance)
    {
        if (fabs(number) <= tolerance) { return true; }
        else { return false; }
    }

This function will gather the updated columns from all MPI procceses.
    typedef struct 
    IN_MPI_GatherColumns_1D_float
    {   
        float *matrix;
        int    matrix_row_count;
        int    matrix_column_count;
        int   *my_column_ids;
        int    my_column_ids_length;
        int    mpi_volume;
        int    mpi_root_rank;
        int    mpi_this_rank;
        float *mpi_receive_buffer;
        int    mpi_receive_buffer_length;
        MPI_Comm mpi_communicator;
        int    omp_thread_count;
    }
    IN_MPI_GatherColumns_1D_float;

    static IN_MPI_GatherColumns_1D_float
    new_IN_MPI_GatherColumns_1D_float(void)
    {
        IN_MPI_GatherColumns_1D_float
        obj = {
            .matrix = NULL,
            .matrix_row_count = -1,
            .matrix_column_count = -1,
            .my_column_ids = NULL,
            .my_column_ids_length = 0,
            .mpi_volume = -1,
            .mpi_root_rank = -1,
            .mpi_this_rank = -1,
            .mpi_receive_buffer = NULL,
            .mpi_receive_buffer_length = -1,
            .mpi_communicator = MPI_COMM_NULL,
            .omp_thread_count = 1,
        };
        return obj;
    }

    static void 
    mpi_gather_columns_1D_float(IN_MPI_GatherColumns_1D_float params)
    {
        /**********************************************/
        /* Gather all the columns to the root process */
        /**********************************************/
        //--------------------------------------------------------
        //                  Import
        //--------------------------------------------------------
        module_MpiJob
        MpiJob = import_MpiJob();
        
        module_Vector
        Vector = import_Vector();

        module_Matrix
        Matrix = import_Matrix();
        
        //--------------------------------------------------------
        //                   Aliases
        //--------------------------------------------------------
        int i_MPI_GathervRoot = params.mpi_root_rank;
        int i_MPI_Volume = params.mpi_volume;
        int i_MPI_TotalTaskCount = params.matrix_column_count;
        int i_MPI_MyColumnCount = params.my_column_ids_length;
        float *f_MPI_ReceiveBufferArray  = params.mpi_receive_buffer;
        int    i_MPI_ReceiveBufferLength = params.mpi_receive_buffer_length;

        float *A = params.matrix;
        int    i_RowCount    = params.matrix_row_count;
        int    i_ColumnCount = params.matrix_column_count;
        //--------------------------------------------------------

        //--------------------------------------------------------
        // check the length of the receive buffer
        //--------------------------------------------------------
        int i_Required_ReceiveBufferLength = i_RowCount * i_ColumnCount;
        assert(i_MPI_ReceiveBufferLength == i_Required_ReceiveBufferLength);
        //--------------------------------------------------------


        //--------------------------------------------------------
        // Prepare for scaling two vectors:
        // 1. i_MPI_GathervDisplacementArray
        // 2. i_MPI_GatherReceiveCountArray
        //--------------------------------------------------------
        Params_Vector_Scale_int
        params_scale_vector = {
            .vector = NULL,
            .length = -1,
            .scalar = -999,
        };

        //--------------------------------------------------------
        // Calculate the receiver displacement array
        //--------------------------------------------------------
        int i_MPI_GathervDisplacementArrayLength = i_MPI_Volume;
        int i_MPI_GathervDisplacementArray[i_MPI_GathervDisplacementArrayLength];
        
        Params_MpiJob_GathervDisplacementsBlock1D
        params_gatherv_displacements = MpiJob.new_Params_GathervDisplacementsBlock1D();
        params_gatherv_displacements.mpi_volume = i_MPI_Volume;
        params_gatherv_displacements.mpi_total_task_count = i_MPI_TotalTaskCount;
        params_gatherv_displacements.results_OUT = i_MPI_GathervDisplacementArray;
        MpiJob.gatherv_displacements_block_1D(params_gatherv_displacements);
        
        params_scale_vector.vector = i_MPI_GathervDisplacementArray;
        params_scale_vector.length = i_MPI_GathervDisplacementArrayLength;
        params_scale_vector.scalar = i_RowCount;
        Vector.scale_int(params_scale_vector);
        //--------------------------------------------------------


        //--------------------------------------------------------
        // Calculate the receive count array
        //--------------------------------------------------------
        int i_MPI_GathervReceiveCountArrayLength = i_MPI_Volume;
        int i_MPI_GathervReceiveCountArray[i_MPI_GathervReceiveCountArrayLength];

        Params_MpiJob_TaskCountForAllBlock1D
        params_task_count_for_all = MpiJob.new_Params_TaskCountForAllBlock1D();
        params_task_count_for_all.mpi_volume = i_MPI_Volume;
        params_task_count_for_all.mpi_total_task_count = i_MPI_TotalTaskCount;
        params_task_count_for_all.results_OUT = i_MPI_GathervReceiveCountArray;
        MpiJob.task_count_for_all_block_1D(params_task_count_for_all);
        
        
        params_scale_vector.vector = i_MPI_GathervReceiveCountArray;
        params_scale_vector.length = i_MPI_GathervReceiveCountArrayLength;
        params_scale_vector.scalar = i_RowCount;
        Vector.scale_int(params_scale_vector);
        //--------------------------------------------------------

        /**********************************************************
         * Send my columns to the MPI root
         **********************************************************/

        //-----------------------------------------------------
        // Copy my columns to the send buffer
        //-----------------------------------------------------
        int   i_MPI_SendBufferLength = i_MPI_MyColumnCount * i_RowCount;
        float f_MPI_SendBufferArray[i_MPI_SendBufferLength];

        // /*********************************************
        //  *                OpenMP setup               *
        //  *********************************************/
        // int i_OMP_Volume = params.omp_thread_count;

        // if ( i_OMP_Volume > i_MPI_MyColumnCount ) 
        //     { i_OMP_Volume = i_MPI_MyColumnCount; }

        // omp_set_num_threads(i_OMP_Volume);
        // /*********************************************/

        // #pragma omp parallel for 
        //-----------------------------------------------------
        for ( int i = 0; i < i_MPI_MyColumnCount; i++ )
        {
            int i_IndexEon = i * i_RowCount;
            float *buffer = &f_MPI_SendBufferArray[i_IndexEon]; 
            int buffer_length = i_RowCount;

            int i_ColumnId = params.my_column_ids[i];

            Params_Matrix_CopyColumnToVector_float
            params_copy_column = Matrix.new_Params_CopyColumnToVector_float();
            params_copy_column.matrix = A;
            params_copy_column.matrix_row_count = i_RowCount;
            params_copy_column.matrix_column_count = i_ColumnCount;
            params_copy_column.row_id_eon = 0;
            params_copy_column.row_id_end = i_RowCount - 1;
            params_copy_column.column_id = i_ColumnId;
            params_copy_column.vector_OUT = buffer;
            params_copy_column.vector_length = buffer_length;
            Matrix.copy_column_to_vector_float(params_copy_column);
        }



        MPI_Gatherv(
            f_MPI_SendBufferArray,
            i_MPI_SendBufferLength,
            MPI_FLOAT,
            f_MPI_ReceiveBufferArray,
            i_MPI_GathervReceiveCountArray,
            i_MPI_GathervDisplacementArray,
            MPI_FLOAT,
            i_MPI_GathervRoot,
            params.mpi_communicator);
        /**********************************************/
    }
