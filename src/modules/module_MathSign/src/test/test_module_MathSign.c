#include "test_module_MathSign.dep.c"

static void
test_hi(void)
{
    module_MathSign
    MathSign = import_MathSign();

    MathSign.hi();
}

static void 
test_sign(void)
{
	puts("\nTest sign()\n");

    module_MathSign
    MathSign = import_MathSign();

    module_Type
    Type = import_Type();

    MyTypeChoice 
    type = Type.Float;

    float list_Inputs[3]     = {2., -2., 0.};
    float list_AnswerKeys[3] = {1, -1, 0};
    float tolerance = 1.E-5;

    for (int i = 0; i < 3; i++)
    {

        Params_MathSign
        params = {
            .input = &list_Inputs[i],
            .type = type,
        };

        int answer = MathSign.sign(params);
        int answer_key = list_AnswerKeys[i];
        printf("answer = %d; key = %d\n", answer, answer_key);
        assert(abs(answer - answer_key) < tolerance);
    }
}

static void
test_sign_float(void)
{
	puts("\nTest sign_float()\n");

    module_MathSign
    MathSign = import_MathSign();

    float list_Inputs[3]     = {2., -2., 0.};
    float list_AnswerKeys[3] = {1, -1, 0};
    float tolerance = 1.E-5;

    for (int i = 0; i < 3; i++)
    {	float input = list_Inputs[i];
    int answer = MathSign.sign_float(input);
    int answer_key = list_AnswerKeys[i];
    printf("answer = %d; key = %d\n", answer, answer_key);
    assert(abs(answer - answer_key) < tolerance);
        
    }
}

int
main(void)
{
    puts("------------------");
    test_hi();
    puts("------------------");
    test_sign();
    puts("------------------");
    test_sign_float();
    puts("------------------");
    return 0;
}