# LICENSE
    /**********************************************************
     Copyright (c) 2015 YUHANG WANG 
     ALL RIGHTS RESERVED

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
    ************************************************************/



# LICENSE
    /**********************************************************
     Copyright (c) 2015 YUHANG WANG 
     ALL RIGHTS RESERVED

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
    ************************************************************/



# Module module_MathSign
This module provide the mathematical sign function.

#[module_MathSign.dep.h](#module_MathSign.dep.h "save:")
    #include "module_Type.h"

#[module_MathSign.h](#module_MathSign.h "save:")
    #ifndef MODULE_MATHSIGN_H
    #define MODULE_MATHSIGN_H

    #include "module_MathSign.dep.h"

    typedef struct
    Params_MathSign
    {
        void *input;
        MyTypeChoice type;
    }
    Params_MathSign;

    typedef struct 
    module_MathSign
    {
        void (*hi)(void);
        int  (*sign)(Params_MathSign);
        int  (*sign_float)(float);
    }
    module_MathSign;

    module_MathSign
    import_MathSign(void);

    #endif



#[module_MathSign.dep.c](#module_MathSign.dep.c "save:")
    #include "module_MathSign.h"
    #include <stdio.h>


#[module_MathSign.c](#module_MathSign.c "save:")
    #include "module_MathSign.dep.c"
    
    _"module_MathSign worker functions"

    _"module_MathSign implementation"

    module_MathSign
    import_MathSign(void)
    {
        module_MathSign obj = {
            .hi = hi,
            .sign = sign,
            .sign_float = sign_float,
        };
        return obj;
    }


# module_MathSign implementation
    static void
    hi(void)
    {
        printf("Greetings from module_MathSign!\n");
    }

Frontend function module_MathSign.sign 

    int
    sign(Params_MathSign params)
    {
        int output = 1;

        module_Type
        Type = import_Type();

        if (Type.same(params.type, Type.Float))
        {
            printf("(from module_MathSign.sign) your input type is float\n");
            float *input = (float*) params.input;
            output = sign_float(*input);
        }

        return output;
    }

# module_MathSign worker functions

Float version of the sign function.

    int
    sign_float(float input)
    {
        int output = 1;
        if ( input > 0. ) { output = 1; } 
        else if ( input < 0. ) { output = -1; }
        else { output = 0; }
        return output;
    }
