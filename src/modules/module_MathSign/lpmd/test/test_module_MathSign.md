# Test module_MathSign
This is a test suite for module_MathSign.

#[test_module_MathSign.dep.h](#test_module_MathSign.dep.h "save:")
    /**/

#[test_module_MathSign.h](#test_module_MathSign.h "save:")
    /**/

#[test_module_MathSign.dep.c](#test_module_MathSign.dep.c "save:")
    #include "test_module_MathSign.h"
    #include "module_MathSign.h"
    #include <assert.h>
    #include <stdio.h>
    #include <stdlib.h>

#[test_module_MathSign.c](#test_module_MathSign.c "save:")
    #include "test_module_MathSign.dep.c"

    _"test_module_MathSign all tests"

    int
    main(void)
    {
        puts("------------------");
        test_hi();
        puts("------------------");
        test_sign();
        puts("------------------");
        test_sign_float();
        puts("------------------");
        return 0;
    }


# test_module_MathSign all tests

Test hi
    static void
    test_hi(void)
    {
        module_MathSign
        MathSign = import_MathSign();

        MathSign.hi();
    }

Test sign 
    static void 
    test_sign(void)
    {
    	puts("\nTest sign()\n");

        module_MathSign
        MathSign = import_MathSign();

        module_Type
        Type = import_Type();

        MyTypeChoice 
        type = Type.Float;

        float list_Inputs[3]     = {2., -2., 0.};
        float list_AnswerKeys[3] = {1, -1, 0};
        float tolerance = 1.E-5;

        for (int i = 0; i < 3; i++)
        {

            Params_MathSign
            params = {
                .input = &list_Inputs[i],
                .type = type,
            };

            int answer = MathSign.sign(params);
            int answer_key = list_AnswerKeys[i];
            printf("answer = %d; key = %d\n", answer, answer_key);
            assert(abs(answer - answer_key) < tolerance);
        }
    }

Test sign_float
    static void
    test_sign_float(void)
    {
    	puts("\nTest sign_float()\n");

        module_MathSign
        MathSign = import_MathSign();

        float list_Inputs[3]     = {2., -2., 0.};
        float list_AnswerKeys[3] = {1, -1, 0};
        float tolerance = 1.E-5;

        for (int i = 0; i < 3; i++)
        {	float input = list_Inputs[i];
	        int answer = MathSign.sign_float(input);
	        int answer_key = list_AnswerKeys[i];
	        printf("answer = %d; key = %d\n", answer, answer_key);
	        assert(abs(answer - answer_key) < tolerance);
	            
        }
    }



