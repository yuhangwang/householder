# Module module_Assert
This is a module for providing better information on testing
using assert for both scalar values and arrays.


#[module_Assert.dep.h](#module_Assert.dep.h "save:")
    _"LICENSE"

    #include "module_Type.h"
    #include <stdbool.h>

#[module_Assert.h](#module_Assert.h "save:")
Note: defining parameter data type helps keeing source code
easy to modify (only need to modify one place).

    _"LICENSE"

    /*****************************************************/
    #ifndef MODULE_ASSERT_H
    #define MODULE_ASSERT_H

    #include "module_Assert.dep.h"
    
    typedef struct 
    Params_Assert_Equal
    {
        void *input1;
        void *input2;
        MyTypeChoice type;
        double tolerance;
    }
    Params_Assert_Equal;

    typedef struct 
    Params_Assert_Equal_int
    {
        int input1;
        int input2;
    }
    Params_Assert_Equal_int;

    typedef struct 
    Params_Assert_Equal_float
    {
        float input1;
        float input2;
        float tolerance;
    }
    Params_Assert_Equal_float;


    typedef struct 
    Params_Assert_Equal_double
    {
        double input1;
        double input2;
        double tolerance;
    }
    Params_Assert_Equal_double;

    typedef struct 
    Params_Assert_EqualArray_int
    {
        int *input1;
        int *input2;
        int  length;
    }
    Params_Assert_EqualArray_int;

    typedef struct 
    Params_Assert_EqualArray_float
    {
        float *input1;
        float *input2;
        int    length;
        float  tolerance;
    }
    Params_Assert_EqualArray_float;

    typedef struct 
    Params_Assert_EqualArray_double
    {
        double *input1;
        double *input2;
        int     length;
        double  tolerance;
    }
    Params_Assert_EqualArray_double;

    //--------------------------------------
    // parameters for matrix_column_equal_vector_float()
    //--------------------------------------
    typedef struct 
    Params_Assert_MatrixColumnEqualVector_float
    {
        float *matrix;
        int    matrix_row_count;
        int    matrix_column_count;
        int    row_id_eon;
        int    row_id_end;
        int    column_id;
        float *vector;
        int    vector_length;
        float  tolerance;
    }
    Params_Assert_MatrixColumnEqualVector_float;

    //--------------------------------------
    // parameters for equal_matrix_float()
    //--------------------------------------
    typedef struct 
    Params_Assert_EqualMatrix_float
    {
        float *matrix1;
        int    matrix1_row_count;
        int    matrix1_column_count;
        int    matrix1_row_id_eon;
        int    matrix1_row_id_end;
        int    matrix1_column_id_eon;
        int    matrix1_column_id_end;
        float *matrix2;
        int    matrix2_row_count;
        int    matrix2_column_count;
        int    matrix2_row_id_eon;
        int    matrix2_row_id_end;
        int    matrix2_column_id_eon;
        int    matrix2_column_id_end;
        float  tolerance;
    }
    Params_Assert_EqualMatrix_float;

    typedef struct 
    module_Assert
    {
        void (*hi)(void);
        bool (*equal)(Params_Assert_Equal, bool *error_found_OUT);
        bool (*equal_int)(Params_Assert_Equal_int);
        bool (*equal_float)(Params_Assert_Equal_float);
        bool (*equal_double)(Params_Assert_Equal_double);
        bool (*equal_array_int)(Params_Assert_EqualArray_int);
        bool (*equal_array_float)(Params_Assert_EqualArray_float);
        bool (*equal_array_double)(Params_Assert_EqualArray_double);
        bool (*equal_matrix_float)(Params_Assert_EqualMatrix_float);
        bool (*matrix_column_equal_vector_float)(Params_Assert_MatrixColumnEqualVector_float);
    }
    module_Assert;

    module_Assert
    import_Assert(void);

    #endif

    /*****************************************************/


#[module_Assert.dep.c](#module_Assert.dep.c "save:")

    _"LICENSE"

    #include "module_Assert.h"

    #include <stdio.h>
    #include <stdlib.h>
    #include <stdbool.h>
    #include <math.h>
    #include <assert.h>

#[module_Assert.c](#module_Assert.c "save:")

    _"LICENSE"

    #include "module_Assert.dep.c"

    _"module_Assert worker functions"

    _"module_Assert Frontend functions"

    module_Assert
    import_Assert(void)
    {
        module_Assert
        obj = {
            .hi = hi,
            .equal = equal,
            .equal_int = equal_int,
            .equal_float = equal_float,
            .equal_double = equal_double,
            .equal_array_int = equal_array_int,
            .equal_array_float = equal_array_float,
            .equal_array_double = equal_array_double,
            .equal_matrix_float = equal_matrix_float,
            .matrix_column_equal_vector_float = matrix_column_equal_vector_float,
        };

        return obj;
    }



## module_Assert Frontend functions

Frontend function equal
    bool
    equal(Params_Assert_Equal params, bool *p_ErrorFound)
    {
        *p_ErrorFound = false;

        bool output = true;

        module_Type
        Type = import_Type();


        if (Type.same(params.type, Type.Float))
        {
            puts("(from Assert.equal) your input is float");

            // cast back to float pointer from void pointer
            float *input1 = (float*) params.input1;
            float *input2 = (float*) params.input2;

            // now convert to double
            Params_Assert_Equal_double
            inputs = {
                .input1 = (double) *input1,
                .input2 = (double) *input2,
                .tolerance = params.tolerance,
            };

            output = equal_double(inputs);
        }
        else if (Type.same(params.type, Type.Double))
        {
            puts("(from Assert.equal) your input is double");

            // cast back to float pointer from void pointer
            double *input1 = (double*) params.input1;
            double *input2 = (double*) params.input2;

            // now convert to double
            Params_Assert_Equal_double
            inputs = {
                .input1 = *input1,
                .input2 = *input2,
                .tolerance = params.tolerance,
            };

            output = equal_double(inputs);
        }
        else
        {
            fprintf(stderr, "(from module_Assert.equal()) "
                "input data type not allowed.\n");
            *p_ErrorFound = true;
        }

        return output;
    }




## module_Assert worker functions

    static void
    hi(void)
    {
        puts("Greetings from module_Assert!");
    }

Worker function equal_int
    //-------------------------------
    // equal_int()
    //-------------------------------

    static bool
    equal_int(Params_Assert_Equal_int params)
    {
        int input1 = params.input1;
        int input2 = params.input2;
        
        bool output = false;
        if (input1 == input2) {output = true;}

        if (output == true)
        {
            printf("\n((( success! )))\n"
                "input1 = %d\n"
                "input2 = %d\n",
                input1, input2);
        }
        else
        {
            printf("\n=== [[[ failed!!! ]]] ===\n"
                "input1 = %d\n"
                "input2 = %d\n",
                input1, input2);
        }
        return output;
    }
Worker function equal_float
    //-------------------------------
    // equal_float()
    //-------------------------------

    static bool
    equal_float(Params_Assert_Equal_float params)
    {
    	bool output = false;
    	float delta = fabs(params.input1 - params.input2);
    	if (delta < params.tolerance) {output = true;}

        if (output == true)
        {
            printf("\n((( success! )))\n"
                "input1 = %e\n"
                "input2 = %e\n",
                params.input1, params.input2);
        }
        else
        {
            printf("\n=== [[[ failed!!! ]]] ===\n"
                "input1 = %e\n"
                "input2 = %e\n",
                params.input1, params.input2);
        }
    	return output;
    }


Worker function equal_double
    //-------------------------------
    // equal_double()
    //-------------------------------

    static bool
    equal_double(Params_Assert_Equal_double params)
    {
        bool output = false;
        double delta = fabs(params.input1 - params.input2);
        if (delta < params.tolerance) {output = true;}

        if (output == true)
        {
            printf("\n((( success! )))\n"
                "input1 = %e\n"
                "input2 = %e\n",
                params.input1, params.input2);
        }
        else
        {
            printf("\n=== [[[ failed!!! ]]] ===\n"
                "input1 = %e\n"
                "input2 = %e\n",
                params.input1, params.input2);
        }

        return output;
    }



Worker function equal_array_int
    static bool 
    equal_array_int(Params_Assert_EqualArray_int params)
    {
        int *v1 = params.input1;
        int *v2 = params.input2;

        if (params.length <= 0)
        {
            puts("\n===============================");
            puts("    [[[[ WARNING!!! ]]]]       ");
            puts("(from equal_array_int)");
            puts("Input parameter params.length must be positive");
            printf("Your params.length == %d\n", params.length);
            puts("===============================\n");
            return false;
        }

        for (int i = 0; i < params.length; i++)
        {
            if ( v1[i] == v2[i] ) 
            {
                printf(
                    "correct! [%d]: %d  %d\n",
                    i, v1[i], v2[i]);
            }
            else 
            { 
                fprintf(stderr, 
                    "\n=== [[[ failed!!! ]]] ===\n"
                    "Item [%d] NOT EQUAL!!!\n"
                    "input1[%d] = %d\n"
                    "input2[%d] = %d\n",
                    i, i, v1[i], i, v2[i]);
                return false; 
            }
        } 
        puts("((( success! ))) all items are equal!"); 
        return true; 
    }

Worker function equal_array_float
    static bool 
    equal_array_float(Params_Assert_EqualArray_float params)
    {
        float *v1 = params.input1;
        float *v2 = params.input2;

        if (params.length <= 0)
        {
            puts("\n===============================");
            puts("    [[[[ WARNING!!! ]]]]       ");
            puts("(from equal_array_float)");
            puts("Input parameter params.length must be positive");
            printf("Your params.length == %d\n", params.length);
            puts("===============================\n");
            return false;
        }

        for (int i = 0; i < params.length; i++)
        {
            float delta = fabs(v1[i] - v2[i]);
            if ( delta <= params.tolerance ) 
            {
                printf(
                    "correct! [%d]: %.3e  %.3e\n",
                    i, v1[i], v2[i]);
            }
            else 
            { 
                fprintf(stderr, 
                    "\n=== [[[ failed!!! ]]] ===\n"
                    "Item [%d] NOT EQUAL!!!\n"
                    "input1[%d] = %e\n"
                    "input2[%d] = %e\n",
                    i, i, v1[i], i, v2[i]);
                return false; 
            }
        } 
        puts("((( success! ))) all items are equal!"); 
        return true; 
    }


Worker function equal_array_double
    static bool 
    equal_array_double(Params_Assert_EqualArray_double params)
    {
        double *v1 = params.input1;
        double *v2 = params.input2;
        
        if (params.length <= 0)
        {
            puts("\n===============================");
            puts("    [[[[ WARNING!!! ]]]]       ");
            puts("(from equal_array_float)");
            puts("Input parameter params.length must be positive");
            printf("Your params.length == %d\n", params.length);
            puts("===============================\n");
            return false;
        }

        for (int i = 0; i < params.length; i++)
        {
            double delta = fabs(v1[i] - v2[i]);
            if ( delta > params.tolerance ) 
            { 
                fprintf(stderr, 
                    "\n=== [[[ failed!!! ]]] ===\n"
                    "NOT EQUAL!!!\n"
                    "input1[%d] = %e\n"
                    "input2[%d] = %e\n",
                    i, v1[i], i, v2[i]);
                return false; 
            }
        } 
        puts("((( success! ))) all items are equal!"); 
        return true; 
    }

Worker function matrix_column_equal_vector_float. 
This function can check whether a matrix column 
(a range of selected rows) equal to a given vector.
    static bool
    matrix_column_equal_vector_float(Params_Assert_MatrixColumnEqualVector_float params)
    {
        float *A = params.matrix;
        int i_ColumnCount = params.matrix_column_count;
        int row_id_eon = params.row_id_eon;
        int row_id_end = params.row_id_end;
        int column_id  = params.column_id;
        float *vector = params.vector;
        int vector_length = params.vector_length;
        int vector_index_upperbound = vector_length - 1;
        int tolerance = params.tolerance;

        assert(params.row_id_end < params.matrix_row_count);

        for (int i = row_id_eon; i <= row_id_end; i++)
        {
            int index = (i * i_ColumnCount) + column_id;
            float matrix_item = A[index];

            /***********************************/
            /* check vector index within bound */
            /***********************************/
            int module_Assert_VectorIndex = i - row_id_eon;
            assert(module_Assert_VectorIndex <= vector_index_upperbound);
            /******************************/

            float vector_item = *vector;

            float delta = fabs(matrix_item - vector_item);

            if (delta <= tolerance)
            {
                printf(
                    "correct! matrix[%d, %d]: %.3e  vector[%d]: %.3e\n",
                     i,
                     column_id,
                     matrix_item,
                     i - row_id_eon,
                     vector_item);
            }
            else
            {
                int vector_index = i - row_id_eon;
                puts("\n=== [[[ failed!!! ]]] ===\n");
                puts("NOT EQUAL!!!");
                fprintf(stderr, "matrix[%d, %d] = %.3e\n",
                    i, column_id, matrix_item);
                fprintf(stderr, "vector[%d]    = %.3e\n",
                    vector_index, vector_item);
                return false; 
            }
            vector++;
        }
        return true;
    }


Worker function equal_matrix_float. 
This function can check whether a matrix column 
(a range of selected rows) equal to a given vector.
    static bool
    equal_matrix_float(Params_Assert_EqualMatrix_float params)
    {
        int matrix1_row_range_length =
            params.matrix1_row_id_end - params.matrix1_column_id_eon + 1;

        int matrix1_column_range_length = 
            params.matrix1_column_id_end - params.matrix1_column_id_eon + 1;

        int matrix2_row_range_length = 
            params.matrix2_row_id_end - params.matrix2_row_id_eon + 1;

        int matrix2_column_range_length =
            params.matrix2_column_id_end - params.matrix2_column_id_eon + 1;


        assert(matrix1_row_range_length == matrix2_row_range_length);
        assert(matrix1_column_range_length == matrix2_column_range_length);


        for (int i = 0; i < matrix1_row_range_length; i++)
        {
            int row_id1 = i + params.matrix1_row_id_eon;
            int row_id2 = i + params.matrix2_row_id_eon;

            for (int j= 0; j < matrix1_column_range_length; j++)
            {
                int column_id1 = j + params.matrix1_column_id_eon;
                int column_id2 = j + params.matrix2_column_id_eon;

                int index1 = (row_id1 * params.matrix1_column_count) + column_id1;
                int index2 = (row_id2 * params.matrix2_column_count) + column_id2;

                float matrix1_item = params.matrix1[index1];
                float matrix2_item = params.matrix2[index2];

                float delta = fabs(matrix1_item - matrix2_item);

                if (delta <= params.tolerance)
                {
                    printf(
                        "correct! matrix1[%d, %d]: %.3e  matrix2[%d, %d]: %.3e\n",
                         row_id1,
                         column_id1,
                         matrix1_item,
                         row_id2,
                         column_id2,
                         matrix2_item);
                }
                else
                {
                    puts("\n=== [[[ failed!!! ]]] ===\n");
                    puts("NOT EQUAL!!!");
                    fprintf(stderr, "matrix1[%d, %d] = %.3e\n",
                        row_id1, column_id1, matrix1_item);
                    fprintf(stderr, "matrix2[%d, %d] = %.3e\n",
                        row_id2, column_id2, matrix2_item);
                    return false; 
                }
            }
        }
        return true;
    }



