/**********************************************************
 Copyright (c) 2015 YUHANG WANG 
 ALL RIGHTS RESERVED

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
************************************************************/

#include "test_module_Assert.dep.c"

static bool
test_hi(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return false; }

	puts("\nTest Assert.hi()\n");

	module_Assert
	Assert = import_Assert();

	Assert.hi();
    return true;
}

static bool
test_equal1(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return false; }

    puts("\nTest equal()\n");

    module_Assert
    Assert = import_Assert();

    module_Type
    Type = import_Type();

    float input1 = 1.00;
    float input2 = 1.01;
    float *p_Input1 = &input1;
    float *p_Input2 = &input2;
    double tolerance = 1.E-5;

    MyTypeChoice
    type = Type.Float;

    Params_Assert_Equal
    params = {
        .input1 = p_Input1,
        .input2 = p_Input2,
        .tolerance = tolerance,
        .type = type,
    };

    bool error_found = false;
    bool answer = Assert.equal(params, &error_found);
    bool key    = false;

    printf("answer = %d; key = %d\n", answer, key);
    assert(answer == key);
    return true;
}

static bool
test_equal2(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return false; }

    puts("\nTest equal()\n");

    module_Assert
    Assert = import_Assert();

    module_Type
    Type = import_Type();

    float input1 = 1.0;
    float input2 = 1.0;
    float *p_Input1 = &input1;
    float *p_Input2 = &input2;
    double tolerance = 1.E-5;

    MyTypeChoice
    type = Type.Float;

    Params_Assert_Equal
    params = {
        .input1 = p_Input1,
        .input2 = p_Input2,
        .tolerance = tolerance,
        .type = type,
    };

    bool error_found = false;
    bool answer = Assert.equal(params,&error_found);
    bool key    = true;

    printf("answer = %d; key = %d\n", answer, key);
    assert(answer == key);
    puts("SUCCESS! (expected fail)");
    return true;
}

static bool
test_equal_error_found(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return false; }

    puts("\nTest equal()'s .error_found tag\n");

    module_Assert
    Assert = import_Assert();

    module_Type
    Type = import_Type();

    int input1 = 1;
    int input2 = 1;
    int *p_Input1 = &input1;
    int *p_Input2 = &input2;
    double tolerance = 1.E-5;

    MyTypeChoice
    type = Type.Int;

    Params_Assert_Equal
    params = {
        .input1 = p_Input1,
        .input2 = p_Input2,
        .tolerance = tolerance,
        .type = type,
    };

    bool error_found = false;
    Assert.equal(params, &error_found);
    
    bool expected    = true;

    printf("error_found = %d; expected = %d\n",
        error_found, expected);
    assert(error_found == expected);
    return true;
}

static bool
test_equal_int1(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return false; }

    puts("\ntest_equal_int1()\n");

    module_Assert
    Assert = import_Assert();

    int input1 = 1;
    int input2 = 1;

    Params_Assert_Equal_int 
    params_assert = {
        .input1 = input1,
        .input2 = input2,
    };
    bool answer = Assert.equal_int(params_assert);

    bool expect = true;
    printf("answer = %d expected = %d\n", answer,expect);
    assert(answer == expect);
    return true;
}

static bool
test_equal_int2(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return false; }

    puts("\ntest_equal_int2()\n");

    module_Assert
    Assert = import_Assert();

    int input1 = 1;
    int input2 = 0;
    
    Params_Assert_Equal_int 
    params_assert = {
        .input1 = input1,
        .input2 = input2,
    };
    bool answer = Assert.equal_int(params_assert);
    
    bool expect = false;
    printf("answer = %d expected = %d\n", answer,expect);
    assert(answer == expect);
    puts("SUCCESS! (expected fail)");
    return true;
}

static bool
test_equal_float1(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return false; }

    puts("\ntest_equal_float1()\n");

    module_Assert
    Assert = import_Assert();

    float input1 = 1.;
    float input2 = 1.;
    float tolerance = 1.0E-5;
    Params_Assert_Equal_float
    params = {
        .input1 = input1,
        .input2 = input2,
        .tolerance = tolerance,
    };

    bool answer = Assert.equal_float(params);
    bool expect = true;
    printf("answer = %d expected = %d\n", answer,expect);
    assert(answer == expect);
    return true;
}

static bool
test_equal_float2(bool bool_RunTest)
{

    if ( ! bool_RunTest ) { return false; }

    puts("\ntest_equal_float2()\n");

    module_Assert
    Assert = import_Assert();

    float input1 = 1.;
    float input2 = 1.1;
    float tolerance = 1.0E-5;
    Params_Assert_Equal_float
    params = {
        .input1 = input1,
        .input2 = input2,
        .tolerance = tolerance,
    };

    bool answer = Assert.equal_float(params);
    bool expect = false;
    printf("answer = %d expected = %d\n", answer,expect);
    assert(answer == expect);
    puts("SUCCESS! (expected fail)");
    return true;
}

static bool
test_equal_double1(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return false; }

    puts("\ntest_equal_double1()\n");

    module_Assert
    Assert = import_Assert();

    double input1 = 1.;
    double input2 = 1.;
    double tolerance = 1.0E-5;
    Params_Assert_Equal_double
    params = {
        .input1 = input1,
        .input2 = input2,
        .tolerance = tolerance,
    };

    bool answer = Assert.equal_double(params);
    bool expect = true;
    printf("answer = %d expected = %d\n", answer,expect);
    assert(answer == expect);
    return true;
}

static bool
test_equal_double2(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return false; }

    puts("\ntest_equal_double2()\n");

    module_Assert
    Assert = import_Assert();

    double input1 = 1.;
    double input2 = 1.1;
    double tolerance = 1.0E-5;
    Params_Assert_Equal_double
    params = {
        .input1 = input1,
        .input2 = input2,
        .tolerance = tolerance,
    };

    bool answer = Assert.equal_double(params);
    bool expect = false;
    printf("answer = %d expected = %d\n", answer,expect);
    assert(answer == expect);
    puts("SUCCESS! (expected fail)");
    return true;
}

static bool
test_equal_array_int1(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return false; }

    puts("\ntest_equal_array_int1()\n");

    module_Assert
    Assert = import_Assert();

    int   length   = 2;
    int input1[] = {1, 0};
    int input2[] = {1, 0};

    Params_Assert_EqualArray_int
    params = {
        .input1 = input1,
        .input2 = input2,
        .length = length,
    };

    bool answer = Assert.equal_array_int(params);
    bool expect = true;

    printf("answer = %d; expected = %d\n",
        answer, expect);
    assert(answer == expect);
    return true;
}

static bool
test_equal_array_int2(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return false; }

    puts("\ntest_equal_array_int1()\n");

    module_Assert
    Assert = import_Assert();

    int   length   = 2;
    int input1[] = {1, 1};
    int input2[] = {1, 0};

    Params_Assert_EqualArray_int
    params = {
        .input1 = input1,
        .input2 = input2,
        .length = length,
    };

    bool answer = Assert.equal_array_int(params);
    bool expect = false;

    printf("answer = %d; expected = %d\n",
        answer, expect);
    assert(answer == expect);
    puts("SUCCESS! (expected fail)");
    return true;
}

static bool
test_equal_array_float1(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return false; }

    puts("\ntest_equal_array_float1()\n");

    module_Assert
    Assert = import_Assert();

    int   length   = 2;
    float input1[] = {1., 0.};
    float input2[] = {1., 0.};
    float tolerance = 1.0E-5;

    Params_Assert_EqualArray_float
    params = {
        .input1 = input1,
        .input2 = input2,
        .length = length,
        .tolerance = tolerance,
    };

    bool answer = Assert.equal_array_float(params);
    bool expect = true;

    printf("answer = %d; expected = %d\n",
        answer, expect);
    assert(answer == expect);
    return true;
}

static bool
test_equal_array_float2(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return false; }

    puts("\ntest_equal_array_float2()\n");

    module_Assert
    Assert = import_Assert();

    int   length   = 3;
    float input1[] = {1., 1., 1.2};
    float input2[] = {1., 1., 1.3};
    float tolerance = 1.0E-5;

    Params_Assert_EqualArray_float
    params = {
        .input1 = input1,
        .input2 = input2,
        .length = length,
        .tolerance = tolerance,
    };

    bool answer = Assert.equal_array_float(params);
    bool expect = false;

    printf("answer = %d; expected = %d\n",
        answer, expect);
    assert(answer == expect);
    puts("SUCCESS! (expected fail)");
    return true;
}

static bool
test_equal_array_double1(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return false; }

    puts("\ntest_equal_array_double1()\n");

    module_Assert
    Assert = import_Assert();

    int   length   = 2;
    double input1[] = {1., 0.};
    double input2[] = {1., 0.};
    double tolerance = 1.0E-5;

    Params_Assert_EqualArray_double
    params = {
        .input1 = input1,
        .input2 = input2,
        .length = length,
        .tolerance = tolerance,
    };

    bool answer = Assert.equal_array_double(params);
    bool expect = true;

    printf("answer = %d; expected = %d\n",
        answer, expect);
    assert(answer == expect);
    return true;
}

static bool
test_equal_array_double2(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return false; }

    puts("\ntest_equal_array_double2()\n");

    module_Assert
    Assert = import_Assert();

    int   length   = 2;
    double input1[] = {1., 1.};
    double input2[] = {1., 0.};
    double tolerance = 1.0E-5;

    Params_Assert_EqualArray_double
    params = {
        .input1 = input1,
        .input2 = input2,
        .length = length,
        .tolerance = tolerance,
    };

    bool answer = Assert.equal_array_double(params);
    bool expect = false;

    printf("answer = %d; expected = %d\n",
        answer, expect);
    assert(answer == expect);
    puts("SUCCESS! (expected fail)");
    return true;
}

static bool
test_equal_array_float_error_message(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return false; }

    puts("\ntest_equal_array_float_error_message()\n");

    module_Assert
    Assert = import_Assert();

    // int   length   = 2;
    float input1[] = {1., 1.};
    float input2[] = {1., 0.};
    float tolerance = 1.0E-5;

    Params_Assert_EqualArray_float
    params = {
        .input1 = input1,
        .input2 = input2,
        .length = 0, // << test error message for length == 0
        .tolerance = tolerance,
    };

    bool answer = Assert.equal_array_float(params);
    bool expect = false;

    printf("answer = %d; expected = %d\n",
        answer, expect);
    return true;
}

static bool
test_matrix_column_equal_vector_float(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return false; }

    puts("\ntest_matrix_column_equal_vector()\n");

    module_Assert
    Assert = import_Assert();

    //------------------------------------
    // define matrix and the vector
    //------------------------------------
    int i_RowCount = 3;
    int i_ColumnCount = 3;
    float A[9] =
    {
     1., 0., 1.,
     0., 1., 1.,
     0., 1., 1.,
    };
    float vector1[3] = {1., 0., 0.};
    float vector2[3] = {0.1, 1., 1.};
    float vector3[3] = {1., 1., 9.};
    float *vectors[3];
    vectors[0] = &vector1[0];
    vectors[1] = &vector2[0];
    vectors[2] = &vector3[0];

    bool expected[3] = {true, true, false};

    for (int j = 0; j < i_ColumnCount; j++)
    {
        float *vector = vectors[j];
        Params_Assert_MatrixColumnEqualVector_float
        params = {
            .matrix = A,
            .matrix_row_count = i_RowCount,
            .matrix_column_count = i_ColumnCount,
            .row_id_eon = j,
            .row_id_end = i_RowCount - 1,
            .column_id = j,
            .vector = &vector[j],
            .vector_length = i_RowCount - j,
            .tolerance = 1.E-5,
        };
        bool answer = Assert.matrix_column_equal_vector_float(params);
        bool expect = expected[j];

        printf("answer = %d; expected = %d\n",
            answer, expect);
        assert(answer == expect);
    }
    return true;
}

static bool
test_equal_matrix_float(bool bool_RunTest)
{
    if ( ! bool_RunTest ) { return false; }

    puts("\ntest_equal_matrix_float()\n");

    module_Assert
    Assert = import_Assert();

    //------------------------------------
    // define matrix and the vector
    //------------------------------------
    int i_RowCount = 3;
    int i_ColumnCount = 3;
    float A[9] =
    {
     1., 0., 1.,
     0., 1., 1.,
     0., 1., 1.,
    };

    float B[9] =
    {
     1., 9., 1.,
     0., 1., 1.,
     0., 1., 9.,
    };

    bool expected[3] = {true, true, false};

    for (int j = 0; j < i_ColumnCount; j++)
    {
        Params_Assert_EqualMatrix_float
        params = {
            .matrix1 = A,
            .matrix1_row_count = i_RowCount,
            .matrix1_column_count = i_ColumnCount,
            .matrix1_row_id_eon = j,
            .matrix1_row_id_end = i_RowCount - 1,
            .matrix1_column_id_eon = j,
            .matrix1_column_id_end = j,
            .matrix2 = B,
            .matrix2_row_count = i_RowCount,
            .matrix2_column_count = i_ColumnCount,
            .matrix2_row_id_eon = j,
            .matrix2_row_id_end = i_RowCount - 1,
            .matrix2_column_id_eon = j,
            .matrix2_column_id_end = j,
            .tolerance = 1.E-5,
        };
        bool answer = Assert.equal_matrix_float(params);
        bool expect = expected[j];

        printf("answer = %d; expected = %d\n",
            answer, expect);
        assert(answer == expect);
    }
    return true;
}

int
main(void)
{

	puts("-------------------------");
	test_hi(false);
    puts("-------------------------");
    test_equal1(false);
    puts("-------------------------");
    test_equal2(false);
    puts("-------------------------");
    test_equal_error_found(false);
    puts("-------------------------");
    test_equal_int1(false);
    puts("-------------------------");
    test_equal_int2(false);
    puts("-------------------------");
    test_equal_float1(false);
    puts("-------------------------");
    test_equal_float2(false);
    puts("-------------------------");
    test_equal_double1(false);
    puts("-------------------------");
    test_equal_double2(false);
    puts("-------------------------");
    test_equal_array_int1(true);
    puts("-------------------------");
    test_equal_array_int2(true);
    puts("-------------------------");
    test_equal_array_float1(false);
    puts("-------------------------");
    test_equal_array_float2(false);
    puts("-------------------------");
    test_equal_array_double1(false);
    puts("-------------------------");
    test_equal_array_double2(false);
    puts("-------------------------");
    test_equal_array_float_error_message(false);
    puts("-------------------------");
    test_matrix_column_equal_vector_float(false);
    puts("-------------------------");
    test_equal_matrix_float(false);
	puts("-------------------------");

	return 0;
}