/**********************************************************
 Copyright (c) 2015 YUHANG WANG 
 ALL RIGHTS RESERVED

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
************************************************************/

/*****************************************************/
#ifndef MODULE_ASSERT_H
#define MODULE_ASSERT_H

#include "module_Assert.dep.h"

typedef struct 
Params_Assert_Equal
{
    void *input1;
    void *input2;
    MyTypeChoice type;
    double tolerance;
}
Params_Assert_Equal;

typedef struct 
Params_Assert_Equal_int
{
    int input1;
    int input2;
}
Params_Assert_Equal_int;

typedef struct 
Params_Assert_Equal_float
{
    float input1;
    float input2;
    float tolerance;
}
Params_Assert_Equal_float;

typedef struct 
Params_Assert_Equal_double
{
    double input1;
    double input2;
    double tolerance;
}
Params_Assert_Equal_double;

typedef struct 
Params_Assert_EqualArray_int
{
    int *input1;
    int *input2;
    int  length;
}
Params_Assert_EqualArray_int;

typedef struct 
Params_Assert_EqualArray_float
{
    float *input1;
    float *input2;
    int    length;
    float  tolerance;
}
Params_Assert_EqualArray_float;

typedef struct 
Params_Assert_EqualArray_double
{
    double *input1;
    double *input2;
    int     length;
    double  tolerance;
}
Params_Assert_EqualArray_double;

//--------------------------------------
// parameters for matrix_column_equal_vector_float()
//--------------------------------------
typedef struct 
Params_Assert_MatrixColumnEqualVector_float
{
    float *matrix;
    int    matrix_row_count;
    int    matrix_column_count;
    int    row_id_eon;
    int    row_id_end;
    int    column_id;
    float *vector;
    int    vector_length;
    float  tolerance;
}
Params_Assert_MatrixColumnEqualVector_float;

//--------------------------------------
// parameters for equal_matrix_float()
//--------------------------------------
typedef struct 
Params_Assert_EqualMatrix_float
{
    float *matrix1;
    int    matrix1_row_count;
    int    matrix1_column_count;
    int    matrix1_row_id_eon;
    int    matrix1_row_id_end;
    int    matrix1_column_id_eon;
    int    matrix1_column_id_end;
    float *matrix2;
    int    matrix2_row_count;
    int    matrix2_column_count;
    int    matrix2_row_id_eon;
    int    matrix2_row_id_end;
    int    matrix2_column_id_eon;
    int    matrix2_column_id_end;
    float  tolerance;
}
Params_Assert_EqualMatrix_float;

typedef struct 
module_Assert
{
    void (*hi)(void);
    bool (*equal)(Params_Assert_Equal, bool *error_found_OUT);
    bool (*equal_int)(Params_Assert_Equal_int);
    bool (*equal_float)(Params_Assert_Equal_float);
    bool (*equal_double)(Params_Assert_Equal_double);
    bool (*equal_array_int)(Params_Assert_EqualArray_int);
    bool (*equal_array_float)(Params_Assert_EqualArray_float);
    bool (*equal_array_double)(Params_Assert_EqualArray_double);
    bool (*equal_matrix_float)(Params_Assert_EqualMatrix_float);
    bool (*matrix_column_equal_vector_float)(Params_Assert_MatrixColumnEqualVector_float);
}
module_Assert;

module_Assert
import_Assert(void);

#endif

/*****************************************************/