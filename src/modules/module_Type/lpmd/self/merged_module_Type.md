# LICENSE
    /**********************************************************
     Copyright (c) 2015 YUHANG WANG 
     ALL RIGHTS RESERVED

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
    ************************************************************/



# LICENSE
    /**********************************************************
     Copyright (c) 2015 YUHANG WANG 
     ALL RIGHTS RESERVED

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
    ************************************************************/



# Module module_Type
This module provides an enum-like representation of the 
data types but using struct.

#[module_Type.dep.h](#module_Type.dep.h "save:")

    _"LICENSE"

    #include <stdbool.h>

#[module_Type.h](#module_Type.h "save:")

    _"LICENSE"

    #ifndef MODULE_TYPE_H
    #define MODULE_TYPE_H

    #include "module_Type.dep.h"

The purpose of MyTypeChoice is to let user 
to choose one type from the types provided by module_Type.
The benefit is that internal representation will 
consistent.

    typedef struct 
    MyTypeChoice
    {
        unsigned long int value;
    }
    MyTypeChoice;

    typedef struct 
    module_Type
    {
        void (*hi)(void);
        bool (*same)(MyTypeChoice, MyTypeChoice);
    	MyTypeChoice Int;
        MyTypeChoice Float;
        MyTypeChoice Double; 
    }
    module_Type;

    module_Type
    import_Type(void);

    #endif



#[module_Type.dep.c](#module_Type.dep.c "save:")

    _"LICENSE"

    #include "module_Type.h"
    #include <stdio.h>

#[module_Type.c](#module_Type.c "save:")

    _"LICENSE"
    
    #include "module_Type.dep.c"

    static void 
    hi(void)
    {
    	printf("Greetings from module_Type!\n");
    }

    bool
    same(MyTypeChoice type1, MyTypeChoice type2)
    {
        if ( type1.value == type2.value ) { return true; }
        else { return false; }
    }

    module_Type
    import_Type(void)
    {
        // use C99 compound literal feature.
    	module_Type 
    	obj = {
    		.hi       = hi,
            .same  = same,
    		.Int      = (MyTypeChoice){0L},
    		.Float    = (MyTypeChoice){1L},
            .Double   = (MyTypeChoice){2L},
    	};

    	return obj;
    }

    

