# LICENSE
    /**********************************************************
     Copyright (c) 2015 YUHANG WANG 
     ALL RIGHTS RESERVED

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
    ************************************************************/



# LICENSE
    /**********************************************************
     Copyright (c) 2015 YUHANG WANG 
     ALL RIGHTS RESERVED

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
    ************************************************************/



# Test module_Type
This is the test suite for module_Type.

#[test_module_Type.dep.h](#test_module_Type.dep.h "save:")

    _"LICENSE"

#[test_module_Type.h](#test_module_Type.h "save:")

    _"LICENSE"

    #include "test_module_Type.dep.h"


#[test_module_Type.dep.c](#test_module_Type.dep.c "save:")

    _"LICENSE"

    #include "test_module_Type.h"
    #include "module_Type.h"
    #include <stdio.h>
    #include <stdbool.h>
    #include <assert.h>


#[test_module_Type.c](#test_module_Type.c "save:")

    _"LICENSE"

    #include "test_module_Type.dep.c"

    _"test_module_Type all tests"

    int
    main(void)
    {
        puts("--------------------------");
        test_hi();
        puts("--------------------------");
        test_compare();
        puts("--------------------------");
        return 0;
    }


#test_module_Type all tests
Test hi
    static void
    test_hi(void)
    {
        puts("\nTest Type.hi()\n");

        module_Type
        Type = import_Type();

        Type.hi();
    }


Test function same.
    static void
    test_compare(void)
    {
        puts("\nTest Type.same()\n");
        module_Type
        Type = import_Type();


        MyTypeChoice
        type1 = Type.Float;

        MyTypeChoice
        type2 = Type.Float;

        bool answer = Type.same(type1, type2);
        bool key    = true;

        printf("answer = %d; key = %d\n", answer, key);
        assert(answer == key);
    }



