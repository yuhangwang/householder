# Test module_Type
This is the test suite for module_Type.

#[test_module_Type.dep.h](#test_module_Type.dep.h "save:")

    _"LICENSE"

#[test_module_Type.h](#test_module_Type.h "save:")

    _"LICENSE"

    #include "test_module_Type.dep.h"


#[test_module_Type.dep.c](#test_module_Type.dep.c "save:")

    _"LICENSE"

    #include "test_module_Type.h"
    #include "module_Type.h"
    #include <stdio.h>
    #include <stdbool.h>
    #include <assert.h>


#[test_module_Type.c](#test_module_Type.c "save:")

    _"LICENSE"

    #include "test_module_Type.dep.c"

    _"test_module_Type all tests"

    int
    main(void)
    {
        puts("--------------------------");
        test_hi();
        puts("--------------------------");
        test_compare();
        puts("--------------------------");
        return 0;
    }


#test_module_Type all tests
Test hi
    static void
    test_hi(void)
    {
        puts("\nTest Type.hi()\n");

        module_Type
        Type = import_Type();

        Type.hi();
    }


Test function same.
    static void
    test_compare(void)
    {
        puts("\nTest Type.same()\n");
        module_Type
        Type = import_Type();


        MyTypeChoice
        type1 = Type.Float;

        MyTypeChoice
        type2 = Type.Float;

        bool answer = Type.same(type1, type2);
        bool key    = true;

        printf("answer = %d; key = %d\n", answer, key);
        assert(answer == key);
    }



