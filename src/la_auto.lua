local la = require("module_la_shortcut")

local Target = la.create_target()


Target.LA_SETUP = {
	SHOW_HEADER = false,
}

-- note: Lua will replace '.' by the path separator '/'
Target:include('config.La_define')
function Target:hi()
	print("Hello world! from hi()")
	return "print(hello)"
end

Target.tests = {}


-- -- module_Type
-- Target.tests.module_Type = la.require(
-- 	'config.La_test_module_Type')

-- -- module_Assert
-- Target.tests.module_Assert = la.require(
-- 	'config.La_test_module_Assert')

-- -- module_MathSign
-- Target.tests.module_MathSign = la.require(
-- 	'config.La_test_module_MathSign')

-- -- module_Vector
-- Target.tests.module_Vector = la.require('config.La_test_module_Vector')

-- -- module_Matrix
-- Target.tests.module_Matrix = la.require('config.La_test_module_Matrix')

-- -- module_MpiJob
-- Target.tests.module_MpiJob = la.require('config.La_test_module_MpiJob')

-- -- module_ReadText
-- Target.tests.module_ReadText = la.require('config.La_test_module_ReadText')

--module_HouseholderFactorize
Target.tests.HouseholderFactorize = la.require(
	'config.La_test_module_HouseholderFactorize')


function Target:test()
	-- self.tests.module_Type:all()
	-- self.tests.module_Assert:all()
	-- self.tests.module_MathSign:all()
	-- self.tests.module_Vector:all()
	-- self.tests.module_Matrix:all()
	-- self.tests.module_MpiJob:all()
	-- self.tests.module_ReadText:all()
	self.tests.HouseholderFactorize:all()
end




return Target

