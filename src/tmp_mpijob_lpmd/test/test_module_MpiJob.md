# Test module_MpiJob 


#[test_module_MpiJob.dep.h](#test_module_MpiJob.dep.h "save:")

    _"LICENSE"

#[test_module_MpiJob.h](#test_module_MpiJob.h "save:")
    
    _"LICENSE"

    #include "test_module_MpiJob.dep.h"


#[test_module_MpiJob.dep.c](#test_module_MpiJob.dep.c "save:")
        
    _"LICENSE" 

    #include "test_module_MpiJob.h"
    #include "module_MpiJob.h"
    #include "module_Assert.h"
    #include "module_Vector.h"
    #include "mpi.h"
    #include <stdbool.h>
    #include <stdio.h>
    #include <assert.h>


#[test_module_MpiJob.c](#test_module_MpiJob.c "save:")

    _"LICENSE"

    #include "test_module_MpiJob.dep.c"

    _"test_module_MpiJob all tests"

    int
    main (int i_ArgumentCount, char *c_ArgumentArray[])
    {
        MPI_Init(&i_ArgumentCount, &c_ArgumentArray);

        MPI_Comm mpi_communicator = MPI_COMM_WORLD;

        // //------------------------------
        // test_task_count_for_me_block_1D(false, mpi_communicator);
        // //------------------------------
        // test_task_count_for_all_block_1D(false, mpi_communicator);
        // //------------------------------
        // test_task_ids_for_me_block_1D(false, mpi_communicator);
        // //------------------------------
        test_Class_MpiJob_TaskIdManagerBlock1D(true, mpi_communicator);
        // //------------------------------
        // test_Class_MpiJob_RankIdBlock1D(false, mpi_communicator);
        // //------------------------------
        // test_gatherv_displacements_block_1D(false, mpi_communicator);
        // //------------------------------
        // test_task_count_for_me_cyclic_1D(false, mpi_communicator);
        // //------------------------------

        MPI_Finalize();

        return 0;
    }

## test_module_MpiJob all tests

Test MpiJob.task_count_for_me_block_1D.

    // static bool
    // test_task_count_for_me_block_1D(
    //     bool bool_RunTest, 
    //     MPI_Comm mpi_communicator)
    // {
    //     if (!bool_RunTest) { return false; }

    //     puts("\n test_task_count_for_me_block_1D() \n");
        
    //     //-----------------------------------
    //     // import
    //     //-----------------------------------
    //     module_MpiJob
    //     MpiJob = import_MpiJob();

    //     module_Assert
    //     Assert = import_Assert();

    //     //------------------------------------------------------
    //     //                     MPI INFO
    //     //------------------------------------------------------
    //     int i_MPI_ThisRank = -1; // MPI rank of this process
    //     int i_MPI_Volume   = -1; // MPI process count

    //     MPI_Comm_rank(mpi_communicator, &i_MPI_ThisRank);
    //     MPI_Comm_size(mpi_communicator, &i_MPI_Volume);
    //     //------------------------------------------------------
         
    //     if (i_MPI_ThisRank == 0)
    //     {
    //         printf("\n\t *** MPI this rank = %d/%d ***\n\n", i_MPI_ThisRank, i_MPI_Volume);

    //         int mpi_volume = 3;
    //         int mpi_total_task_count = 10;
    //         int i_ExpectedArray[3] = {4,3,3};

    //         for (int i = 0; i < mpi_volume; i++)
    //         {
    //             int this_rank = i;
    //             int answer = -1;

    //             Params_MpiJob_TaskCountForMeBlock1D
    //             params = MpiJob.new_Params_TaskCountForMeBlock1D();
    //             params.mpi_volume = mpi_volume;
    //             params.mpi_this_rank = this_rank;
    //             params.mpi_total_task_count = mpi_total_task_count;
    //             params.result_OUT = &answer;
    //             MpiJob.task_count_for_me_block_1D(params);

    //             int expect = i_ExpectedArray[i];

    //             printf("answer: %d\n", answer);
    //             printf("expect: %d\n", expect);

    //             Params_Assert_Equal_int 
    //             params_assert = {
    //                 .input1 = answer,
    //                 .input2 = expect,
    //             };

    //             Assert.equal_int(params_assert);
    //         }

    //     }
    //     return true;
    // }

Test MpiJob.task_count_for_all_block_1D.

    // static bool
    // test_task_count_for_all_block_1D(
    //     bool bool_RunTest, 
    //     MPI_Comm mpi_communicator)
    // {
    //     if (!bool_RunTest) { return false; }

    //     puts("\n test_task_count_for_all_block_1D() \n");
        
    //     //-----------------------------------
    //     // import
    //     //-----------------------------------
    //     module_MpiJob
    //     MpiJob = import_MpiJob();

    //     module_Vector
    //     Vector = import_Vector();

    //     module_Assert
    //     Assert = import_Assert();

    //     //------------------------------------------------------
    //     //                     MPI INFO
    //     //------------------------------------------------------
    //     int i_MPI_ThisRank = -1; // MPI rank of this process
    //     int i_MPI_Volume   = -1; // MPI process count

    //     MPI_Comm_rank(mpi_communicator, &i_MPI_ThisRank);
    //     MPI_Comm_size(mpi_communicator, &i_MPI_Volume);
    //     //------------------------------------------------------
         
    //     if (i_MPI_ThisRank == 0)
    //     {
    //         printf("\n\t *** MPI this rank = %d/%d ***\n\n", i_MPI_ThisRank, i_MPI_Volume);

    //         int mpi_volume = 3;
    //         int vector_length = mpi_volume;
    //         int mpi_total_task_count = 10;
    //         int answer[3];
    //         int expect[3] = {4,3,3};

    //         Params_MpiJob_TaskCountForAllBlock1D
    //         params = MpiJob.new_Params_TaskCountForAllBlock1D();
    //         params.mpi_volume = mpi_volume;
    //         params.mpi_total_task_count = mpi_total_task_count;
    //         params.results_OUT = answer;
    //         MpiJob.task_count_for_all_block_1D(params);

    //         Params_Assert_EqualArray_int
    //         params_assert = {
    //             .input1 = answer,
    //             .input2 = expect,
    //             .length = vector_length,
    //         };

    //         Assert.equal_array_int(params_assert);

    //         Params_Vector_Print_int
    //         params_print = {
    //             .vector = NULL,
    //             .length = vector_length,
    //         };

    //         puts("answer:");
    //         params_print.vector = answer;
    //         Vector.print_int(params_print);

    //         puts("expect:");
    //         params_print.vector = expect;
    //         Vector.print_int(params_print);

    //     }
    //     return true;
    // }


Test MpiJob.task_ids_for_me_block_1D.

    // static bool
    // test_task_ids_for_me_block_1D(
    //     bool bool_RunTest, 
    //     MPI_Comm mpi_communicator)
    // {
    //     if (!bool_RunTest) { return false; }

    //     puts("\n test_task_ids_for_me_block_1D() \n");

    //     //-----------------------------------
    //     // import
    //     //-----------------------------------
    //     module_MpiJob
    //     MpiJob = import_MpiJob();

    //     module_Assert
    //     Assert = import_Assert();

    //     //------------------------------------------------------
    //     //                     MPI INFO
    //     //------------------------------------------------------
    //     int i_MPI_ThisRank = -1; // MPI rank of this process
    //     int i_MPI_Volume   = -1; // MPI process count

    //     MPI_Comm_rank(mpi_communicator, &i_MPI_ThisRank);
    //     MPI_Comm_size(mpi_communicator, &i_MPI_Volume);
    //     //------------------------------------------------------
         
    //     if (i_MPI_ThisRank == 0)
    //     {
    //         printf("\n\t *** MPI this rank = %d/%d ***\n\n", i_MPI_ThisRank, i_MPI_Volume);

    //         int mpi_volume = 3;
    //         int mpi_total_task_count = 10;
    //         int *i_ExpectedArray[3];
    //         i_ExpectedArray[0] = (int[]) {0, 1, 2, 3};
    //         i_ExpectedArray[1] = (int[]) {4, 5, 6};
    //         i_ExpectedArray[2] = (int[]) {7, 8, 9};

    //         int i_TaskCountArray[3] = {4, 3, 3};

    //         for (int i = 0; i < mpi_volume; i++)
    //         {
    //             puts("----------------------------------");
    //             int this_rank = i;
    //             int vector_length = i_TaskCountArray[i];
    //             int answer[vector_length];

    //             printf("for MPI rank: %d\n", this_rank);

    //             Params_MpiJob_TaskIdsForMeBlock1D
    //             params = MpiJob.new_Params_TaskIdsForMeBlock1D();
    //             params.mpi_volume = mpi_volume;
    //             params.mpi_this_rank = this_rank;
    //             params.mpi_total_task_count = mpi_total_task_count;
    //             params.results_OUT = answer;
    //             MpiJob.task_ids_for_me_block_1D(params);

    //             int *expect = i_ExpectedArray[i];

    //             for (int j = 0; j < i_TaskCountArray[i]; j++)
    //             {
    //                 puts("\n............................");
    //                 printf("answer: %d\n", answer[j]);
    //                 printf("expect: %d\n", expect[j]);
    //                 puts("");

    //                 Params_Assert_Equal_int 
    //                 params_assert = {
    //                     .input1 = answer[j],
    //                     .input2 = expect[j],
    //                 };

    //                 bool result = Assert.equal_int(params_assert);
    //                 assert(result == true);
    //             }
    //         }

    //     }
    //     return true;
    // }



Test MpiJob.task_ids_for_me_block_1D.

    // static bool
    // test_Class_MpiJob_RankIdBlock1D(
    //     bool bool_RunTest, 
    //     MPI_Comm mpi_communicator)
    // {
    //     if (!bool_RunTest) { return false; }

    //     puts("\n test_Class_MpiJob_RankIdBlock1D() \n");

    //     //-----------------------------------
    //     // import
    //     //-----------------------------------
    //     module_MpiJob
    //     MpiJob = import_MpiJob();

    //     module_Assert
    //     Assert = import_Assert();

    //     //------------------------------------------------------
    //     //                     MPI INFO
    //     //------------------------------------------------------
    //     int i_MPI_ThisRank = -1; // MPI rank of this process
    //     int i_MPI_Volume   = -1; // MPI process count

    //     MPI_Comm_rank(mpi_communicator, &i_MPI_ThisRank);
    //     MPI_Comm_size(mpi_communicator, &i_MPI_Volume);
    //     //------------------------------------------------------
         
    //     if (i_MPI_ThisRank == 0)
    //     {
    //         printf("\n\t *** MPI this rank = %d/%d ***\n\n", i_MPI_ThisRank, i_MPI_Volume);

    //         int mpi_volume = 3;
    //         int mpi_total_task_count = 5;
    //         int i_ExpectedArray[5] = {0, 0, 1, 1, 2};
    //         int storage[mpi_volume];

    //         Class_MpiJob_RankIdBlock1D
    //         obj_rank = MpiJob.new_RankIdBlock1D(
    //             mpi_volume,
    //             mpi_total_task_count,
    //             storage);

    //         obj_rank.mpi_volume = 3;
    //         obj_rank.mpi_total_task_count = 5;

    //         for (int i = 0; i < mpi_total_task_count; i++)
    //         {
    //             puts("------------------");
    //             printf("\ni = %d\n", i);
    //             int task_id = i;
    //             int answer = obj_rank.rank(obj_rank, task_id);
    //             int expect = i_ExpectedArray[i];

    //             printf("rank answer: %d\n", answer);
    //             printf("rank expect: %d\n", expect);

    //             Params_Assert_Equal_int 
    //             params_assert = {
    //                 .input1 = answer,
    //                 .input2 = expect,
    //             };

    //             Assert.equal_int(params_assert);
    //         }
    //         puts("-------------------");

    //         //----------------------------------
    //         // check for non-existent task_id
    //         //----------------------------------
    //         puts("");
    //         puts("check the case of non-existent task_id");
    //         int task_id = mpi_total_task_count;
    //         int answer = obj_rank.rank(obj_rank, task_id);
    //         int expect = -1;
    //         printf("rank answer: %d\n", answer);
    //         printf("rank expect: %d\n", expect);

    //         Params_Assert_Equal_int 
    //         params_assert = {
    //             .input1 = answer,
    //             .input2 = expect,
    //         };

    //         Assert.equal_int(params_assert);
    //         puts("-------------------");

    //         //------------------------------
    //         // check for the insidious case
    //         // where #cpu cores > #tasks
    //         //------------------------------
    //         puts("-------------------");
    //         puts("\nCheck for the case when #cores > #tasks");
    //         puts("(note: expected failure)\n");
    //         Class_MpiJob_RankIdBlock1D
    //         obj_rank2 = MpiJob.new_RankIdBlock1D(
    //             1000,
    //             1,
    //             storage);

    //         bool success = obj_rank2.success;
    //         bool expected_flag = false;

    //         printf("result: %d\n", success);
    //         printf("expect: %d\n", expected_flag);
    //         assert(success == expected_flag);
    //     }
    //     return true;
    // }




Test MpiJob.mpi_gatherv_displacement_array_block_1D.

    // static bool
    // test_gatherv_displacements_block_1D(
    //     bool bool_RunTest, 
    //     MPI_Comm mpi_communicator)
    // {
    //     if (!bool_RunTest) { return false; }

    //     puts("\n test_gatherv_displacements_block_1D() \n");

    //     //-----------------------------------
    //     // import
    //     //-----------------------------------
    //     module_MpiJob
    //     MpiJob = import_MpiJob();

    //     module_Vector
    //     Vector = import_Vector();

    //     module_Assert
    //     Assert = import_Assert();

    //     //------------------------------------------------------
    //     //                     MPI INFO
    //     //------------------------------------------------------
    //     int i_MPI_ThisRank = -1; // MPI rank of this process
    //     int i_MPI_Volume   = -1; // MPI process count

    //     MPI_Comm_rank(mpi_communicator, &i_MPI_ThisRank);
    //     MPI_Comm_size(mpi_communicator, &i_MPI_Volume);
    //     //------------------------------------------------------
         
    //     if (i_MPI_ThisRank == 0)
    //     {
    //         printf("\n\t *** MPI this rank = %d/%d ***\n\n", i_MPI_ThisRank, i_MPI_Volume);

    //         int vector_length = 3;
    //         int mpi_volume = 3;
    //         int mpi_total_task_count = 5;
    //         int answer[3];
    //         int expect[3] = {0, 2, 4};

    //         Params_MpiJob_GathervDisplacementsBlock1D
    //         params_mpijob = MpiJob.new_Params_GathervDisplacementsBlock1D();
    //         params_mpijob.mpi_total_task_count = mpi_total_task_count;
    //         params_mpijob.mpi_volume = mpi_volume;
    //         params_mpijob.results_OUT = answer;
    //         MpiJob.gatherv_displacements_block_1D(params_mpijob);

    //         Params_Assert_EqualArray_int
    //         params_assert  = {
    //             .input1 = answer,
    //             .input2 = expect,
    //             .length = vector_length,
    //         };

    //         Assert.equal_array_int(params_assert);

    //         Params_Vector_Print_int
    //         params_print = {
    //             .vector = NULL,
    //             .length = vector_length,
    //         };

    //         puts("answer:");
    //         params_print.vector = answer;
    //         Vector.print_int(params_print);

    //         puts("expect:");
    //         params_print.vector = expect;
    //         Vector.print_int(params_print);
    //     }
    //     return true;
    // }



Test MpiJob.task_count_for_me_block_1D.

    // static bool
    // test_task_count_for_me_cyclic_1D(
    //     bool bool_RunTest, 
    //     MPI_Comm mpi_communicator)
    // {
    //     if (!bool_RunTest) { return false; }

    //     puts("\n test_task_count_for_me_cyclic_1D() \n");
        
    //     //-----------------------------------
    //     // import
    //     //-----------------------------------
    //     module_MpiJob
    //     MpiJob = import_MpiJob();

    //     module_Assert
    //     Assert = import_Assert();

    //     //------------------------------------------------------
    //     //                     MPI INFO
    //     //------------------------------------------------------
    //     int i_MPI_ThisRank = -1; // MPI rank of this process
    //     int i_MPI_Volume   = -1; // MPI process count

    //     MPI_Comm_rank(mpi_communicator, &i_MPI_ThisRank);
    //     MPI_Comm_size(mpi_communicator, &i_MPI_Volume);
    //     //------------------------------------------------------
         
    //     if (i_MPI_ThisRank == 0)
    //     {
    //         printf("\n\t *** MPI this rank = %d/%d ***\n\n", i_MPI_ThisRank, i_MPI_Volume);

    //         int mpi_volume = 3;
    //         int mpi_total_task_count = 10;
    //         int i_ExpectedArray[3] = {4,3,3};

    //         for (int i = 0; i < mpi_volume; i++)
    //         {
    //             int this_rank = i;
    //             int answer = -1;

    //             Params_MpiJob_TaskCountForMeCyclic1D
    //             params = MpiJob.new_Params_TaskCountForMeCyclic1D();
    //             params.mpi_volume = mpi_volume;
    //             params.mpi_this_rank = this_rank;
    //             params.mpi_total_task_count = mpi_total_task_count;
    //             params.result_OUT = &answer;
    //             MpiJob.task_count_for_me_cyclic_1D(params);

    //             int expect = i_ExpectedArray[i];

    //             printf("answer: %d\n", answer);
    //             printf("expect: %d\n", expect);

    //             Params_Assert_Equal_int 
    //             params_assert = {
    //                 .input1 = answer,
    //                 .input2 = expect,
    //             };

    //             Assert.equal_int(params_assert);
    //         }

    //     }
    //     return true;
    // }