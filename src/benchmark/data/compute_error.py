

import numpy


def read_key(file_name):
    data = numpy.loadtxt(file_name)
    return data


def read_result(file_name):
    data = numpy.loadtxt(file_name)
    return data

def compute_rmsd_error(A,B):
    diff = A - B
    m, n = numpy.shape(A)
    length = m * n
    length = length * 1.0 # convert to double
    rmsd = numpy.sqrt(numpy.sum(numpy.square(diff))/length)
    return rmsd


def compute_error(s_WhichMatrix, size, mpi_np, tag):
    result_file_name = ''
    if mpi_np == 1:
        result_file_name = "./raw/mpi/answer_{0}_{1}x{1}_serial_{2}.dat".format(
            s_WhichMatrix,
            size,
            tag
            )
    else:
        result_file_name = "./raw/mpi/answer_{0}_{1}x{1}_mpi_{2}_{3}.dat".format(
                s_WhichMatrix,
                size,
                mpi_np,
                tag
                )
    answer = read_result(result_file_name)

    expect_file_name = "./expect/expect_{1}_{0}x{0}.dat".format(size, s_WhichMatrix)
    expect = read_result(expect_file_name)

    return compute_rmsd_error(answer, expect)


def main():
    size = 1024
    omp_level = 0
    mpi_mapping = 1
    list_ErrorR = []
    list_ErrorV = []
    for omp_level in [1]:
    # for omp_level in [1, 2, 4, 6]:
        # for mpi_np in [1, 2, 4, 8, 12, 24, 28, 32, 36, 40, 44, 48]:
        for mpi_np in [1, 2, 4, 8, 12, 24, 28, 32, 36, 40, 44, 48]:
        # for mpi_np in [1, 2, 4, 8]:
            tag = "omp_{}_map_{}".format(omp_level, mpi_mapping)
            print('mpi np', mpi_np)
            error_R = compute_error('R', size, mpi_np, tag)
            error_V = compute_error('V', size, mpi_np, tag)
            list_ErrorR.append((mpi_np, error_R))
            list_ErrorV.append((mpi_np, error_V))
            print('\t rmsd V', list_ErrorV)
            print('\t rmsd R', list_ErrorR)

        file_error_R = './error/mpi/error_R_mpi_omp_{}_map_{}.dat'.format(omp_level, mpi_mapping)
        file_error_V = './error/mpi/error_V_mpi_omp_{}_map_{}.dat'.format(omp_level, mpi_mapping)
        numpy.savetxt(file_error_R, numpy.array(list_ErrorR))
        numpy.savetxt(file_error_V, numpy.array(list_ErrorV))

main()
        


