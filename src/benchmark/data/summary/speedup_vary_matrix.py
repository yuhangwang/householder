import numpy

import sys


def save_speedup(serial_data, mpi_data, tag):
    speedup = serial_data[:,2]/mpi_data[:,2]

    matrix_sizes = serial_data[:,0]

    m, n = numpy.shape(serial_data)

    speedup_matrix = numpy.zeros((m, 2))

    speedup_matrix[:,0] = matrix_sizes
    speedup_matrix[:,1] = speedup

    output_file = "summary_speedup_{}_vary_matrix_size.data".format(tag)

    numpy.savetxt(output_file, speedup_matrix)


def save_efficiency(serial_data, mpi_data, tag):
    E = serial_data[:,1]/mpi_data[:,1]

    m, n = numpy.shape(serial_data)

    efficiency_matrix = numpy.zeros((m, 2))
    efficiency_matrix[:,0] = serial_data[:,0]
    efficiency_matrix[:,1] = E 

    output_file = "summary_efficiency_{}_vary_matrix_size.data".format(tag)

    numpy.savetxt(output_file, efficiency_matrix)



def save_Tp_sec(data, tag):
    m, n = numpy.shape(data)

    output = numpy.zeros((m, 2))
    output[:,0] = data[:,0]
    output[:,1] = data[:,2]/1000000.

    output_file = "summary_Tp_sec_{}_vary_matrix_size.data".format(tag)
    numpy.savetxt(output_file, output)



def main():
    for mpi_mapping in [0, 1]:
        node_count = 48 
        serial_file = "summary_time_mpi_1_map_{}_vary_matrix_size.dat".format(mpi_mapping)
        mpi_file = "summary_time_mpi_{}_map_{}_vary_matrix_size.dat".format(node_count, mpi_mapping)

        mpi_tag = 'mpi_{}_map_{}'.format(node_count, mpi_mapping)
        serial_tag = 'serial_map_{}'.format(mpi_mapping)

        serial_data = numpy.loadtxt(serial_file)
        mpi_data = numpy.loadtxt(mpi_file)

        save_speedup(serial_data,mpi_data, mpi_tag)
        save_efficiency(serial_data, mpi_data, mpi_tag)
        save_Tp_sec(serial_data, serial_tag)
        save_Tp_sec(mpi_data, mpi_tag)



main()