import numpy

import sys





def save_speedup(data, tag):
    speedup = data[0,2]/data[:,2]

    m, n = numpy.shape(data)

    list_Output = []

    output_file = "summary_speedup_{0}.data".format(tag)
    obj_Out = open(output_file, 'w')
    for i in range(m):
        obj_Out.write("{0:.0f} {1:.3f}\n".format(
            data[i,0], speedup[i]))
    obj_Out.close()

def save_efficiency(data, tag):
    E = data[0,1]/data[:,1]

    m, n = numpy.shape(data)

    list_Output = []

    output_file = "summary_efficiency_{0}.data".format(tag)
    obj_Out = open(output_file, 'w')
    for i in range(m):
        obj_Out.write("{0:.0f} {1:.3f}\n".format(
            data[i,0], E[i]))
    obj_Out.close()


def save_Tp_ms(data, tag):
    m, n = numpy.shape(data)

    output = numpy.zeros((m, 2))
    output[:,0] = data[:,0]
    output[:,1] = data[:,2]/1000.

    file_name = "summary_Tp_ms_{0}.data".format(tag)
    numpy.savetxt(file_name, output)

def save_Tp_us(data, tag):
    m, n = numpy.shape(data)

    output = numpy.zeros((m, 2))
    output[:,0] = data[:,0]
    output[:,1] = data[:,2]

    file_name = "summary_Tp_us_{0}.data".format(tag)
    numpy.savetxt(file_name, output)


def main():
    mpi_mapping=0
    # for omp_level in [0, 1, 2, 4, 6]:
    for omp_level in [1]:
        tag = "omp_{0}_map_{1}_vary_size".format(omp_level, mpi_mapping)
        file_name = "summary_time_{0}.dat".format(tag)

        data = numpy.loadtxt(file_name)

        save_speedup(data, tag)
        save_efficiency(data, tag)
        save_Tp_ms(data, tag)
        save_Tp_us(data, tag)

main()