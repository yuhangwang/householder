#!/bin/sh


in_dir="raw/mpi"
out_dir="summary/mpi"

mpi_mapping="0"

for nodes in 1 48 ; do 
    output_file="${out_dir}/summary_time_mpi_${nodes}_map_${mpi_mapping}_vary_matrix_size.dat"
    echo -n "" > $output_file
    echo $output_file
    for cores in 1 ; do
        for size in 100 200 400 800 1600 ; do
            if [ $nodes -eq 1 ]; then
                # serial
                file="${in_dir}/time_cost_sum_microsecond_${size}x${size}_serial_omp_${cores}.dat"
                echo -n "$size " >> $output_file
                awk '{print $2, $3}' $file >> $output_file
            else

               file="${in_dir}/time_cost_sum_microsecond_${size}x${size}_mpi_${nodes}_omp_${cores}_map_${mpi_mapping}.dat"
               echo -n "$size " >> $output_file
               awk '{print $2, $3}' $file >> $output_file
            fi
        done 
    done
done