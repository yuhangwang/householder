#!/bin/sh


in_dir="raw/mpi"
out_dir="summary/mpi"

mpi_mapping="0"

# size=1024
# for cores in 1 ; do
# # for cores in 1 2 4 6 ; do
#     output_file="${out_dir}/summary_time_omp_${cores}_map_${mpi_mapping}_vary_size.dat"
#     # for nodes in 1 2 4 8 12 24 28 32 36 40 44 48 ; do 
#     for nodes in 48 ; do 
#     # for nodes in 1 2 4 8 ; do
#         if [ $nodes -eq 1 ]; then
#             # serial
#             file="${in_dir}/time_cost_sum_microsecond_${size}x${size}_serial_omp_${cores}.dat"
#             cat $file > $output_file
#         else

#            file="${in_dir}/time_cost_sum_microsecond_${size}x${size}_mpi_${nodes}_omp_${cores}_map_${mpi_mapping}.dat"
#            echo -n "$nodes " >> $output_file
#            awk '{print $2, $3}' $file >> $output_file
#         fi
#     done 
# done

for size in 100 200 400 800 1600 ; do
    for cores in 1 ; do
    # for cores in 1 2 4 6 ; do
        output_file="${out_dir}/summary_time_omp_${cores}_map_${mpi_mapping}_vary_size.dat"
        # for nodes in 1 2 4 8 12 24 28 32 36 40 44 48 ; do 
        for nodes in 48 ; do 
        # for nodes in 1 2 4 8 ; do
            if [ $nodes -eq 1 ]; then
                # serial
                file="${in_dir}/time_cost_sum_microsecond_${size}x${size}_serial_omp_${cores}.dat"
                echo -n "$size " >> $output_file
                cat $file > $output_file
            else

               file="${in_dir}/time_cost_sum_microsecond_${size}x${size}_mpi_${nodes}_omp_${cores}_map_${mpi_mapping}.dat"
               echo -n "$size " >> $output_file
               awk '{print $2, $3}' $file >> $output_file
            fi
        done 
    done
done