#!/bin/sh

#!/bin/bash
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=12
#PBS -N block
#PBS -q test
#PBS -j oe

# Load MPI module (Enable MPI in user environment) 
module load  mpi/mpich/3.1.3-gcc-4.7.1

# Change to the directory from which the batch job was submitted
cd $PBS_O_WORKDIR

exe_file="Run.exe"
mpi_mapping=0

for matrix_size in 100 200 400 800 1600 ; do
  for node_count in 1 ; do
    for thread_count in 1 ; do 
      echo $node_count
      mpirun -np $node_count ./$exe_file $matrix_size $thread_count $mpi_mapping
    done
  done
done
