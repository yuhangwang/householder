#ifndef MODULE_MATHSIGN_H
#define MODULE_MATHSIGN_H

#include "module_MathSign.dep.h"

typedef struct
Params_MathSign
{
    void *input;
    MyTypeChoice type;
}
Params_MathSign;

typedef struct 
module_MathSign
{
    void (*hi)(void);
    int  (*sign)(Params_MathSign);
    int  (*sign_float)(float);
}
module_MathSign;

module_MathSign
import_MathSign(void);

#endif