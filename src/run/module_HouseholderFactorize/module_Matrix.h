/**********************************************************
 Copyright (c) 2015 YUHANG WANG 
 ALL RIGHTS RESERVED

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
************************************************************/

#ifndef MODULE_MATRIX_H
#define MODULE_MATRIX_H

#include "module_Matrix.dep.h"

typedef struct 
Params_Matrix_Print
{
    void *matrix;
    int   matrix_row_count;
    int   matrix_column_count;
    int   precision;
    MyTypeChoice type;
}
Params_Matrix_Print;

typedef struct 
Params_Matrix_Print_float
{
    float *matrix;
    int    matrix_row_count;
    int    matrix_column_count;
    int    precision;
}
Params_Matrix_Print_float;
//------------------------------------------------    

//------------------------------------------------
// class for manipulating matrix indices
// for 2D array that is stored as 1D array.
//------------------------------------------------
typedef struct
Class_MatrixIndex2D
{   int matrix_row_count;
    int matrix_column_count;
    int length; // length of the 1D representation
    int (*index)(struct Class_MatrixIndex2D self, int i,int j);
}
Class_MatrixIndex2D;

//------------------------------------------------
//  parameters for o2_norm_column
//------------------------------------------------
/*
 * Calculate the order-2 norm of selected matrix columns
 */
typedef struct 
Params_Matrix_o2NormColumn_float
{
    float *matrix;
    int    matrix_row_count;   // row count of the matrix
    int    matrix_column_count; // column count of the matrix
    int    row_id_eon; // starting row ID
    int    row_id_end; // ending row ID
    int    column_id;  // corresponding column ID
    float *result_OUT; // pointer to the result storage
}
Params_Matrix_o2NormColumn_float;
//------------------------------------------------

//------------------------------------------------
//  parameters for o2_norm_column
//------------------------------------------------
/*
 * Calculate the order-2 norm of selected matrix columns
 */
typedef struct 
Params_Matrix_o2NormColumns_float
{
    float *matrix;
    int    matrix_row_count;   // row count of the matrix
    int    matrix_column_count; // column count of the matrix
    int   *row_ids_eon; // starting row IDs
    int   *row_ids_end; // ending row IDs
    int   *column_ids;  // corresponding column IDs
    int    length;      // length of each of the above arrays, e.g. 2
    float *results_OUT; // pointer to the result storage
}
Params_Matrix_o2NormColumns_float;
//------------------------------------------------

//------------------------------------------------
// parameters for matrix column dot product
//------------------------------------------------
typedef struct 
Params_Matrix_DotColumn_float
{
    float *matrix;
    int    matrix_row_count;
    int    matrix_column_count;
    int    row_id_eon;
    int    row_id_end;
    float *vector;
    int    vector_length;
    int    column_id;
    float *result_OUT;
}
Params_Matrix_DotColumn_float;

//------------------------------------------------
// parameters for adding a scaled vector to a 
// matrix column (in place)
//------------------------------------------------
typedef struct 
Params_Matrix_AddScaledVectorToColumn_float
{
    float *matrix;
    int    matrix_row_count;
    int    matrix_column_count;
    int    row_id_eon;
    int    row_id_end;
    int    column_id;
    float *vector;
    int    vector_id_eon;
    int    vector_id_end;
    float  scalar;
}
Params_Matrix_AddScaledVectorToColumn_float;

//-----------------------------
// parameters for Matrix.reset()
//-----------------------------
typedef struct 
Params_Matrix_Reset_float
{
    float *matrix;
    int    matrix_row_count;
    int    matrix_column_count;
    float  value;
}
Params_Matrix_Reset_float;

//-----------------------------
// parameters for Matrix.copy_vector_to_column_float()
//-----------------------------
typedef struct 
Params_Matrix_CopyVectorToColumn_float
{
    float *matrix;
    int    matrix_row_count;
    int    matrix_column_count;
    int    row_id_eon;
    int    row_id_end;
    int    column_id;
    float *vector;
    int    vector_length;
}
Params_Matrix_CopyVectorToColumn_float;

//-----------------------------
// parameters for Matrix.copy_vector_to_columns_float()
//-----------------------------
typedef struct 
Params_Matrix_CopyVectorToColumns_float
{
    float *matrix;
    int    matrix_row_count;
    int    matrix_column_count;
    int    row_id_eon;
    int    row_id_end;
    int    column_id_eon;
    int    column_id_end;
    float *vector;
    int    vector_length;
}
Params_Matrix_CopyVectorToColumns_float;

//-----------------------------
// parameters for Matrix.copy_column_to_vector_float()
//-----------------------------
typedef struct 
Params_Matrix_CopyColumnToVector_float
{
    float *matrix;
    int    matrix_row_count;
    int    matrix_column_count;
    int    row_id_eon;
    int    row_id_end;
    int    column_id;
    float *vector_OUT;
    int    vector_length;
}
Params_Matrix_CopyColumnToVector_float;

//-----------------------------
// parameters for Matrix.copy_column_to_vector_float()
//-----------------------------
typedef struct 
Params_Matrix_CopyColumnsToVector_float
{
    float *matrix;
    int    matrix_row_count;
    int    matrix_column_count;
    int    row_id_eon;
    int    row_id_end;
    int    column_id_eon;
    int    column_id_end;
    float *vector_OUT;
    int    vector_length;
}
Params_Matrix_CopyColumnsToVector_float;

//------------------------------------------------
// parameters for Matrix.write_text_file_float()
//------------------------------------------------
typedef struct 
Params_Matrix_WriteTextFile_float
{
    char *file_name;
    float *matrix;
    int    matrix_row_count;
    int    matrix_column_count;
    int    precision;
    bool   scientific;
}
Params_Matrix_WriteTextFile_float;

//------------------------------------------------
/**************************************************/  
/*************  Module Matrix Interface ***********/
/**************************************************/    
typedef struct 
module_Matrix
{
    void (*hi)(void);
    void (*print)(Params_Matrix_Print);
    void (*print_float)(Params_Matrix_Print_float);
    void (*o2_norm_column_float)(Params_Matrix_o2NormColumn_float);
    void (*o2_norm_columns_float)(Params_Matrix_o2NormColumns_float);
    void (*dot_column_float)(Params_Matrix_DotColumn_float);
    void (*add_scaled_vector_to_column_float)(Params_Matrix_AddScaledVectorToColumn_float);
    void (*reset_float)(Params_Matrix_Reset_float);
    void (*copy_vector_to_column_float)(Params_Matrix_CopyVectorToColumn_float);
    void (*copy_vector_to_columns_float)(Params_Matrix_CopyVectorToColumns_float);
    void (*copy_column_to_vector_float)(Params_Matrix_CopyColumnToVector_float);
    void (*copy_columns_to_vector_float)(Params_Matrix_CopyColumnsToVector_float);
    void (*write_text_file_float)(Params_Matrix_WriteTextFile_float);
    Class_MatrixIndex2D (*new_MatrixIndex2D)(int matrix_row_count, int matrix_column_count);
    Params_Matrix_o2NormColumn_float  (*new_Params_o2NormColumn_float)(void);
    Params_Matrix_o2NormColumns_float (*new_Params_o2NormColumns_float)(void);
    Params_Matrix_DotColumn_float (*new_Params_DotColumn_float)(void);
    Params_Matrix_Reset_float (*new_Params_Reset_float)(void);
    Params_Matrix_CopyVectorToColumn_float (*new_Params_CopyVectorToColumn_float)(void);
    Params_Matrix_CopyVectorToColumns_float (*new_Params_CopyVectorToColumns_float)(void);
    Params_Matrix_CopyColumnToVector_float (*new_Params_CopyColumnToVector_float)(void);
    Params_Matrix_CopyColumnsToVector_float (*new_Params_CopyColumnsToVector_float)(void);
    Params_Matrix_WriteTextFile_float (*new_Params_WriteTextFile_float)(void);
}
module_Matrix;

module_Matrix
import_Matrix(void);

#endif
/**************************************************/    
//------------------------------------------------