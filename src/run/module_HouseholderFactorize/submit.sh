#!/bin/sh

#!/bin/bash
#PBS -l walltime=00:30:00
#PBS -l nodes=4:ppn=12
#PBS -N project
#PBS -q cs
#PBS -j oe

# Load MPI module (Enable MPI in user environment) 
module load  mpi/mpich/3.1.3-gcc-4.7.1

# Change to the directory from which the batch job was submitted
cd $PBS_O_WORKDIR

matrix_size=1024
exe_file="Run.exe"
mpi_mapping=1
for node_count in 2 4 8 12 24 28 32 36 40 44 48 ; do
  for thread_count in 1 ; do 
    echo $node_count
    mpirun -np $node_count ./$exe_file $matrix_size $thread_count $mpi_mapping
  done
done
