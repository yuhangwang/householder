/**********************************************************
 Copyright (c) 2015 YUHANG WANG 
 ALL RIGHTS RESERVED

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
************************************************************/

#include "module_Matrix.dep.c"

static Params_Matrix_o2NormColumn_float
new_Params_o2NormColumn_float(void)
{
    Params_Matrix_o2NormColumn_float
    obj = {
        .matrix = NULL,
        .row_id_eon = -1,
        .row_id_end = -1,
        .column_id  = -1,
        .matrix_row_count  = -1,
        .matrix_column_count = -1,
        .result_OUT = NULL,
    };
    return obj;
}

static Params_Matrix_o2NormColumns_float
new_Params_o2NormColumns_float(void)
{
    Params_Matrix_o2NormColumns_float
    obj = {
        .matrix = NULL,
        .row_ids_eon = NULL,
        .row_ids_end = NULL,
        .column_ids  = NULL,
        .matrix_row_count  = -1,
        .matrix_column_count = -1,
        .results_OUT = NULL,
    };
    return obj;
}

static Params_Matrix_DotColumn_float
new_Params_DotColumn_float(void)
{
    Params_Matrix_DotColumn_float
    obj = {
        .matrix = NULL,
        .matrix_row_count = -1,
        .matrix_column_count = -1,
        .column_id = -1,
        .vector = NULL,
        .vector_length = -1,
        .result_OUT = NULL,
    };
    return obj;
}

static Params_Matrix_Reset_float
new_Params_Reset_float(void)
{
    Params_Matrix_Reset_float
    obj = {
        .matrix = NULL,
        .matrix_row_count = -1,
        .matrix_column_count = -1,
        .value = 0.,
    };
    return obj;
}

static Params_Matrix_CopyVectorToColumn_float
new_Params_CopyVectorToColumn_float(void)
{
    Params_Matrix_CopyVectorToColumn_float
    obj = {
        .matrix = NULL,
        .matrix_row_count = -1,
        .matrix_column_count = -1,
        .row_id_eon = -1,
        .row_id_end = -1,
        .column_id = -1,
        .vector = NULL,
        .vector_length = -1,
    };
    return obj;
}

static Params_Matrix_CopyVectorToColumns_float
new_Params_CopyVectorToColumns_float(void)
{
    Params_Matrix_CopyVectorToColumns_float
    obj = {
        .matrix = NULL,
        .matrix_row_count = -1,
        .matrix_column_count = -1,
        .row_id_eon = -1,
        .row_id_end = -1,
        .column_id_eon = -1,
        .column_id_end = -1,
        .vector = NULL,
        .vector_length = -1,
    };
    return obj;
}

static Params_Matrix_CopyColumnToVector_float
new_Params_CopyColumnToVector_float(void)
{
    Params_Matrix_CopyColumnToVector_float
    obj = {
        .matrix = NULL,
        .matrix_row_count = -1,
        .matrix_column_count = -1,
        .row_id_eon = -1,
        .row_id_end = -1,
        .column_id = -1,
        .vector_OUT = NULL,
        .vector_length = -1,
    };
    return obj;
}

static Params_Matrix_CopyColumnsToVector_float
new_Params_CopyColumnsToVector_float(void)
{
    Params_Matrix_CopyColumnsToVector_float
    obj = {
        .matrix = NULL,
        .matrix_row_count = -1,
        .matrix_column_count = -1,
        .row_id_eon = -1,
        .row_id_end = -1,
        .column_id_eon = -1,
        .column_id_end = -1,
        .vector_OUT = NULL,
        .vector_length = -1,
    };
    return obj;
}

static Params_Matrix_WriteTextFile_float
new_Params_WriteTextFile_float(void)
{
    Params_Matrix_WriteTextFile_float
    obj = {
        .file_name = NULL,
        .matrix = NULL,
        .matrix_row_count = -1,
        .matrix_column_count = -1,
        .precision = 3,
        .scientific = true,
    };
    return obj;
}

static void 
hi(void)
{
	printf("Greetings from module_Matrix!\n");
}

//---------------------------------------------
/* 
 * function: print_float()
 */
//---------------------------------------------
static void
print_float(Params_Matrix_Print_float params)
{
    int module_Matrix_print_float__input_matrix_row_count = params.matrix_row_count;
    int module_Matrix_print_float__input_column_count = params.matrix_column_count;
    assert(module_Matrix_print_float__input_matrix_row_count > 0);
    assert(module_Matrix_print_float__input_column_count > 0);

    for (int i = 0; i < params.matrix_row_count; i++)
    {
        for (int j = 0; j < params.matrix_column_count; j++)
        {
            int index = (i * params.matrix_column_count) + j;

            printf("%.*f ", 
                params.precision,
                params.matrix[index]);
        }
        puts("");
    }
}
//---------------------------------------------

static void
print(Params_Matrix_Print params)
{
    module_Type
    Type = import_Type();

    if (Type.same(params.type, Type.Float))
    {
        float *A = (float*) params.matrix;

        Params_Matrix_Print_float
        inputs = {
            .matrix = &A[0],
            .matrix_row_count = params.matrix_row_count,
            .matrix_column_count = params.matrix_column_count,
            .precision = params.precision,
        };

        print_float(inputs);
    }
}

//---------------------------------------------

//---------------------------------------------
// Class for calculating 2D matrix index 
// in its 1D storage.
//---------------------------------------------
static int 
get_index(Class_MatrixIndex2D self, int i, int j)
{
    int MatrixIndex2D_requested_index = (i * self.matrix_column_count) + j;
    int matrix_length_1D = self.length;
    assert(MatrixIndex2D_requested_index < matrix_length_1D);
    return MatrixIndex2D_requested_index;
}

Class_MatrixIndex2D
new_MatrixIndex2D(int matrix_row_count, int matrix_column_count)
{
    int new_MatrixIndex2D_input1__matrix_row_count = matrix_row_count;
    int new_MatrixIndex2D_input2__column_count = matrix_column_count;
    assert(new_MatrixIndex2D_input1__matrix_row_count > 0);
    assert(new_MatrixIndex2D_input2__column_count > 0);

    Class_MatrixIndex2D
    obj = {
        .matrix_row_count = matrix_row_count,
        .matrix_column_count = matrix_column_count,
        .length = matrix_row_count * matrix_column_count,
        .index = get_index,
    };

    return obj;
}

//---------------------------------------------

//---------------------------------------------
// o2_norm_column_float
//---------------------------------------------
static void
o2_norm_column_float(Params_Matrix_o2NormColumn_float params)
{
    Class_MatrixIndex2D
    obj_Id = new_MatrixIndex2D(
        params.matrix_row_count, 
        params.matrix_column_count);

    int row_id_eon  = params.row_id_eon;
    int row_id_end  = params.row_id_end;
    int column_id   = params.column_id;
    float *o2_norm  = params.result_OUT;

    /********************************************/
    // o2_norm will accumulate the 2-norm of the column
    *o2_norm = 0.;
    /********************************************/
    
    for (int i = row_id_eon; i <= row_id_end; i++)
    {
        int index = obj_Id.index(obj_Id, i, column_id);
        float element = params.matrix[index];
        *o2_norm = *o2_norm + (element * element);
    }

    //---------------------------------
    // make sure *o2_norm is non-negative
    // before doing sqrt()
    //---------------------------------
    int module_Matrix_o2_norm_column_float_internal_varialbe_o2_norm
     = *o2_norm;
    assert(module_Matrix_o2_norm_column_float_internal_varialbe_o2_norm >= 0.);
    //---------------------------------
    
    *o2_norm = sqrt(*o2_norm);
}

//---------------------------------------------
// o2_norm_column_float
//---------------------------------------------
static void
o2_norm_columns_float(Params_Matrix_o2NormColumns_float params)
{
    for (int k = 0; k < params.length; k++)
    {
        Params_Matrix_o2NormColumn_float
        inputs = {
            .matrix = params.matrix,
            .row_id_eon = params.row_ids_eon[k],
            .row_id_end = params.row_ids_end[k],
            .column_id  = params.column_ids[k],
            .matrix_row_count = params.matrix_row_count,
            .matrix_column_count = params.matrix_column_count,
            .result_OUT = &params.results_OUT[k],
        };

        o2_norm_column_float(inputs);
    }
}

static void
dot_column_float(Params_Matrix_DotColumn_float params)
{
    float *A = params.matrix;
    float *vector = params.vector;
    int vector_length = params.vector_length;

    int row_id_eon = params.row_id_eon;
    int row_id_end = params.row_id_end;
    int column_id  = params.column_id;
    int i_RowCount = params.matrix_row_count;
    int i_ColumnCount = params.matrix_column_count;
    float *p_Output  = params.result_OUT;

    Class_MatrixIndex2D 
    obj_Id = new_MatrixIndex2D(i_RowCount, i_ColumnCount);

    /**********************************************/
    // make sure *p_Output starts from zero
    /**********************************************/
    *p_Output = 0.;
    /**********************************************/

    for (int i = row_id_eon; i <= row_id_end; i++)
    {
        int index = obj_Id.index(obj_Id, i, column_id);
        float matrix_item = A[index];
        float vector_item = *vector;
        vector++;// move on to the next

        *p_Output = *p_Output + (matrix_item * vector_item);

        /************************************************/
        // SAFETY CHECK
        /************************************************/
        int module_Matrix_dot_column_float_for_loop_i = i - row_id_eon;
        assert(module_Matrix_dot_column_float_for_loop_i < vector_length);
        /************************************************/
    }
}

static void
add_scaled_vector_to_column_float(Params_Matrix_AddScaledVectorToColumn_float params)
{
    Class_MatrixIndex2D
    obj_Id = new_MatrixIndex2D(params.matrix_row_count, params.matrix_column_count);

    int row_id_range_length = params.row_id_end - params.row_id_eon + 1;
    int vector_id_range_length = params.vector_id_end - params.vector_id_eon + 1;
    assert(row_id_range_length == vector_id_range_length);

    for (int i = 0; i < row_id_range_length; i++)
    {
        int i_RowId = i + params.row_id_eon;
        int matrix_index = obj_Id.index(obj_Id, i_RowId, params.column_id);
        int vector_index = i + params.vector_id_eon;
        params.matrix[matrix_index] += (params.scalar * params.vector[vector_index]);
    }
}

static void 
reset_float(Params_Matrix_Reset_float params)
{
    Class_MatrixIndex2D
    obj_Id = new_MatrixIndex2D(params.matrix_row_count, params.matrix_column_count);

    for (int i = 0; i < params.matrix_row_count; i++)
    {
        for (int j = 0; j < params.matrix_column_count; j++)
        {
            int index = obj_Id.index(obj_Id, i, j);
            params.matrix[index] = params.value;
        }
    }        
}

static void 
copy_vector_to_column_float(Params_Matrix_CopyVectorToColumn_float params)
{
    int row_id_range_length = params.row_id_end - params.row_id_eon + 1;
    int Matrix_copy_vector_to_column_float__row_id_range_length = row_id_range_length;
    assert(Matrix_copy_vector_to_column_float__row_id_range_length == params.vector_length);

    Class_MatrixIndex2D
    obj_Id = new_MatrixIndex2D(params.matrix_row_count, params.matrix_column_count);

    for (int i = 0; i < params.vector_length; i++)
    {
        int i_RowId = params.row_id_eon + i;
        int index = obj_Id.index(obj_Id, i_RowId, params.column_id);
        float value = *params.vector;
        params.matrix[index] = value;
        params.vector++;
    }        
}

static void 
copy_vector_to_columns_float(Params_Matrix_CopyVectorToColumns_float params)
{
    int row_id_range_length = params.row_id_end - params.row_id_eon + 1;
    int column_id_range_length = params.column_id_end - params.column_id_eon + 1;
    int Matrix_copy_vector_to_column_float__required_vector_length = 
        column_id_range_length * row_id_range_length;
    assert(Matrix_copy_vector_to_column_float__required_vector_length == params.vector_length);
    
    Params_Matrix_CopyVectorToColumn_float
    inputs = new_Params_CopyVectorToColumn_float();
    inputs.matrix = params.matrix;
    inputs.matrix_row_count = params.matrix_row_count;
    inputs.matrix_column_count = params.matrix_column_count;
    inputs.row_id_eon = params.row_id_eon;
    inputs.row_id_end = params.row_id_end;

    for (int k = 0; k < column_id_range_length; k++)
    { 
        int index_offset = k * row_id_range_length;
        float *vector = &params.vector[index_offset];
        int i_ColumnId = k + params.column_id_eon;
        inputs.column_id = i_ColumnId;
        inputs.vector = vector;
        inputs.vector_length = row_id_range_length;
        copy_vector_to_column_float(inputs);
    }
}

static void 
copy_column_to_vector_float(Params_Matrix_CopyColumnToVector_float params)
{
    int row_id_range_length = params.row_id_end - params.row_id_eon + 1;
    int Matrix_copy_column_to_vector_float__row_id_range_length = row_id_range_length;
    assert(Matrix_copy_column_to_vector_float__row_id_range_length == params.vector_length);

    Class_MatrixIndex2D
    obj_Id = new_MatrixIndex2D(params.matrix_row_count, params.matrix_column_count);

    for (int i = 0; i < params.vector_length; i++)
    {
        int i_RowId = params.row_id_eon + i;
        int index = obj_Id.index(obj_Id, i_RowId, params.column_id);
        float value = params.matrix[index];
        *params.vector_OUT= value;
        params.vector_OUT++;
    }        
}

static void 
copy_columns_to_vector_float(Params_Matrix_CopyColumnsToVector_float params)
{
    int column_id_range_length = params.column_id_end - params.column_id_eon + 1;
    int required_vector_length = column_id_range_length * params.matrix_row_count;
    int Matrix_copy_columns_to_vector_float__required_vector_length = 
        required_vector_length;
    assert(Matrix_copy_columns_to_vector_float__required_vector_length == params.vector_length);

    int column_length = params.row_id_end - params.row_id_eon + 1;

    for (int j = params.column_id_eon; j <= params.column_id_end; j++)
    {
        int i_ColumnId = j;

        Params_Matrix_CopyColumnToVector_float
        inputs = new_Params_CopyColumnToVector_float();
        inputs.matrix = params.matrix;
        inputs.matrix_column_count = params.matrix_column_count;
        inputs.matrix_row_count = params.matrix_row_count;
        inputs.row_id_eon = params.row_id_eon;
        inputs.row_id_end = params.row_id_end;
        inputs.column_id = i_ColumnId;
        inputs.vector_OUT  = params.vector_OUT;
        inputs.vector_length = column_length;
        copy_column_to_vector_float(inputs);

        //shift output vector pointer position by one column length
        for (int i = 0; i < column_length; i++)
        { params.vector_OUT++; }
    }        
}

static void 
write_text_file_float(Params_Matrix_WriteTextFile_float params)
{
    FILE *p_File = fopen(params.file_name, "w");

    float *A = params.matrix; // alias
    bool  bool_UseScientificNotation = params.scientific;

    Class_MatrixIndex2D
    obj_Id = new_MatrixIndex2D(
        params.matrix_row_count, params.matrix_column_count);

    for ( int i = 0; i < params.matrix_row_count; i++ )
    {
        for ( int j = 0; j < params.matrix_column_count; j++ )
        {
            int index = obj_Id.index(obj_Id, i, j);
            if ( bool_UseScientificNotation )
            {
                fprintf(p_File, "%.*e", params.precision, A[index]);
            }
            else
            {
                fprintf(p_File, "%.*f", params.precision, A[index]);
            }

            if ( j != (params.matrix_column_count - 1) )
                { fprintf(p_File, " "); }
        }
        fprintf(p_File, "\n");
    }

    fclose(p_File);
}

module_Matrix
import_Matrix(void)
{
    module_Matrix obj = {
        .hi = hi,
        .print = print,
        .print_float = print_float,
        .o2_norm_column_float = o2_norm_column_float,
        .o2_norm_columns_float = o2_norm_columns_float,
        .dot_column_float = dot_column_float,
        .add_scaled_vector_to_column_float = add_scaled_vector_to_column_float,
        .reset_float = reset_float,
        .copy_vector_to_column_float = copy_vector_to_column_float,
        .copy_vector_to_columns_float = copy_vector_to_columns_float,
        .copy_column_to_vector_float = copy_column_to_vector_float,
        .copy_columns_to_vector_float = copy_columns_to_vector_float,
        .write_text_file_float = write_text_file_float,
        .new_MatrixIndex2D = new_MatrixIndex2D,
        .new_Params_o2NormColumn_float = new_Params_o2NormColumn_float,
        .new_Params_o2NormColumns_float = new_Params_o2NormColumns_float,
        .new_Params_DotColumn_float = new_Params_DotColumn_float,
        .new_Params_Reset_float = new_Params_Reset_float,
        .new_Params_CopyVectorToColumn_float = new_Params_CopyVectorToColumn_float,
        .new_Params_CopyVectorToColumns_float = new_Params_CopyVectorToColumns_float,
        .new_Params_CopyColumnToVector_float = new_Params_CopyColumnToVector_float,
        .new_Params_CopyColumnsToVector_float = new_Params_CopyColumnsToVector_float,
        .new_Params_WriteTextFile_float = new_Params_WriteTextFile_float,
    };

    return obj;
}