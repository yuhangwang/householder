/**********************************************************
 Copyright (c) 2015 YUHANG WANG 
 ALL RIGHTS RESERVED

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
************************************************************/

#include "module_HouseholderFactorize.dep.h"

typedef struct
Params_HouseholderFactorize_float
{
    int    omp_thread_count;
    float *matrix;
    int    matrix_row_count;
    int    matrix_column_count;
    float *V_OUT;
    int    parallel_level;
    MPI_Comm mpi_communicator;
    int    mpi_task_mapping_style;
}
Params_HouseholderFactorize_float;

//=========================================
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~ temporary ~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//-------------------------------------------------
// parameter type for get_householder_vector_float
//-------------------------------------------------
typedef struct 
Params_HouseholderFactorize_GetHouseholderVector_float
{
    float *matrix;
    int    matrix_row_count;
    int    matrix_column_count;
    int    current_column_id;
    float *result_OUT;
}
Params_HouseholderFactorize_GetHouseholderVector_float;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//-------------------------------------------------
// parameter type for get_householder_beta_float
//-------------------------------------------------
typedef struct 
Params_HouseholderFactorize_GetHouseholderBeta_float
{
    float *householder_vector;
    int    householder_vector_length;
    float *result_OUT;
}
Params_HouseholderFactorize_GetHouseholderBeta_float;
//-------------------------------------------------

//-------------------------------------------------
// parameter type for get_householder_gamma_float
//-------------------------------------------------
typedef struct 
Params_HouseholderFactorize_GetHouseholderGamma_float
{
    float *matrix; 
    int    matrix_row_count;
    int    matrix_column_count;
    int    row_id_eon;
    int    column_id;
    float *householder_vector;
    int    householder_vector_length;
    float *result_OUT;
}
Params_HouseholderFactorize_GetHouseholderGamma_float;
//-------------------------------------------------
  

//-------------------------------------------------
// parameter type for get_householder_gamma_float
//-------------------------------------------------
typedef struct 
Params_HouseholderFactorize_UpdateMatrixColumn_float
{
    float *matrix; 
    int    matrix_row_count;
    int    matrix_column_count;
    int    row_id_eon;
    int    column_id;
    float  householder_beta;
    float  householder_gamma;
    float *householder_vector;
    int    householder_vector_length;
}
Params_HouseholderFactorize_UpdateMatrixColumn_float;

//-------------------------------------------------
// parameter type for get_householder_gamma_float
//-------------------------------------------------
typedef struct 
Params_HouseholderFactorize_OMP_UpdateMatrixColumn_float
{
    int    omp_thread_count;
    float *matrix; 
    int    matrix_row_count;
    int    matrix_column_count;
    int    row_id_eon;
    int   *column_ids;
    int    column_ids_length;
    float  householder_beta;
    float *householder_vector;
    int    householder_vector_length;
    int    rank;
}
Params_HouseholderFactorize_OMP_UpdateMatrixColumn_float;
//-------------------------------------------------

typedef struct 
module_HouseholderFactorize 
{
    void (*hi)(void);
    void (*factorize_float)(Params_HouseholderFactorize_float);
    void (*factorize_mpi_1D_float)(Params_HouseholderFactorize_float);
    void (*get_householder_vector_float)(Params_HouseholderFactorize_GetHouseholderVector_float);
    void (*get_householder_beta_float)(Params_HouseholderFactorize_GetHouseholderBeta_float);
    void (*get_householder_gamma_float)(Params_HouseholderFactorize_GetHouseholderGamma_float);
    void (*update_matrix_column)(Params_HouseholderFactorize_UpdateMatrixColumn_float);
    void (*omp_update_matrix_column)(Params_HouseholderFactorize_OMP_UpdateMatrixColumn_float);
    
    Params_HouseholderFactorize_float
    (*new_Params_HouseholderFactorize_float)(void);
   
    Params_HouseholderFactorize_GetHouseholderVector_float 
    (*new_Params_GetHouseholderVector_float)(void);
    
    Params_HouseholderFactorize_GetHouseholderBeta_float
    (*new_Params_GetHouseholderBeta_float)(void);
    
    Params_HouseholderFactorize_GetHouseholderGamma_float
    (*new_Params_GetHouseholderGamma_float)(void);
    
    Params_HouseholderFactorize_UpdateMatrixColumn_float
    (*new_Params_UpdateMatrixColumn_float)(void);     

    Params_HouseholderFactorize_OMP_UpdateMatrixColumn_float
    (*new_Params_OMP_UpdateMatrixColumn_float)(void);
}
module_HouseholderFactorize;

module_HouseholderFactorize 
import_HouseholderFactorize(void);