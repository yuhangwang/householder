/**********************************************************
 Copyright (c) 2015 YUHANG WANG 
 ALL RIGHTS RESERVED

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
************************************************************/

#include "module_MpiJob.dep.h"

typedef struct 
Params_MpiJob_TaskCountForMeBlock1D
{
    int mpi_this_rank;
    int mpi_volume;
    int mpi_total_task_count;
    int *result_OUT;
}
Params_MpiJob_TaskCountForMeBlock1D;

typedef struct 
Params_MpiJob_TaskCountForAllBlock1D
{
    int mpi_volume;
    int mpi_total_task_count;
    int *results_OUT;
}
Params_MpiJob_TaskCountForAllBlock1D;

typedef struct 
Params_MpiJob_TaskIdRangeForMeBlock1D
{
    int mpi_this_rank;
    int mpi_volume;
    int mpi_total_task_count;
    int *results_OUT;
}
Params_MpiJob_TaskIdRangeForMeBlock1D;

typedef struct 
Params_MpiJob_TaskIdsForMeBlock1D
{
    int mpi_this_rank;
    int mpi_volume;
    int mpi_total_task_count;
    int *results_OUT;
    int results_length;
}
Params_MpiJob_TaskIdsForMeBlock1D;

typedef struct 
Params_MpiJob_TaskIdsForMeCyclic1D
{
    int mpi_this_rank;
    int mpi_volume;
    int mpi_total_task_count;
    int *results_OUT;
    int results_length;
}
Params_MpiJob_TaskIdsForMeCyclic1D;

typedef struct 
Class_MpiJob_RankIdBlock1D
{
    int mpi_volume;
    int mpi_total_task_count;
    int (*rank)(struct Class_MpiJob_RankIdBlock1D, int task_id);
    int *_internal_storage;
    bool success;
}
Class_MpiJob_RankIdBlock1D;

typedef struct 
Class_MpiJob_RankIdCyclic1D
{
    int mpi_volume;
    int mpi_total_task_count;
    int (*rank)(struct Class_MpiJob_RankIdCyclic1D, int task_id);
    int *_internal_storage;
    bool success;
}
Class_MpiJob_RankIdCyclic1D;

typedef struct 
Params_MpiJob_GathervDisplacementsBlock1D
{
    int mpi_volume;
    int mpi_total_task_count;
    int *results_OUT;
}
Params_MpiJob_GathervDisplacementsBlock1D;

typedef struct 
Params_MpiJob_IsMyTask
{
    int *my_task_ids;
    int  my_task_count;
    int task_id;
}
Params_MpiJob_IsMyTask;

typedef struct 
module_MpiJob
{
    Class_MpiJob_RankIdBlock1D (*new_RankIdBlock1D)(int mpi_volume, int mpi_total_task_count, int _internal_storage[mpi_volume]);
    Class_MpiJob_RankIdCyclic1D (*new_RankIdCyclic1D)(int mpi_volume, int mpi_total_task_count);
    void (*task_count_for_me_block_1D)(Params_MpiJob_TaskCountForMeBlock1D);
    void (*task_count_for_all_block_1D)(Params_MpiJob_TaskCountForAllBlock1D);
    void (*task_id_range_for_me_block_1D)(Params_MpiJob_TaskIdRangeForMeBlock1D);
    void (*task_ids_for_me_block_1D)(Params_MpiJob_TaskIdsForMeBlock1D);
    void (*task_ids_for_me_cyclic_1D)(Params_MpiJob_TaskIdsForMeCyclic1D);
    void (*gatherv_displacements_block_1D)(Params_MpiJob_GathervDisplacementsBlock1D);
    bool (*is_my_task)(Params_MpiJob_IsMyTask);
    Params_MpiJob_TaskCountForMeBlock1D (*new_Params_TaskCountForMeBlock1D)(void);
    Params_MpiJob_TaskCountForAllBlock1D (*new_Params_TaskCountForAllBlock1D)(void);
    Params_MpiJob_TaskIdRangeForMeBlock1D (*new_Params_TaskIdRangeForMeBlock1D)(void);
    Params_MpiJob_TaskIdsForMeBlock1D (*new_Params_TaskIdsForMeBlock1D)(void);
    Params_MpiJob_TaskIdsForMeCyclic1D (*new_Params_TaskIdsForMeCyclic1D)(void);
    Params_MpiJob_GathervDisplacementsBlock1D (*new_Params_GathervDisplacementsBlock1D)(void);
    Params_MpiJob_IsMyTask (*new_Params_IsMyTask)(void);
}
module_MpiJob;

module_MpiJob 
import_MpiJob(void);