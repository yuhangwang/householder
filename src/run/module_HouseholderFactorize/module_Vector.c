/**********************************************************
 Copyright (c) 2015 YUHANG WANG 
 ALL RIGHTS RESERVED

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
************************************************************/

#include "module_Vector.dep.c"

Params_Vector_Dot_float
new_Params_Dot_float(void)
{
    Params_Vector_Dot_float
    obj = {
        .vector1 = NULL,
        .vector2 = NULL,
        .length = 0,
        .threads = 1,
        .result_OUT = NULL,
    };
    return obj;
}

//------------------------------------
// print_int()
//------------------------------------
static void
print_int(Params_Vector_Print_int input)
{
    for (int i = 0; i < input.length; i++)
    {
        printf("%d ", input.vector[i]);
    }
    puts("");
}

//------------------------------------
// print_float()
//------------------------------------
static void
print_float(Params_Vector_Print_float input)
{
    for (int i = 0; i < input.length; i++)
    {
        printf("%.*f ", input.precision, input.vector[i]);
    }
    puts("");
}

static void
reset_float(Params_Vector_Reset_float input)
{
    for (int i = 0; i < input.length; i++)
    {
        input.vector[i] = input.value;
    }
}

//------------------------------------
// dot_float()
//------------------------------------

static void
dot_float(Params_Vector_Dot_float input)
{
    float sum = 0.;

    for (int i = 0; i < input.length; i++)
    {
        sum += input.vector1[i] * input.vector2[i];
    }

    *input.result_OUT = sum;
}
//------------------------------------

//------------------------------------
// dot_float()
//------------------------------------

static void
omp_dot_float(Params_Vector_Dot_float input)
{
    int vector_length = input.length;

    /***********************************
     *         OpenMP setup            *
     ***********************************/
    omp_set_num_threads(input.threads);

    int i_OMP_ThreadCount = input.threads;

    if ( i_OMP_ThreadCount > vector_length )
    {
        i_OMP_ThreadCount = vector_length;
    }

    /***********************************/

    float sum = 0.;

    #pragma omp parallel for reduction(+:sum)
    for ( int i = 0; i < input.length; i++ )
    {
        sum += input.vector1[i] * input.vector2[i];
    }

    // int i_OMP_BlockSize = vector_length / i_OMP_ThreadCount;
    // float f_LocalSumArray[i_OMP_ThreadCount];

    // #pragma omp parallel for 
    // for ( int i = 0; i < i_OMP_ThreadCount; i++ )
    // {
    //     int i_OMP_ThisId = omp_get_thread_num();
    //     int i_Eon = i_OMP_ThisId * i_OMP_BlockSize;
    //     int i_End = ( i_OMP_ThisId + 1 ) * i_OMP_BlockSize;

    //     if ( ( i_OMP_ThisId == (i_OMP_ThreadCount - 1) ) &&
    //          ( i_End < vector_length ) )
    //     { 
    //         i_End = vector_length; 
    //     }

    //     int i_LocalSum = 0;
    //     for ( int j = i_Eon; j < i_End; j++ )
    //     {
    //         i_LocalSum += input.vector1[j] * input.vector2[j];
    //     }

    //     f_LocalSumArray[i] = i_LocalSum;
    // }        

    // for ( int i = 0; i < i_OMP_ThreadCount; i++ )
    // {
    //     sum += f_LocalSumArray[i];
    // }
    *input.result_OUT = sum;
}
//------------------------------------

//------------------------------------
// o2_norm_float()
//------------------------------------

static void
o2_norm_float(Params_Vector_o2Norm_float input)
{
    float *output = input.result_OUT;

    /**************************
     * must start from zero
     **************************/
    *output = 0.;

    for (int i = 0; i < input.length; i++)
    {
        *output += input.vector[i] * input.vector[i];
    }
    // note: the sqrt function provided by math.h
    // takes a double and output a double.
    // There's why I have to do a cast.

    int module_Vector_o2_norm_float_tmp_output = *output;
    assert(module_Vector_o2_norm_float_tmp_output > 0);
    *output = (float)sqrt((double) *output);
}

//------------------------------------
// add_float()
//------------------------------------

static void
add_float(Params_Vector_Add_float input)
{
    for (int i = 0; i < input.length; i++)
    {
        input.result_OUT[i] = 
            input.vector1[i] + input.vector2[i];
    }
}
//------------------------------------

static void
scale_int(Params_Vector_Scale_int input)
{
    for (int i = 0; i < input.length; i++)
    {
        input.vector[i] *= input.scalar;
    }
}

static void
scale_float(Params_Vector_Scale_float input)
{
    for (int i = 0; i < input.length; i++)
    {
        input.vector[i] *= input.scalar;
    }
}

static void
copy_float(Params_Vector_Copy_float input)
{
    int module_Vector_copy_float__input_length = input.length;
    assert(module_Vector_copy_float__input_length > 0);
    
    for (int i = 0; i < input.length; i++)
    {
        *input.vector_to = *input.vector_from;
        input.vector_from++;
        input.vector_to++;
    }
}

//===========================================

static void
hi(void)
{
    printf("Greetings from module_Vector!\n");
}

static void
dot(Params_Vector_Dot input)
{
    module_Type
    Type = import_Type();

    if (Type.same(input.type, Type.Float))
    {
        printf("(from Vector.dot) your input type is float\n");

        Params_Vector_Dot_float
        inputs = {
            .vector1 = (float*) input.vector1,
            .vector2 = (float*) input.vector2,
            .length  = input.length,
            .result_OUT = (float*) input.result_OUT,
        };

        dot_float(inputs);
    }

}

static void
norm(Params_Vector_o2Norm input)
{
    module_Type
    Type = import_Type();

    if (Type.same(input.type, Type.Float))
    {
        printf("(from Vector.norm) your input type is float\n");

        Params_Vector_o2Norm_float
        inputs = {
            .vector = (float*) input.vector,
            .length = input.length,
            .result_OUT = (float*) input.result_OUT,
        };

        o2_norm_float(inputs);
    }
}

static void
add(Params_Vector_Add input)
{
    module_Type 
    Type = import_Type();

    if (Type.same(input.type, Type.Float))
    {
        printf("(from Vector.add) your input type is float\n");

        Params_Vector_Add_float
        inputs = {
            .vector1 = (float*) input.vector1,
            .vector2 = (float*) input.vector2,
            .length  = input.length,
            .result_OUT = (float*) input.result_OUT,
        };

        add_float(inputs);
    }
}

module_Vector
import_Vector(void)
{
    module_Vector
    obj = {
        .hi = hi,
        .print_int = print_int,
        .print_float = print_float,
        .reset_float = reset_float,
        .dot = dot,
        .dot_float = dot_float,
        .omp_dot_float = omp_dot_float,
        .norm = norm,
        .o2_norm_float = o2_norm_float,
        .add = add,
        .add_float = add_float,
        .scale_int = scale_int,
        .scale_float = scale_float,
        .copy_float = copy_float,
        .new_Params_Dot_float = new_Params_Dot_float,
    };
    return obj;
}