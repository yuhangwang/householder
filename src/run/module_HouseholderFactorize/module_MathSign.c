#include "module_MathSign.dep.c"

int
sign_float(float input)
{
    int output = 1;
    if ( input > 0. ) { output = 1; } 
    else if ( input < 0. ) { output = -1; }
    else { output = 0; }
    return output;
}

static void
hi(void)
{
    printf("Greetings from module_MathSign!\n");
}

int
sign(Params_MathSign params)
{
    int output = 1;

    module_Type
    Type = import_Type();

    if (Type.same(params.type, Type.Float))
    {
        printf("(from module_MathSign.sign) your input type is float\n");
        float *input = (float*) params.input;
        output = sign_float(*input);
    }

    return output;
}

module_MathSign
import_MathSign(void)
{
    module_MathSign obj = {
        .hi = hi,
        .sign = sign,
        .sign_float = sign_float,
    };
    return obj;
}