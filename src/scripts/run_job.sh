#!/bin/sh

exe_file="./module_HouseholderFactorize.exe"
target_dir="./scratch/module_HouseholderFactorize"
cd $target_dir
for node_count in {2..3}; do
    echo $node_count
    mpirun -np $node_count ./$exe_file
done
cd ../