local la = require('module_la_shortcut')
local LaAutoTest = require("module_la_auto_test_c")

local Target = LaAutoTest:new()

Target:include("config.La_define")

Target:set_subject_module('module_MpiJob')

Target:set_internal_dependency_list{
	"module_Type",
	"module_Assert",
    "module_Vector",
}

Target:set_external_dependency_list{
}

Target:set_compiler('mpicc')
Target:set_compiler_flag('-fopenmp')
Target:set_linker_flag('-lm')

Target:set_runtime('mpirun')
Target:set_runtime_flag('-np 1')

return Target