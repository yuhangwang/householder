local la = require('module_la_shortcut')
local LaAutoTest = require("module_la_auto_test_c")

local Target = LaAutoTest:new()

Target:include("config.La_define")

Target:set_subject_module('module_Assert')

Target:set_internal_dependency_list{
	"module_Type",
}

Target:set_external_dependency_list{
}

Target:set_linker_flag('-lm')

return Target