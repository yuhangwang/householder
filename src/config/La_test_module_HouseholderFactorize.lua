local la = require('module_la_shortcut')
local LaAutoTest = require("module_la_auto_test_c")

local Target = LaAutoTest:new()

Target.mpi_volume = 2
Target.matrix_row_count = 100
Target.omp_thread_count = 1
Target.mpi_task_mapping_style = 1

Target.argv = {}
Target.argv[1] = tostring(Target.matrix_row_count)
Target.argv[2] = tostring(Target.omp_thread_count)
Target.argv[3] = tostring(Target.mpi_task_mapping_style)

Target:include("config.La_define")

Target:set_subject_module('module_HouseholderFactorize')

Target:set_internal_dependency_list{
	"module_Type",
	"module_Matrix",
	"module_Vector",
	"module_MathSign",
	"module_Assert",
    "module_MpiJob"
}

Target:set_external_dependency_list{
}


Target:set_linker_flag('-lm')


Target:set_compiler('mpicc')
Target:set_compiler_flag('-Wall -std=c99 -fopenmp')
Target:set_runtime('mpirun')
Target:set_runtime_flag(string.format('-np %s', Target.mpi_volume))

function Target:get_run_command()
    local run_command = la.path('.', self:get_executable())

    if self:get_runtime() ~= '' then
        run_command = la.command(
            self:get_runtime(), 
            self:get_runtime_flag(),
            run_command,
            unpack(self.argv))
    end
    return run_command
end

return Target